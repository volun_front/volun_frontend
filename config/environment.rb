# Load the Rails application.
require_relative "application"

# Initialize the Rails application.
Rails.application.initialize!
MONTHNAMES = [nil,'Enero', 'Febrero', 'Marzo',     'Abril',
    'Mayo','Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre','Noviembre',
    'Diciembre' ]