Leaflet.tile_layer = "https://api.tiles.mapbox.com/v4/mapbox.streets/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiaWFtZGVzYXJyb2xsbyIsImEiOiJjamNyaXQxamYwN3V0MndwaGd0Y3FzMGowIn0.MAPWA_aWdS9hFPh6ynwlCA"
# You can also use any other tile layer here if you don't want to use Cloudmade - see http://leafletjs.com/reference.html#tilelayer for more
Leaflet.attribution = "openstreetmap"
Leaflet.max_zoom = 18

#Leaflet.continuousWorld = true
#Leaflet.crs = crs23030