Rails.application.routes.draw do

  get "/", to: redirect("/volun")

  scope "volun" do
    mount Ckeditor::Engine => '/ckeditor'
    concern :downloadable do
      get 'download', on: :member
    end
    resources :new_campaings 

    namespace :rt do
      resources :others, only:[:new,:create]
      resources :activity_unpublishings, only:[:new,:create], param: :activity_id
      resources :activity_publishings, only:[:new,:create]
      resources :project_unpublishings, only:[:new,:create], param: :project_id
      resources :project_publishings, only:[:new,:create], param: :project_id
      resources :volunteers_demands, only:[:new,:create], param: :project_id
      resources :entity_unsubscribes, only:[:new,:create]
      resources :volunteer_appointments, only:[:new,:create]
      resources :volunteer_amendments, only:[:new,:create]
      resources :volunteer_unsubscribes, only:[:new,:create], param: :project_id
      resources :volunteer_subscribes, only:[:new,:create,:index]
      resources :volunteer_project_subscribes, only:[:new,:create,:index]
      resources :solidarity_chains, only:[:new,:create]
    end

    resources :addresses do
      get 'bdc_search_towns', on: :collection
      get 'bdc_search_roads', on: :collection
      get 'bdc_search_road_numbers', on: :collection
    end

    resources :activities, only: [:show, :index ] do
      collection do
        get :my_area
        get :boroughs
        get :search
      end
    end

    resources :entities, only:[:show,:new,:create] do
      resources :projects, only:[:index, :show], param: :q
      resources :activities, only:[:index, :show], param: :q
    end

    resources :projects, only: [:show, :index] do
      collection do
        get :my_area
        get :boroughs
        get :search
      end
      resources :images
      resources :links
    end

    resources :links, concerns: :downloadable

    # resources :volunteers do
    #   resources :projects, only:[:index], param: :q
    # end

    devise_for :users, controllers: {
                        sessions: 'users/sessions',
                        passwords: 'users/passwords',
                        registrations: 'users/registrations'
                      }
    resources :users, only: [:show] do
      collection do
        get :search_activities
        get :search_projects
        get :search_requests
        get :search_meetings
        post :download_zip
      end
    end

    get 'text_json' => 'welcome#test'

    get 'welcome/index'
    get '/whoami' => 'pages#whoami'
    get '/accesibility' => 'pages#accesibility'
    get '/volunteers' => 'pages#volunteers'
    get '/entities' => 'pages#entities'
    get '/mailchimp' => 'pages#mailchimp'
    get '/conditions' => 'pages#conditions'
    get '/blog' => redirect("https://voluntariospormadridblog.madrid.es/")
    get '/mta_privacy' => 'pages#mta_privacy'

    get '/apple-app-site-association', to: redirect('/system/apple-app-site-association')
    get '/.well-known/apple-app-site-association', to: redirect('/system/apple-app-site-association')
    resources :pages, path: '/', only: [:show]

    root 'welcome#index'
    post "/" => "welcome#index"

    draw :ws
    draw :profile
  end

  if Rails.env.development?
    mount LetterOpenerWeb::Engine, at: "/letter_opener"
  end
end
