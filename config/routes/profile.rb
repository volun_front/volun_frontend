scope :profile do
    get 'myarea' => 'profile/users#index', as: "myarea"
    post 'download_zip' => 'profile/users#download_zip', as: "download_zip"
    get 'project/:id' => 'profile/projects#show',as: 'show_project'
    get 'faq/:id' => 'profile/projects#faq',as: 'faq_digital'
    get 'topic/:id' => 'profile/projects#topic',as: 'topic_digital'
    get 'new_topic/:id' => 'profile/projects#new_topic',as: 'new_topic_digital'
    post 'create_topic/:id' => 'profile/projects#create_topic',as: 'create_topic_digital'
    get 'vote_topic_up/:id/topic/:topic_id' => 'profile/projects#vote_topic_up',as: 'vote_topic_up_digital'
    get 'vote_topic_down/:id/topic/:topic_id' => 'profile/projects#vote_topic_down',as: 'vote_topic_down_digital'
    get 'follow_topic_up/:id/topic/:topic_id' => 'profile/projects#follow_topic_up',as: 'follow_topic_up_digital'
    get 'follow_topic_down/:id/topic/:topic_id' => 'profile/projects#follow_topic_down',as: 'follow_topic_down_digital'
    get 'topic_show/:id/topic/:topic_id' => 'profile/projects#topic_show',as: 'topic_show_digital'
    post 'create_comment/:id/topic/:topic_id' => 'profile/projects#create_comment',as: 'create_comment_digital'
    get 'vote_comment_up/:topic_id/comment/:comment_id' => 'profile/projects#vote_comment_up',as: 'vote_comment_up_digital'
    get 'vote_comment_down/:topic_id/comment/:comment_id' => 'profile/projects#vote_comment_down',as: 'vote_comment_down_digital'
end