source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.7.2'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails', branch: 'main'
gem 'rails', '~> 6.1.4'
# Use sqlite3 as the database for Active Record
#gem 'sqlite3', '~> 1.4'
# Use Puma as the app server
gem 'puma', '~> 5.0'
# Use SCSS for stylesheets
gem 'sass-rails', '>= 6'
# Transpile app-like JavaScript. Read more: https://github.com/rails/webpacker
#gem 'webpacker', '~> 5.0'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.7'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
# Use Active Model has_secure_password
# gem 'bcrypt', '~> 3.1.7'
gem 'rails-assets-sweetalert2', '~> 5.1.1', source: 'http://insecure.rails-assets.org'
gem 'sweet-alert2-rails'
gem 'rails-sweetalert2-confirm'
# Use Active Storage variant
# gem 'image_processing', '~> 1.2'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.4.4', require: false

gem 'bootstrap-sass'
gem 'bootstrap-sass-extras'

gem 'bootstrap-datetimepicker-rails' 
gem 'browser'
gem 'coffee-rails'
gem 'devise'
gem 'invisible_captcha'
gem 'jquery-rails'
gem 'jquery-ui-rails'
gem 'kaminari'
gem 'leaflet-rails'

gem 'leaflet-fullscreen-rails'

gem 'leaflet-js'
gem 'newrelic_rpm'
gem 'pg', '~> 1.2', '>= 1.2.3'
gem 'ransack'
gem 'rollbar'
gem 'savon'
gem 'sdoc', group: :doc
gem 'sitemap_generator'
gem 'social-share-button'#, '~> 0.10'
gem 'uglifier', '>= 1.3.0'
gem 'unicorn'
gem 'paperclip', '~> 5.0.0'
gem 'paperclip-ffmpeg'
gem 'paperclip-av-transcoder'
gem 'will_paginate-bootstrap'
gem 'will_paginate'
gem 'responders'
gem 'rubyzip'#, '>= 1.0.0'
gem 'zip-zip'
gem 'simple_form'
gem 'nested_form'
gem 'ahoy_matey'#, '2.2'
gem 'chartkick'#, '~> 3.2'
gem 'groupdate'
gem 'highcharts'#, '~> 0.0.3'
gem "simple_calendar"#, "~> 2.0"
gem 'delayed_job_active_record'
gem "daemons"
gem 'ckeditor_rails_6'
gem 'ckeditor'#, github: 'galetahub/ckeditor'
gem 'sprockets', '~> 4'
gem 'sprockets-rails'#, :require => 'sprockets/railtie'
gem "cocoon"

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console', '>= 4.1.0'
  # Display performance information such as SQL time and flame graphs for each request in your browser.
  # Can be configured to work on production as well see: https://github.com/MiniProfiler/rack-mini-profiler/blob/master/README.md
  gem 'rack-mini-profiler', '~> 2.0'
  gem 'listen', '~> 3.3'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'capistrano',         '~> 3.5.0', require: false
  gem 'capistrano-rails',   '~> 1.1.6', require: false
  gem 'capistrano-bundler', '~> 1.1.4', require: false
  gem 'capistrano-rvm',              require: false
  gem 'capistrano3-delayed-job', '~> 1.7.3'
  gem 'mdl', require: false
  gem 'rubocop', require: false
  gem 'rubocop-faker', require: false
  gem 'rvm1-capistrano3', '~> 1.4.0', require: false
  gem 'scss_lint',  require: false
end

group :test do
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '>= 3.26'
  gem 'selenium-webdriver'
  # Easy installation and use of web drivers to run system tests with browsers
  gem 'webdrivers'
  gem 'coveralls', require: false
  gem 'database_cleaner'#, '~> 1.6.1'
  gem 'email_spec'#, '~> 2.1.0'
  gem 'poltergeist'#, '~> 1.15.0'
  gem 'rspec-rails'#, '~> 3.6'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]


group :development, :test do
 # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'bullet'
  gem 'factory_girl_rails'
  gem 'faker'
  gem 'i18n-tasks'
  gem 'launchy'
  gem 'letter_opener_web'
  #gem 'quiet_assets'
  gem 'spring-commands-rspec'
end
