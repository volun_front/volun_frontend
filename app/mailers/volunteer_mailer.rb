class VolunteerMailer < ActionMailer::Base

    default from: (Rails.application.secrets.default_email_sender || 'comunidadvoluntariad@madrid.es')

    def send_email(mail_address, options = {})
        @message = options[:message]
        mail(from: options[:sender].presence || Rails.application.secrets.default_email_sender || 'comunidadvoluntariad@madrid.es', to: mail_address, subject: options[:subject])
    end

    def send_email_ws(mail_address, options = {})
        @message = options[:message]
        if !options[:upload].nil?
          options[:upload].each do |file|
            if !options[:type_upload].blank? && options[:type_upload].to_s == "csv"
              attachments["#{options[:name_upload]}"] = {mime_type: 'text/csv', content: file}
            else
              attachments["#{file.original_filename}"] = File.read(file.tempfile)
            end
          end
        end 
        mail(from: "voluntariospormadrid@madrid.es", to: mail_address, subject: options[:subject])
    end
end