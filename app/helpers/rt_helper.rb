module RtHelper

    def get_color_rt(status_id) 
        case t(status_id).to_s 
        when 'Rechazada'
          color = '#e74d4d' 
        when 'Aceptada'
          color = '#36b933' 
        when 'En trámite'
          color = '#e1991b'
        when 'Citado'
          color = '#64b8f6' 
        end
    end
end