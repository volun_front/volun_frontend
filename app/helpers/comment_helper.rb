module CommentHelper

  def filter_comments(comments, search_text)
    comments=comments.active.order(created_at: :desc)
    if !search_text.blank?
      comments = comments.where("translate(UPPER(cast(digital_comments.body as varchar)), 'ÁÉÍÓÚ', 'AEIOU') LIKE translate(UPPER(cast(? as varchar)), 'ÁÉÍÓÚ', 'AEIOU')", "%#{search_text}%")
    else
      comments
    end
  end
end