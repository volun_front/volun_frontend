module ApplicationHelper
    def home_page?
      return false if user_signed_in?
      request.path == '/'
    end

    def volunteer_tabs
      [["myProjects",t('user.other.projects')],["myProjectsDigitals","Proyectos digitales"],["myRequests",t('user.other.requests')],["myMeetings","Mis reuniones"]]
    end

    def entity_tabs
      [["myProjects",t('user.other.projects')],["myActivities",t('user.other.activities')],["myRequests",t('user.other.requests')]]
    end

    def get_districts
      District.all.order(:name)
    rescue
      []
    end

    def get_boroughts(district)
      Borought.where(district_id: district).order(:name)
    rescue
      []
    end
  
    def current_path_with_query_params(query_parameters)
      url_for(request.query_parameters.merge(query_parameters))
    end
  
    def markdown(text)
      render_options = {
        filter_html:     false,
        hard_wrap:       true,
        link_attributes: { target: '_blank' }
      }
      renderer = Redcarpet::Render::HTML.new(render_options)
      extensions = {
        autolink:           true,
        fenced_code_blocks: true,
        lax_spacing:        true,
        no_intra_emphasis:  true,
        strikethrough:      true,
        superscript:        true
      }
      Redcarpet::Markdown.new(renderer, extensions).render(text).html_safe
    end
  
    def districts_select_options
      District.all.order(name: :asc).collect { |g| [g.name, g.id] }
    end
  
    def request_reasons_select_options
      ReqReason.all.order(id: :asc).collect { |g| [g.description.html_safe, g.id] }
    end
  
    def road_types_select_options
      RoadType.all.order(name: :asc).collect { |g| [g.name, g.name] }
    end
  
    def entity_types_select_options
      EntityType.all.order(description: :asc).collect { |g| [g.description.html_safe, g.id] }
    end
  
    def number_types_select_options
      { 'Km' => :Km, 'Num' => :Num }
    end
  
    def provinces_select_options
      Province.all.select(:name,:id).order(name: :asc).collect { |g| [g.name, g.id] }
    end
  
    def projects_select_options
      Project.all.order(name: :asc).collect { |g| [g.name, g.id] }
    end
  
    def entity_projects_select_options(id)
      Project.unscoped.entity_projects(id).order(name: :asc).collect { |g| [g.name, g.id] }
    end
  
    def volunteer_projects_select_options(id)
      Project.unscoped.includes(:volunteers).where("volunteer_id = ?", id).references(:volunteers).order(name: :asc).collect { |g| [g.name, g.id] }
    end
  
    def entity_activities_select_options(id)
      Activity.unscoped.where("entity_id = ?", id).order(name: :asc).collect { |g| [g.name, g.id] }
    end
  
    def get_latitude_and_longitude (address)
      @hash_coords={}
        if !address.blank? && address.normalize?
          unless address.district.try(:name) == 'OTROS'
            if (address.yetrs89.blank? || address.xetrs89.blank?)
              TransformBDC.transform_bdc(address.latitude, address.longitude)
            else 
              {:latitude=> address.yetrs89, :longitude=> address.xetrs89}
            end
          end
        else
          {:latitude=> 4474098.85, :longitude=> 439986.21}
        end
    end
  end
  
  module AuditGenerate
    def self.generate(attr_options={})
      unless attr_options[:resource_id].blank?
        Audit.create(attr_options)
      end
    end
  end
  
  module GlobalMailSMS 
    extend ActiveSupport::Concern
  
    def self.send_rt(object, message,subject)
      if object.email.blank?
        if !object.phone_number_alt.blank? || !object.phone_number.blank?
          begin
            movil = object.mobile_number
            SMSApi.new.sms_deliver(movil, message)
          rescue Exception  => e
            Rails.logger.error('sendSms#rt') do
              "#{I18n.t('sms.error')}: \n#{e}"
            end
          end
        end
      else
        begin
          VolunteerMailer.send_email(object.email, message: message, subject: subject).deliver_now
        rescue Exception  => e
          Rails.logger.error('sendEmail#rt') do
            "#{I18n.t('email.error')}: \n#{e}"
          end
        end
      end
    end
  
    def self.send(param={}, message,subject)
      if param.any?
        if param[:email].blank?
          unless param[:phone_number].blank?
            begin
              movil = param[:phone_number]
              SMSApi.new.sms_deliver(movil, message)
            rescue Exception  => e
              Rails.logger.error('sendSms#rt') do
                "#{I18n.t('sms.error')}: \n#{e}"
              end
            end
          end
        else
          begin
            VolunteerMailer.send_email(param[:email], message: message, subject: subject).deliver_now
          rescue Exception  => e
            Rails.logger.error('sendEmail#rt') do
              "#{I18n.t('email.error')}: \n#{e}"
            end
          end
        end
      end
    end
  
  end
  
  module TransformBDC 
    extend ActiveSupport::Concern
    require 'net/http' 
    require 'json' 
  
    def transform_bdc (latitude, longitude)
      coords={xetrs89: '',yetrs89: ''}
        begin
          lat_aux       = latitude.to_i/100.0 
          long_aux      = longitude.to_i/100.0 
          if Rails.env.production?
            environment = 'http://www-j.munimadrid.es';
          elsif Rails.env.preproduction?
            environment = 'http://prejinter.munimadrid.es';
          else
            environment = 'https://desajinter.munimadrid.es';
          end
  
          url = "#{environment}/BDCTR_RSGENERAL/restBDC/conversionETRS89?coordX=#{lat_aux}&coordY=#{long_aux}&aplicacion=VOLUN" 
  
          uri = URI.parse(url) 
          response = Net::HTTP.get(uri) 
          hash_89 = JSON.parse(response) 
          
          coord[:xetrs89] = hash_89["coordX"] 
          coord[:yetrs89] = hash_89["coordY"] 
  
        rescue => exception
          coord[:xetrs89] = nil
          coord[:yetrs89] = nil
        end
        coords
    end
  end