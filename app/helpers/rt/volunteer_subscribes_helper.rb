module Rt::VolunteerSubscribesHelper

	def volunteer_create(attr_p)  #Método que crea un nuevo voluntario
    
      
      req_exist = Rt::VolunteerSubscribe.joins(:request_form).where("rt_volunteer_subscribes.email=? AND (request_forms.req_status_id=1 OR request_forms.req_status_id=2 OR request_forms.req_status_id=3)",rt_volunteer_subscribe_params[:email]).count
      @project= Project.find(params[:project_id]) unless params[:project_id].blank?

      if (req_exist>0) #Si ya tiene una solicitud -> ERROR  
        redirect_to new_rt_volunteer_subscribe_path, alert: I18n.t('request.exist')
      else #Si no, genera la nueva solicitud de alta de voluntario
        @rt_volunteer_subscribe = Rt::VolunteerSubscribe.new(rt_volunteer_subscribe_params) #Nueva solicitud
        @rt_volunteer_subscribe.set_terms_of_service(params[:terms_of_service])
        @rt_volunteer_subscribe.channel = Channel.find_by(code: 1)

        if @rt_volunteer_subscribe.save #Si se crea con éxito
          if attr_p[:user_id].blank?
            current = nil
          else 
            current = User.find(attr_p[:user_id])
          end

          attr_p[:operation_type] = "#{I18n.t('audit.register')}" 
          attr_p[:operation] = "#{I18n.t('audit_enum.register', id: @rt_volunteer_subscribe.id)}"
          attr_p[:new_data] = JSON.parse(@rt_volunteer_subscribe.attributes.to_json).except("created_at", "updated_at")
          AuditGenerate.generate(attr_p)
    
          session.delete(:project)

          redirect_to projects_path, notice: t('volunteer_subscribe.response')
        else
          respond_with(@rt_volunteer_subscribe)
        end
      end
    rescue => e
      begin
        Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
  end

    def project_volunteer_create(attr_p, user) #Método para crear una solicitud de alta de un voluntario ya registrado en un proyecto
      req_exist = Rt::VolunteerProjectSubscribe.joins(:request_form).where("rt_volunteer_project_subscribes.email=? AND rt_volunteer_project_subscribes.project_id=? AND (request_forms.req_status_id=1 OR request_forms.req_status_id=2 OR request_forms.req_status_id=3)",rt_volunteer_project_subscribe_params[:email],rt_volunteer_project_subscribe_params[:project_id]).count
      @project= Project.find(params[:project_id]) unless params[:project_id].blank?
      if (req_exist>0) #Si ya tiene una solicitud -> ERROR  
    
        redirect_to new_rt_volunteer_project_subscribe_path(project_id: params[:project_id]), alert: I18n.t('request.exist')
      else #Si no, genera la nueva solicitud de alta de voluntario en proyecto
        @rt_volunteer_project_subscribe = Rt::VolunteerProjectSubscribe.new(rt_volunteer_project_subscribe_params) #Nueva solicitud
        @rt_volunteer_project_subscribe.request_form.user_id = user.id unless user.blank?
        begin
          @rt_volunteer_project_subscribe.volunteer_id = user.loggable.id
        rescue
        end
        @rt_volunteer_project_subscribe.set_terms_of_service(params[:terms_of_service])
        # @rt_volunteer_project_subscribe.request_form.user_id = User.find(attr_p[:user_id]) unless User.find(attr_p[:user_id]).blank?

          if @rt_volunteer_project_subscribe.save #Si se crea con éxito
            if User.find(attr_p[:user_id]).blank?
              current = nil
            else 
              current = User.find(attr_p[:user_id])
            end
            attr_p[:operation_type] = "#{I18n.t('audit.register')}" 
            attr_p[:operation] = "#{I18n.t('audit_enum.register', id: @rt_volunteer_project_subscribe.id)}"
            attr_p[:new_data] = JSON.parse(@rt_volunteer_project_subscribe.attributes.to_json).except("created_at", "updated_at")
            AuditGenerate.generate(attr_p)
      
            session.delete(:project)
            VolunInterestedTracking.interesting_track(user,@rt_volunteer_project_subscribe,user.loggable_id, @project)
            redirect_to projects_path, notice: t('volunteer_subscribe.response')
          else
            respond_with(@rt_volunteer_project_subscribe)
          end
      end
    rescue => e
      begin
        Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
  end

end


module VolunInterestedTracking
  #Seguimiento que indica que un voluntario se ha interesado por un proyecto
  extend ActiveSupport::Concern
  def self.interesting_track(volunteer,manager, project,attr_p)
    req_form_aux = volunteer.request_form
    req_form_aux.user = User.find(attr_p[:user_id])
    req_form_aux.save
    puts "==========================="
    puts volunteer.attributes
    puts volunteer.request_form.user
    puts User.find(attr_p[:user_id])
    puts volunteer.request_form.id
    comments = I18n.t('volunteer_project_tracking', name: "#{project.name}" ) 
  
    if (!project.blank? || !manager.blank?)
      default_attrs = {
          tracking_type:  TrackingType.get_volunteer_project_subscribe,
          comments:       comments,
          volunteer:      volunteer.request_form.user.loggable,
          request_form:   volunteer.request_form,
          project:        project,
          manager_id:     nil,
          tracked_at:     DateTime.now,
          automatic:      true
        }
        tracking = Volun::Tracking.new(default_attrs)
        if tracking.save
          if User.find(attr_p[:user_id]).blank?
            current = nil
          else 
            current = User.find(attr_p[:user_id])
          end
          attr_p[:operation_type] = "#{I18n.t('audit.register')}" 
          attr_p[:operation] = "#{I18n.t('audit_enum.register', id: tracking.id)}"
          attr_p[:new_data] = JSON.parse(tracking.attributes.to_json).except("created_at", "updated_at")
          AuditGenerate.generate(attr_p)
        end
    end
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end
end