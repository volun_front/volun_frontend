module CalendarCustomHelper

    def get_week_first_monday
        th_export= ""
        (1..6).each {|day| th_export =  th_export + "<th >#{I18n.t('date.abbr_day_names')[day]}</th>"}
        th_export = th_export + "<th >#{I18n.t('date.abbr_day_names')[0]}</th>"

        th_export.html_safe
    rescue => e
        begin
          Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
        ""
    end   

    def get_days_month_range(current = Time.zone.now, events=nil, params_aux= {})
        body = ""
        start_date = current.beginning_of_month.to_date
        end_date= current.end_of_month.to_date

        return body if (start_date.blank? || end_date.blank?)
        parameters = params_aux.dup

        begin
            parameters[:set_date] = Time.zone.parse(parameters[:set_date]).to_date.strftime("%Y-%m-%d")
        rescue
            parameters[:set_date] = current.strftime("%Y-%m-%d")
        end

        (start_date.to_date..end_date.to_date).each do |week| 
            if week==start_date ||  week.wday == 1
                body = body + "<tr>"
                if week==start_date
                    if week.wday == 0
                        (1..6).each {|d|  body = body + get_out_day}
                        body = body + get_day(week, events, parameters, current)
                    else
                        (1..week.wday-1).each {|d|  body = body + get_out_day}
                        body = body + get_day(week, events, parameters, current)
                    end
                elsif week==end_date
                    body = body + get_day(week, events, parameters, current)
                    if week.wday == 6
                        body = body + get_out_day
                    elsif week.wday != 0
                        (week.wday+1..7).each {|d|  body = body + get_out_day}
                    end
                    body = body + "</tr>"
                else
                    body = body + get_day(week, events, parameters, current)
                    body = body + "</tr>" if  week.wday == 0
                end
            else
                if week==end_date
                    body = body + get_day(week, events, parameters, current)
                    if week.wday == 6
                        body = body + get_out_day
                    elsif week.wday != 0
                        (week.wday+1..7).each {|d|  body = body + get_out_day}
                    end
                    body = body + "</tr>"
                else
                    body = body + get_day(week, events, parameters, current)
                    body = body + "</tr>" if  week.wday == 0
                end
            end
        end

        body.html_safe
    rescue => e
        begin
          Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
        ""
    end

    def set_now_calendar(params_aux = {},  item=nil)
        parameters = params_aux.dup        
        current_path_with_query_params({:set_date => Date.today}.merge(parameters.permit!.except(:set_date)))
    rescue => e
        begin
          Rails.logger.error("COD-00004: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
        current_path_with_query_params({})
    end

    def set_day_calendar(params_aux = {})
      parameters = params_aux.dup        
      current_path_with_query_params({:select_check => 'day'}.merge(parameters.permit!.except(:select_check)))
    rescue => e
      begin
        Rails.logger.error("COD-00004: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
      current_path_with_query_params({})
    end

    def set_month_calendar(params_aux = {})
      parameters = params_aux.dup        
      current_path_with_query_params({:select_check => 'month'}.merge(parameters.permit!.except(:select_check)))
    rescue => e
      begin
        Rails.logger.error("COD-00004: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
      current_path_with_query_params({})
    end

    def set_init_month_calendar(params_aux = {},  item=nil)
        parameters = params_aux.dup       
        current_path_with_query_params({:set_date => parameters[:set_date].blank? ? Date.today.change(month: 1) : Date.parse(parameters[:set_date]).change(month: 1)}.merge(parameters.permit!.except(:set_date)))
    rescue => e
        begin
          Rails.logger.error("COD-00005: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
        current_path_with_query_params({})
    end

    def set_previus_month_calendar(params_aux = {},  item=nil)
        parameters = params_aux.dup        
        current_path_with_query_params({:set_date => parameters[:set_date].blank? ? Date.today - 1.month : Date.parse(parameters[:set_date]) - 1.month}.merge(parameters.permit!.except(:set_date)))
    rescue => e
        begin
          Rails.logger.error("COD-00006: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
        current_path_with_query_params({})
    end

    def set_next_month_calendar(params_aux = {},  item=nil)
        parameters = params_aux.dup        
        current_path_with_query_params({:set_date => parameters[:set_date].blank? ? Date.today + 1.month : Date.parse(parameters[:set_date]) + 1.month}.merge(parameters.permit!.except(:set_date)))
    rescue => e
        begin
          Rails.logger.error("COD-00007: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
        current_path_with_query_params({})
    end

    def set_finish_month_calendar(params_aux = {},  item=nil)
        parameters = params_aux.dup        
        current_path_with_query_params({:set_date => parameters[:set_date].blank? ? Date.today.change(month: 12) : Date.parse(parameters[:set_date]).change(month: 12)}.merge(parameters.permit!.except(:set_date)))
    rescue => e
        begin
          Rails.logger.error("COD-00008: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
        current_path_with_query_params({})
    end

    def set_tab_calendar(params_aux = {},  item=nil, current=Time.zone.now)
        parameters = params_aux.dup
        return current_path_with_query_params(parameters) if parameters.blank? || item.blank?
        if item.to_s=="all"
            parameters.merge!({:tab_calendar => item,:set_date => nil, :select => {:month =>  current.to_date.month , :year => current.to_date.year, :day => current.to_date.day}}) 
        elsif item == parameters[:tab_calendar]
            parameters.merge!({:tab_calendar => nil,:set_date => parameters[:set_date], :select => {:month =>  current.to_date.month , :year => current.to_date.year, :day => current.to_date.day}}) 
        else
            parameters.merge!({:tab_calendar => item,:set_date => parameters[:set_date], :select => {:month =>  current.to_date.month , :year => current.to_date.year, :day => current.to_date.day}}) 
        end
        current_path_with_query_params(parameters)
    rescue => e
        begin
          Rails.logger.error("COD-0009: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
        current_path_with_query_params({})
    end   

    private

    def get_day(week, events, parameters, current)
        f_wday = (current - (current.wday - 1))
        f_wday = f_wday.month == current.month ? f_wday.day : 1
        l_wday = (current + (7 - current.wday))
        l_wday = l_wday.month == current.month ? l_wday.day : current.end_of_month.day
        
        if week==Time.zone.now.to_date
            aux_class ='calendar_event_now'
        elsif !events.blank? && !events.where(" cast(? as date) =cast(start_date as date) OR cast(? as date) = cast(end_date as date) OR  cast(? as date) between cast(start_date as date) and cast(end_date as date)", week,week,week).blank?
            aux_class ='calendar_event'
        elsif (!parameters[:set_date].blank? && week == parameters[:set_date].to_date) || parameters[:tab_calendar] == 'month' || (parameters[:tab_calendar] == 'week' && (f_wday..l_wday).include?(week.day))
            aux_class ='calendar_selected'
        else
            aux_class ='calendar_without'
        end

        aux= "<td style='border: 0.1rem solid lightgrey;text-align: center' class='#{aux_class}'>"
        aux = aux + "<a class='day_link calendar_without' href='#{current_path_with_query_params({tab_calendar: parameters[:tab_calendar], set_date: week, holder: parameters[:holder],
            :select => {day: week.day, year: week.year, month: week.month}})}'>#{week.day}</a>"
        aux = aux + "</td>"
    end

    def get_out_day        
        "<td class='calendar_out' style='border: 0.1rem solid lightgrey;'></td>"
    end

end