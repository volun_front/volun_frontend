
// $(document).ready(function(){
//     var utm = "+proj=utm +zone=30";
//     var wgs84 = "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs";
  
//     //proj4.defs('EPSG:25830', '+proj=utm +zone=30 +ellps=GRS80 +units=m +no_defs');
//     //proj4.defs["EPSG:23030"] = "+proj=utm +zone=30 +ellps=intl +units=m +no_defs";
  
//     var crs25830 = new L.Proj.CRS('EPSG:25830',
//    '+proj=utm +zone=30 +ellps=GRS80 +units=m +no_defs', //http://spatialreference.org/ref/epsg/25830/proj4/
//               {
//                   resolutions: [2048, 1024, 512, 256, 128, 64, 32, 16, 8, 4, 2, 1, 0.5],
//       //Origen de servicio teselado
//       //origin:[0,0]
//     });
  
//    var crs23030 = new L.Proj.CRS('EPSG:23030',
//    '+proj=utm +zone=30 +ellps=intl +units=m +no_defs', //http://spatialreference.org/ref/epsg/25830/proj4/
//               {
//                   resolutions: [2048, 1024, 512, 256, 128, 64, 32, 16, 8, 4, 2, 1, 0.5],
//       //Origen de servicio teselado
//       //origin:[0,0]
//     });
  
//     var latlonGEO = "" + proj4(utm,wgs84,[440389.35,4481736.16])
//     var arrGEO = latlonGEO.split(",", 2);
//     var map = L.map( 'map', {
//       minZoom: 2,
//       maxZoom: 18,
//       crs: crs23030,
//       center: [ arrGEO[1], arrGEO[0] ], // coordenadas etrs89
//       zoom: 12,
  
//     });
  
  
//   var layer = L.tileLayer( 'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}',
//     {
//       attribution: 'openstreetmap',
//       id: 'mapbox.streets',
//       accessToken: 'pk.eyJ1IjoiaWFtZGVzYXJyb2xsbyIsImEiOiJjamNyaXQxamYwN3V0MndwaGd0Y3FzMGowIn0.MAPWA_aWdS9hFPh6ynwlCA',
//       continuousWorld: true,
//       crs: crs23030
//     }).addTo(map);
  
//   /*
//     var layer = L.tileLayer( 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
//     {
//       attribution: 'openstreetmap',
//       subdomains: ['a','b','c'],
//       continuousWorld: true,
//       crs: crs23030
//     }).addTo(map);
  
//   */
  
//   /*
//     var layerRasterIgn = new L.tileLayer.wms("http://www.ign.es/wms-inspire/mapa-raster",
//       {
//          layers: 'mtn_rasterizado',
//          crs: crs23030,
//          format: 'image/png',
//          continuousWorld: true,
//          transparent: true,
//          attribution: '© Instituto Geográfico Nacional de España'
//   }).addTo(map);
  
//   */
  
//     var myIcon = L.icon({
//       iconUrl: '/assets/GreenPin1LargeB.png',
//       iconSize: [20, 20],
//       iconAnchor: [22, 94],
//       popupAnchor: [-3, -76],
//       shadowUrl: '/assets/GreenPin1LargeB.png',
//       shadowSize: [0, 0],
//       shadowAnchor: [22, 94]
//   });
  
//     var greenIcon = L.icon({
//       iconUrl: 'leaf-green.png',
//       shadowUrl: 'leaf-shadow.png',
  
//       iconSize:     [10, 10], // size of the icon
//       shadowSize:   [1, 1], // size of the shadow
//       iconAnchor:   [10, 10], // point of the icon which will correspond to marker's location
//       shadowAnchor: [0, 0],  // the same for the shadow
//       popupAnchor:  [-0, -0] // point from which the popup should open relative to the iconAnchor
//   });
  
//    $.each(locations, function(i, location)
//    {
//       var latitude = (location.address.latitude/100)
//       var longitude = (location.address.longitude/100)
//       latlonGEO = "" + proj4(utm,wgs84,[latitude,longitude])
//       arrGEO = latlonGEO.split(",", 2);
//       latitude = arrGEO[1];
//       longitude = arrGEO[0];
//       if (latitude != null && longitude!= null)
//       {
//         L.marker([latitude, longitude], {icon: myIcon}).addTo(map);
//         //L.marker([latitude,longitude], {icon: myIcon}).addTo(map).bindPopup('prueba');
//       }
//       //L.marker([40.41806830000001 , -3.6752301000000216], {icon: myIcon}).addTo(map);
  
//    });
  
  
  
//   })
  