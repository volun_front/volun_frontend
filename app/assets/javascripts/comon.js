
var map;

function coords_convert (latitude, longitude,color,i,object,type){
  /*CONVERSION DE ETRS89 A COORDENADAS DECIMALES*/
      proj4.defs("EPSG:25830","+proj=utm +zone=30N +ellps=WGS84 +datum=WGS84 +units=m +no_defs");

      var source = new proj4.Proj('EPSG:25830');  
      var dest = new proj4.Proj('EPSG:4326'); 
      var p = {x: longitude, y: latitude};
      var result = proj4(source, dest, p);
      var conjunto=[result['y'], result['x']];
      var base = new L.TileLayer('http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png',{
        maxZoom: 19,
        subdomains: 'abcd',
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> &copy; <a href="http://cartodb.com/attributions">CartoDB</a>'
      });
      /**** Edición de los markers ****/
      colorIcon = create_marker(color);

      if(i==0){
        /**** Edición del mapa ****/
        map = new L.Map('map', {
          layers: [base],
          center: new L.LatLng(conjunto[0], conjunto[1]),
          zoom: 12,
          fullscreenControl: true,
          fullscreenControlOptions: { // optional
            title:"Pantalla completa",
            titleCancel:"Salir de pantalla"
        }
      });

      /**** Función FULL SCREEN ****/
      map.on('enterFullscreen', function(){
        if(window.console) window.console.log('enterFullscreen');
      });
      map.on('exitFullscreen', function(){
        if(window.console) window.console.log('exitFullscreen');
      });
  }

  /***** Colocación del marker en el mapa *****/
  if (type == 'activity'){

    L.marker([result['y'], result['x']], {icon: colorIcon})
    .bindPopup( "<h4 class='text-center'>Actividad</h4><hr><a href='/activities/" + object.id + "'>" + object.name + "</a>" )
    .addTo(map);
  }else{
    
    L.marker([result['y'], result['x']], {icon: colorIcon})
    .bindPopup( "<h4 class='text-center'>Proyecto</h4><hr><a href='/projects/" + object.id + "'>" + object.name + "</a>" )
    .addTo(map);
  };
}

window.coords_convert = coords_convert


function create_marker(color) {
  return L.icon({
    iconUrl: color,
    iconSize:     [30, 50], // size of the icon
    shadowSize:   [1, 1], // size of the shadow
    iconAnchor:   [10, 50], 
    shadowAnchor: [0, 0],  // the same for the shadow
    popupAnchor:  [-0, -0] 
  });
}

function closeErrores(){
  $('#errores').hide();
}


function closeAvisos(){
  $('#avisos').hide();
}

function confirm_field(object,params){
    if(params.length>0 && object.val()!=""){
        var error=false;
        for(var i=0;i<params.length;i++){
            if(params[i][0].val().trim()==""){
                error=true;
                break;
            }
            switch(params[i][1]){
                case 'e': 
                    if(!/^[\w+\-.]+@[a-z\d\-.]+\.[a-z]+$/.test(params[i][0].val().trim())){
                        error=true;
                        break;
                    }
                    break;
                case 'sm':
                    if(params[i][0].val().trim().length>params[i][2]){
                        error=true;
                        break;
                    }
                    break;
                case 'i':
                    if(!/[6|7|8|9]\d{8}/.test(params[i][0].val().trim())){
                        error=true;
                        break;
                    }
                    break;
            }
        }
        if(error){
            alert("Antes de adjuntar el Archivo, es necesario que cumplimente de forma correcta los datos obligatorios marcados con *")
            return false;
        } else return true;
    }else{
        return true;
    }
}

function actualizarCombos(origen,destino,url ) {
  jQuery.ajax({
    type: "GET",
    dataType: "json",
    url: url,
    success: function(data) {
        $("#" + destino).children().remove();
        $("#" + destino).append("<option value=' ' selected = true></option>");
        $.each(data, function(i, location)
        {
            $("#" + destino).append('<option value=' + '"' + location + '"' + '>' + location + '</option>');
        });
    },
    error: function(data){
        $("#" + destino).children().remove();
    },
    "data":{district: $("#" + origen).val() },
    "async": true,
  });
}

function validateLength(object,length){
  if(object.val().length>length)
    object.val(object.val().substr(0, length));  
}

$(window).on('page:load',function(){
  $(".form-control").datepicker({
    language:'es',
    todayHighlight: false,
    format: 'dd/mm/yyyy',
  });

  if ($("#entity_req_reason_id").val()==4)
      $('#other_motive').show();
  else
      $('#other_motive').hide();

  $("#entity_req_reason_id").change(function(){
      if ($("#entity_req_reason_id").val() == 4)
      {
        $('#other_motive').show();
        $('#entity_other_subscribe_reason').val("");
      }
      else
      {
        $('#other_motive').hide();
        $('#entity_other_subscribe_reason').val("");
      }
  });

  $('#q_addresses_district_eq').on('click', function() {
      actualizarCombos("q_addresses_district_eq","q_addresses_borough_eq",url);
  });

  $('#q_address_district_eq').on('click', function() {
      actualizarCombos("q_address_district_eq", "q_address_borough_eq",url)
  });

  $('#selectOrdenar').on('change', function() {
    if ($('#selectOrdenar').val()!= "")
    {
       jQuery.ajax({
        type: "GET",
        dataType: "script",
        url: 'projects/search',
        "data":{order:$('#selectOrdenar').val() },
        "async": true,
        });
    }
  });
});

function resize(objeto,size){
    var img = new Image();                   
    img.src =objeto.attr("src");
    img.alt =objeto.attr("alt");
                  
    if(img.width>img.height){
        objeto.attr("width",size);
        objeto.attr("height","auto");
    }else{ 
        objeto.attr("height",size);
        objeto.attr("width","auto");
    }
}

function verOcultar(params){
    console.log("ver ocultar");
    var valor=$('#ver').html();
    if (valor=="Ver filtros"){
        $('#ver').html('Ocultar filtros');
        $('#extend_search').val("1");
        $('#searcher-diary').css("visibility", "visible");
        $('#searcher-diary').css("display", "block");
    }else{
        $('#ver').html('Ver filtros');
        $('#extend_search').val("0");
        clear_filter();
    }
   reload_with_params(params);
}

window.verOcultar = verOcultar;

function search_clear_project(){
    $("#search_project").val("");
    clear_filter();
    collapse_filter();
} 

function collapse_filter() {
    if ($('#ver').html()!="Ver filtros"){
        $('#ver').html('Ver filtros');
        $('#extend_search').val("0");
        $('#searcher-diary').css("visibility", "hidden");
        $('#searcher-diary').css("display", "none");
    }
}

function clear_filter(){
    $("#search_district").val("");
    $("#search_date").val("");
    $("#search_borough").val("");
    $('#searcher-diary').css("visibility", "hidden");
    $('#searcher-diary').css("display", "none");
    $('input[name ="q[areas_name_in][]"]').each(function() {
        if ($( this ).prop('checked')){
            $( this ).prop('checked', false);
        }
    });
}

function params_search() {
    return [
        ['extend_search',$('#extend_search'),'i'],
        ['q[name_or_description_cont]',$('#search_project'),'s'],
        ['q[timetables_execution_date_eq]',$('#search_date'),'s'],
        ['q[addresses_district_eq]',$('#search_district'),'m'],
        ['q[addresses_borough_eq]',$('#search_borough'),'m'],
        ['[order]',$('#selectOrdenar'),'m']];
}

window.params_search = params_search;

function submit_form(params){
    reload_with_params(params);
}
window.submit_form = submit_form;

function reload_with_params(parametros){
    var url_params="";
    for(i=0;i<parametros.length;i++){
        var conversion="";
        if(parametros[i][1].length!=0){
            switch(parametros[i][2]){
                case 'i': //Conversión a numérico
                    conversion=parametros[i][1].val().replace(/[ ]/g,"").trim();
                    break;
                case 'b': //Dato simple
                    conversion=parametros[i][1];
                    break;
                case 's': //Conversión a texto
                    conversion=parametros[i][1].val().replace(/[\'%();]/g,"").trim();
                    break;
                case 'm':
                    conversion=parametros[i][1].val();
                    break;
                case 'check': //Obtención de valor booleano
                    if(parametros[i][1].prop('checked')) {
                        conversion=parametros[i][1].prop('checked');
                    }
                    break;
            }
        }
        var conca="";
        if(url_params!="") conca="&";
        url_params=url_params+conca+parametros[i][0]+"="+conversion;
        var union="?";
    }
    
    var check="q[areas_name_in]=";
    
    $('input[name ="q[areas_name_in][]"]:checked').each(
        function() {
            if(check == "q[areas_name_in]=") {
                check="q[areas_name_in]=["
                check = check +"'" + $(this).val()+"'";
            }else {
                check = check +",'" + $(this).val()+"'";
            }
        }
    );

    if (check!="q[areas_name_in]="){
        check = check +"]";
    }

    url_params=url_params+conca+check;
    if(window.location.href.includes(union)) union="&";
    $("#reload").load(window.location.href+union+encodeURI(url_params)+" #reload");
}
