// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.



//= require jquery
//= require rails-ujs
//= require bootstrap-sprockets
//= require cocoon
//= require jquery_nested_form
//= require sweetalert2
//= require sweet-alert2-rails
//= require rails-sweetalert2-confirm
//= require turbolinks
//= require bootstrap
//= require ckeditor-jquery
//= require ckeditor/ckeditor
//= require leaflet
//= require leaflet.fullscreen
//= require proj4
//= require chartkick
//= require Chart.bundle
//= require_tree .



// import 'core-js/stable'
// import 'regenerator-runtime/runtime'
// window.jQuery = $;
// window.$ = $;
// import $ from 'jquery';

// require("@rails/ujs").start()
// require("turbolinks").start()
// require("@nathanvda/cocoon")
// require("@rails/activestorage").start()
// require("@popperjs/core");
// require("channels")
// require("jquery")
// require('webpack-jquery-ui');
// require('webpack-jquery-ui/css');
// require('bootstrap');
// require('./datepicker');
// require('./bootstrap.min');
// require('leaflet');
// // require('leaflet.fullscreen');
// require('chartkick');
// require('./leaflet.fullscreen');
// require('./proj4');
// require('./comon');
//require('./datepicker_es.min');



// import Swal from 'sweetalert2/src/sweetalert2.js'
// import 'sweetalert2/src/sweetalert2.scss'
// window.Swal = Swal;

// import Rails from '@rails/ujs'
// Behavior after click to confirm button
const confirmed = (element, result) => {
    if (result.value) {
        if (!!element.getAttribute('data-remote')) {
            let reloadAfterSuccess = !!element.getAttribute('data-reload');

            $.ajax({
                method: element.getAttribute('data-method') || 'GET',
                url: element.getAttribute('href'),
                dataType: 'json',
                success: function(result) {
                    Swal.fire('Success!', result.message || '', 'success')
                        .then((_result) => {
                            if (reloadAfterSuccess) {
                                window.location.reload();
                            }
                        });
                },
                error: function(xhr) {
                    let title   = 'Error!';
                    let message = 'Something went wrong. Please try again later.';

                    if (xhr.responseJSON && xhr.responseJSON.message) {
                        message = xhr.responseJSON.message;
                    }

                    Swal.fire(title, message,'error');
                }
            });
        } else {
            element.removeAttribute('data-confirm-swal');
            element.click();
        }
    }
};


$(document).on('ready load beforeunload page:load turbolinks:load nested:fieldAdded cocoon:after-insert turbolinks:before-cache', function() {
    $('.ckeditor').each(function() { 
        if ($(this).attr('id') && !$('#cke_'+$(this).attr('id'))) {            
            CKEDITOR.replace($(this).attr('id'));
        }
    });
});



// Display the confirmation dialog
const showConfirmationDialog = (element) => {
    const message = element.getAttribute('data-confirm-swal');
    const text    = element.getAttribute('data-text');

    Swal.fire({
        title:             message || 'Are you sure?',
        text:              text || '',
        showCancelButton:  true,
        confirmButtonText: 'Sí',
        cancelButtonText:  'Cancelar',
    }).then(result => confirmed(element, result));
};

const allowAction = (element) => {
    if (element.getAttribute('data-confirm-swal') === null) {
        return true;
    }

    showConfirmationDialog(element);
    return false;
};

function handleConfirm(element) {
    element.preventDefault();
    if (!allowAction(this)) {
        Rails.stopEverything(element);
    }
}

function send_form() {
    let form = $('.dates_form')  
    $("#set_date").val($("#select_year").val()+"-"+$("#select_month").val()+"-"+$("#select_day").val())
    
    if(!Date.parse($("#set_date").val())) {
        $("#set_date").val($("#select_year").val()+"-"+$("#select_month").val()+"-1")
         $('#select_day').val(1)
    }   
    form.submit()
  }

window.send_form = send_form;

$(document).on('ready load beforeunload page:load turbolinks:load nested:fieldAdded cocoon:after-insert', function(){
    /*$( "input.datepicker" ).datepicker({
        dateFormat: "dd/mm/yy",
        autoSize: true,
        onSelect: function () {
            $(this).trigger('onchange');
            $(this).next('input[type="hidden"]').attr('value', this.value);
        }
    });

    $(".datepicker").datepicker({
        dateFormat: "dd/mm/yy",
        autoSize: true,
        onSelect: function () {
            $(this).trigger('onchange');
            $(this).next('input[type="hidden"]').attr('value', this.value);
        }
    });*/
    $('#search_district').on('change', function() {       
        var union="?";
        $('#boroughts').load(window.location.pathname+union+"search_district="+ $('#search_district').find(":selected").val()+" #boroughts");
    });
    
    $('a[data-confirm-swal]').on('click', handleConfirm);
    $('a[data-confirm]').on('click', handleConfirm);
  
    var day_c =""

    $("#fecha").datepicker({
                  language:'es',
                  todayHighlight: false,
                  format: 'yyyy-mm-dd',
                  beforeShowDay: DisableDays
    });
    
    $("#fecha_home").datepicker({
                  language:'es',
                  todayHighlight: false,
                  format: 'yyyy-mm-dd',
                  beforeShowDay: DisableDays
    });
    
    $("#fecha_show").datepicker({
                  language:'es',
                  todayHighlight: false,
                  format: 'yyyy-mm-dd',
                  beforeShowDay: DisableDays
    });
    
    
    $('#fecha_show').datepicker('setDates', day_c);
    $('#fecha_show').datepicker().on('changeDate', function() {
        actualizarActividades("/activities/" + id,$('#fecha_show').datepicker('getFormattedDate'));
    });
    
    
    $('#fecha_home').datepicker('setDates', day_c);
    $('#fecha_home').datepicker().on('changeDate', function() {
    
        window.location.href = "/activities/?day=" + $('#fecha_home').datepicker('getFormattedDate');
    });
    
    $('#fecha').datepicker('setDates', day_c);
    $('#fecha').datepicker().on('changeDate', function() {
          actualizarActividades("/activities/search",$('#fecha').datepicker('getFormattedDate'));
    
    });
    
    function actualizarActividades(url, parametro) {
                              jQuery.ajax({
                                          type: "GET",
                                          dataType: "script",
                                          url: url,
                                          success: function(data) {
    
    
                                          },
                                          error: function(data){
    
                                              // mostramos mensaje de error
                                          },
                                          "data":{day:parametro },
                                          "async": true,
    
                                   });
    
    }
    
    function DisableDays(date){
               var formatted = date.getFullYear() + "-" + ("0" + (date.getMonth()+1)).slice(-2) + "-" + ("0"+date.getDate()).slice(-2);
               var str = list_days.toString();
               var res = (str.indexOf(formatted,0) >= 0) ? true : false
               return res
    
    }
    
    
    $('#fecha_home .prev').remove();
    $('#fecha_home .next').remove();
    $('#fecha_home thead tr:nth-child(2)').prepend('<th class="next"><i class="fa fa-chevron-right"></i></th>')
    $('#fecha_home thead tr:nth-child(2)').prepend('<th class="prev"><i class="fa fa-chevron-left"></i></th>')
    
    
    $('#fecha .prev').remove();
    $('#fecha .next').remove();
    $('#fecha thead tr:nth-child(2)').prepend('<th class="next"><i class="fa fa-chevron-right"></i></th>')
    $('#fecha thead tr:nth-child(2)').prepend('<th class="prev"><i class="fa fa-chevron-left"></i></th>')
    
    $('#fecha_show .prev').remove();
    $('#fecha_show .next').remove();
    $('#fecha_show thead tr:nth-child(2)').prepend('<th class="next"><i class="fa fa-chevron-right"></i></th>')
    $('#fecha_show thead tr:nth-child(2)').prepend('<th class="prev"><i class="fa fa-chevron-left"></i></th>')
    
    })





    