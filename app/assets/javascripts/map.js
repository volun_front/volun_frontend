

// function load_map ()
// {
//  require([
//      "esri/geometry/Point", "esri/Color", "esri/InfoTemplate","esri/map","esri/dijit/InfoWindowLite",
//      "esri/symbols/SimpleMarkerSymbol",  "esri/layers/ArcGISDynamicMapServiceLayer",
//      "esri/SpatialReference", "esri/layers/GraphicsLayer", "esri/graphic",
//      "esri/layers/ArcGISTiledMapServiceLayer","esri/InfoTemplate","esri/symbols/PictureMarkerSymbol","dojo/dom-construct","dojo/dom", "dojo/on", "dojo/domReady!"],
//      function(Point, Color, InfoTemplate, Map,InfoWindowLite,SimpleMarkerSymbol,  ArcGISDynamicMapServiceLayer,
//             SpatialReference, GraphicsLayer, Graphic, ArcGISTiledMapServiceLayer,InfoTemplate,PictureMarkerSymbol,domConstruct,dom,on)
//      {
//              var picBaseUrl = "/assets/";
//              var sr = new SpatialReference({wkid:25830});
//              var centro =  new Point([446137,4475471],sr);
//              var pmsRed = new PictureMarkerSymbol(picBaseUrl + "project_blue.png", 23, 45).setOffset(0, 15);
//              var pmsOrange = new PictureMarkerSymbol(picBaseUrl + "project_yellow.png", 23, 45).setOffset(0, 15);
//              var pmsGreen = new PictureMarkerSymbol(picBaseUrl + "project_green.png", 23, 45).setOffset(0, 15);
//              if (typeof (map) != "undefined") map.destroy();

//              if (lock == true)
//              {
//                   map = new Map("mapDiv", {
//                      center: centro,
//                      spatialReference: sr,
//                      logo: false,
//                      maxZoom: 4,
//                      smartNavigation: false

//                   });
//                   map.enablePan();
//                   map.showZoomSlider();
//                   map.enableScrollWheelZoom() ;
//                   map.enableMapNavigation();
//                   map.setZoom(2);

//              }
//              else
//              {
//                   centro = new Point([(locations[0].address.latitude/100).toFixed(0),(locations[0].address.longitude/100).toFixed(0)],sr);
//                   map = new Map("mapDiv", {
//                      center: centro,
//                      spatialReference: sr,
//                      logo: false
//                   });
//                   map.enablePan();
//                   map.showZoomSlider();
//                   map.enableScrollWheelZoom() ;
//                   map.enableMapNavigation();
//                   map.setZoom(5);
//                   map.centerAt(centro);
//              }

//              mapabase = new ArcGISTiledMapServiceLayer(url_map ,{"spatialReference": sr});
//              map.addLayer(mapabase);

//              var infoWindow = new InfoWindowLite(null, domConstruct.create("div", null, null, map.root));
//              infoWindow.startup();
//              map.setInfoWindow(infoWindow);
//              map.infoWindow.resize(200, 80);
//              $.each(locations, function(i, location)
//              {

//                  //alert(location.id);

//                   if (location.project!= undefined)  {
//                       //alert(location.project);
//                       //alert(location.address.latitude);
//                       //alert(location.address.longitude);
//                       var pt = new Point((location.address.latitude / 100).toFixed(0), (location.address.longitude / 100).toFixed(0), map.spatialReference);
//                       var graficos = new GraphicsLayer();
//                       map.addLayer(graficos);

//                       if (lock == true) {
//                           if (location.project.project_type_id == 3)
//                               var g = new Graphic(pt, pmsRed);
//                           else if (location.project.project_type_id == 4)
//                               var g = new Graphic(pt, pmsGreen);
//                           else
//                               var g = new Graphic(pt, pmsOrange);
//                           var infoTemplate = new InfoTemplate();
//                           infoTemplate.setTitle("Proyecto");
//                           infoTemplate.setContent("<a href='/projects/" + location.project.id + "'>" + location.project.name + "</a>");
//                           g.setInfoTemplate(infoTemplate);
//                       }
//                       else {
//                           var g = new Graphic(pt, pmsRed);
//                       }

//                       graficos.add(g);
//                   }
//              });




//       })
// }

//  $(document).ready(function(){
//  var map = L.map('map').setView([50.301, 18.654], 13);
//  var mapabase;
//  var rotulacion;
//  load_map();
//  });











// function load_map (){
//   var utm = "+proj=utm +zone=30";
//   var wgs84 = "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs";

//   //proj4.defs('EPSG:25830', '+proj=utm +zone=30 +ellps=GRS80 +units=m +no_defs');
//   //proj4.defs["EPSG:23030"] = "+proj=utm +zone=30 +ellps=intl +units=m +no_defs";

//   var crs25830 = new L.Proj.CRS('EPSG:25830',
//  '+proj=utm +zone=30 +ellps=GRS80 +units=m +no_defs', //http://spatialreference.org/ref/epsg/25830/proj4/
//             {
//                 resolutions: [2048, 1024, 512, 256, 128, 64, 32, 16, 8, 4, 2, 1, 0.5],
//     //Origen de servicio teselado
//     //origin:[0,0]
//   });

//  var crs23030 = new L.Proj.CRS('EPSG:23030',
//  '+proj=utm +zone=30 +ellps=intl +units=m +no_defs', //http://spatialreference.org/ref/epsg/25830/proj4/
//             {
//                 resolutions: [2048, 1024, 512, 256, 128, 64, 32, 16, 8, 4, 2, 1, 0.5],
//     //Origen de servicio teselado
//     //origin:[0,0]
//   });

//   var latlonGEO = "" + proj4(utm,wgs84,[440389.35,4481736.16])
//   var arrGEO = latlonGEO.split(",", 2);
//   var map = L.map( 'map', {
//     minZoom: 2,
//     maxZoom: 18,
//     crs: crs23030,
//     center: [ arrGEO[1], arrGEO[0] ], // coordenadas etrs89
//     zoom: 12,

//   });


// var layer = L.tileLayer( 'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}',
//   {
//     attribution: 'openstreetmap',
//     id: 'mapbox.streets',
//     accessToken: 'pk.eyJ1IjoiaWFtZGVzYXJyb2xsbyIsImEiOiJjamNyaXQxamYwN3V0MndwaGd0Y3FzMGowIn0.MAPWA_aWdS9hFPh6ynwlCA',
//     continuousWorld: true,
//     crs: crs23030
//   }).addTo(map);

// /*
//   var layer = L.tileLayer( 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
//   {
//     attribution: 'openstreetmap',
//     subdomains: ['a','b','c'],
//     continuousWorld: true,
//     crs: crs23030
//   }).addTo(map);

// */

// /*
//   var layerRasterIgn = new L.tileLayer.wms("http://www.ign.es/wms-inspire/mapa-raster",
//     {
//        layers: 'mtn_rasterizado',
//        crs: crs23030,
//        format: 'image/png',
//        continuousWorld: true,
//        transparent: true,
//        attribution: '© Instituto Geográfico Nacional de España'
// }).addTo(map);

// */

//   var myIcon = L.icon({
//     iconUrl: '/assets/images/project_green.png',
//     iconSize: [20, 20],
//     iconAnchor: [22, 94],
//     popupAnchor: [-3, -76],
//     shadowUrl: '/assets/GreenPin1LargeB.png',
//     shadowSize: [0, 0],
//     shadowAnchor: [22, 94]
// });

//   var greenIcon = L.icon({
//     iconUrl: 'leaf-green.png',
//     shadowUrl: 'leaf-shadow.png',

//     iconSize:     [10, 10], // size of the icon
//     shadowSize:   [1, 1], // size of the shadow
//     iconAnchor:   [10, 10], // point of the icon which will correspond to marker's location
//     shadowAnchor: [0, 0],  // the same for the shadow
//     popupAnchor:  [-0, -0] // point from which the popup should open relative to the iconAnchor
// });

//  $.each(locations, function(i, location)
//  {
//     var latitude = (location.address.latitude/100)
//     var longitude = (location.address.longitude/100)
//     latlonGEO = "" + proj4(utm,wgs84,[latitude,longitude])
//     arrGEO = latlonGEO.split(",", 2);
//     latitude = arrGEO[1];
//     longitude = arrGEO[0];
//     if (latitude != null && longitude!= null)
//     {
//       L.marker([latitude, longitude], {icon: myIcon}).addTo(map);
//       //L.marker([latitude,longitude], {icon: myIcon}).addTo(map).bindPopup('prueba');
//     }
//     //L.marker([40.41806830000001 , -3.6752301000000216], {icon: myIcon}).addTo(map);

//  });



// }


