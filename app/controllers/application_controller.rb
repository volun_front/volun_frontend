class ApplicationController < ActionController::Base
  require "application_responder"
  require 'openssl'
  require 'base64'
  require 'digest/md5'
  include Response

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  before_action :authenticate_http_basic, if: -> {http_basic_auth_site?}
  before_action :set_locale
  before_action :set_return_url
  before_action :authenticate_NIF
  before_action :load_rrss
  self.responder = ApplicationResponder
  protect_from_forgery with: :exception
  respond_to :html

  helper_method :use_devise_authentication?, :cast_as_boolean

  def cast_as_boolean(boolean_string)
    ActiveRecord::Type::Boolean.new.cast(boolean_string)
  end

  def user_authenticated?
    session[:uweb_user_data] = {} if use_devise_authentication?
    current_user.present?
  end

  def use_devise_authentication?
    cast_as_boolean Setting.where(key: "VolunBackend.devise_auth").pluck(:value).first.presence
  end

  def log_error(code,e)
    begin
      Rails.logger.error("#{code}: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def load_rrss
    @rrss_facebook=Setting.find_by(key: 'RRSS.facebook').try(:value)
    @rrss_twitter=Setting.find_by(key: 'RRSS.twitter').try(:value)
    @rrss_instagram=Setting.find_by(key: 'RRSS.instagram').try(:value)
    @rrss_youtube=Setting.find_by(key: 'RRSS.youtube').try(:value)
    @cvcontact_telefono = Setting.find_by(key: "CVContact.telefono").try(:value)
    @cvcontact_email = Setting.find_by(key: "CVContact.email").try(:value)
    @component_home = VirtualCommunity::Component.find_by(denomination: 'Homepage')
  end

  private

    def authenticate!
      if use_devise_authentication?
        redirect_to new_user_session_path unless devise_controller?
      else
        render file: 'public/401.html', status: :unauthorized unless uweb_authenticated?
      end
    end

    def authenticate_http_basic   
      authenticate_or_request_with_http_basic do |username, password|
        username == Rails.application.secrets.http_basic_username && password == Rails.application.secrets.http_basic_password
      end
    end

    def authenticate_NIF
      return if (params[:DNI].blank? && params[:login].blank?) || params[:login].to_s!="PORTAL" || !current_user.blank?
    
      begin
        dni=Criptografia.new.decrypt('controlhorario',params[:DNI])
        exists = Volunteer.find_by(id_number: dni)
      rescue 
        exists = nil
      end

      if !exists.blank?
        user=User.where("loggable_id=#{exists.id} AND loggable_type='Volunteer'")[0]
        flash[:alert]= I18n.t('message_login.login_success')

        sign_in(user)
        redirect_to user_path(user)
      else
       flash[:alert]= I18n.t('message_login.no_login')
       redirect_to volunteers_path
      end
    end

    def current_user
      @current_user= super
    end

    def http_basic_auth_site?
      Rails.application.secrets.http_basic_auth
    end

    def set_locale
      if params[:locale] && I18n.available_locales.include?(params[:locale].to_sym)
        session[:locale] = params[:locale]
      end
      session[:locale] ||= I18n.default_locale
      locale = session[:locale]
      I18n.locale = locale
    end

    def set_layout
      if devise_controller?
        'devise'
      else
        'application'
      end
    end

    def set_return_url
      if !devise_controller? && controller_name != 'welcome' && is_navigational_format?
        store_location_for(:user, request.path)
      end
    end

    def set_page_params
      params[:per_page_list] ||= [10, 20, 30, 40, 50]
      params[:per_page] ||= 6
    end

  protected

    def check_auth
      unless user_signed_in?
        redirect_to new_user_session_path(sending_params)
      end
    end

    def sending_params
      params.permit(:username, :password)
    end
end
