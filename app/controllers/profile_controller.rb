class ProfileController < ApplicationController
    require 'rubygems'
    require 'zip'
  
    before_action :authenticate_user!
    respond_to :html, :js, :json
end