class EntitiesController < AuditGeneratesController
  include ActionView::Helpers::UrlHelper 
  invisible_captcha only: [:create], honeypot: :subtitle
  before_action :load_data

  def new
    @entity = Entity.new
    @entity.build_address
    @entity.build_user
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def create
    @entity = Entity.find_by_vat_number params[:entity][:vat_number]
    key=ActiveRecord::Base.connection.exec_query("SELECT value FROM settings WHERE key = 'VolunFrontend.cifAytoMadrid'")
    cifAytoMadrid = ''
    key.each do|k|
      cifAytoMadrid = k["value"]
    end
      cif_ayto = params[:entity][:vat_number].gsub('-', '').delete(' ').upcase
      if @entity && !cifAytoMadrid.blank? && (cif_ayto != cifAytoMadrid.to_s)
        if @entity.user.blank?
          @entity.build_user
          @entity.build_address if @entity.address.blank?
          set_user
          @attr[:old_data]= JSON.parse(@entity.attributes.to_json).except("created_at", "updated_at")
          if @entity.update(entity_params)
            if current_user.blank? 
              current = nil
            else 
              current = current_user.id
            end
            @attr[:operation_type] = "#{I18n.t('audit.update')}" 
            @attr[:operation] = "#{I18n.t('audit_enum.update', id: @entity.id)}"
            @attr[:new_data] = JSON.parse(@entity.attributes.to_json).except("created_at", "updated_at")
            AuditGenerate.generate(@attr)

            redirect_to entities_path, notice: t('entity_subscribe.response')
          else
            respond_with(@entity)
          end
        else
          redirect_to entities_path, notice: t('entity_subscribe.response',:href=> link_to(t('entity_subscribe.link'),new_confirmation_path('user') )).html_safe 
        end #'https://voluntariospormadrid.madrid.es/users/confirmation/new'
      else
        @entity = Entity.new(entity_params)
        set_user
        if @entity.save
          if current_user.blank?
            current = nil
          else 
            current = current_user.id
          end
          @attr[:operation_type] = "#{I18n.t('audit.register')}" 
          @attr[:operation] = "#{I18n.t('audit_enum.register', id: @entity.id)}"
          @attr[:new_data] = JSON.parse(@entity.attributes.to_json).except("created_at", "updated_at")
          AuditGenerate.generate(@attr) 

          redirect_to entities_path, notice: t('entity_subscribe.response')
        else
          respond_with(@entity)
        end
      end
  rescue => e
    begin
      Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  protected

    def entity_params
      params
        .require(:entity)
        .permit(
          :name,
          :description,
          :vat_number,
          :email,
          :representative_name,
          :representative_last_name,
          :representative_last_name_alt,
          :contact_name,
          :contact_last_name,
          :contact_last_name_alt,
          :phone_number,
          :phone_number_alt,
          :publish_pictures,
          :annual_survey,
          :req_reason_id,
          :entity_type_id,
          :comments,
          :other_subscribe_reason,
          :active,
          :subscribed_at,
          :unsubscribed_at,
          {
            address_attributes: [
              :id,
              :road_type,
              :road_name,
              :road_number_type,
              :road_number,
              :grader,
              :stairs,
              :floor,
              :door,
              :postal_code,
              :borough,
              :district,
              :borough_id,
              :district_id,
              :town,
              :province,
              :province_id,
              :country,
              :normalize,
              :_destroy
            ]
          },
          {
            user_attributes: [
              :id,
              :password,
              :password_confirmation
            ]
          }
        )
    end

  private
    def audit_data
      @attr= {
        operation_type: '', 
        user_id: current_user.try(:id),
        resource_id: Resource.find_by(name: Entity.name).try(:id),
        operation: '',
        new_data: '',
        old_data: ''
      }
    end
    def set_user
      @entity.address.normalize = false
      @entity.user.email = @entity.email
      @entity.user.login = @entity.email
      @entity.user.notice_type_id = 1
      @entity.user.terms_of_service = true
      @entity.user.confirmed_at = Time.zone.now
    end

    def load_data
      @component=VirtualCommunity::Component.find_by(denomination: 'Hazte entidad colaboradora')
    end
end
