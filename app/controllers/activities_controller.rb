class ActivitiesController < ApplicationController
  before_action :authenticate_user!, only: :my_area
  before_action :load_query_search, only: :search
  before_action :set_activity, only: :show
  before_action :set_list_days_show, only: :show
  before_action :set_locations, only: :show
  before_action :set_areas, only: :index
  before_action :set_districts, only: :index
  before_action :set_boroughs, only: :index
  before_action :set_list_days, only: :index
  before_action :load_query, only: :index
  before_action :search_complete, only: [:index,:search]

  before_action :load_data_calendar, only: [:index, :show]
  respond_to :html, :js, :json

  def my_area
    params[:q] ||= Activity.ransack_default
    @search_q = Activity.all
    @activities = @search_q.result.page(params[:page])
    respond_to do |format|
      format.html
      format.js
    end
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def index
    Ahoy::Event.create(name: "Visit", properties: {title: "Actividades"}.to_json, user_id: current_user.try(:id), visit_id: current_visit.try(:id), time:Time.now)
    @activities_search = @activities_unpag
    
    if !params[:search_text].to_s.strip.blank?
      @activities_search = @activities_search.where("TRANSLATE(UPPER(name),'ÁÉÍÓÚ','AEIOU') LIKE TRANSLATE(UPPER(?),'ÁÉÍÓÚ','AEIOU') OR TRANSLATE(UPPER(description),'ÁÉÍÓÚ','AEIOU') LIKE TRANSLATE(UPPER(?),'ÁÉÍÓÚ','AEIOU')","%#{params[:search_text]}%","%#{params[:search_text]}%")
    end

    if !params[:search_district].to_s.strip.blank?
      @activities_search = @activities_search.where("id in (?)",Activity.joins(:addresses).where("addresses.district_id = ?", params[:search_district]).select(:id))
    end

    if !params[:search_borought].to_s.strip.blank?
      @activities_search = @activities_search.where("id in (?)",Activity.joins(:addresses).where("addresses.borought_id = ?", params[:search_borought]).select(:id))
    end
    
    if !params[:search_areas].blank?     
      @activities_search = @activities_search.where("id in (?)", Activity.joins(:areas).where("areas.id in (?)",params[:search_areas].to_s.gsub(/\"|\[|\]/,'').strip.split(',')).select(:id))
    end

    if !params[:select_check].blank? && params[:select_check].to_s=='day'
      @activities_search = @activities_search.where(" cast(? as date) =cast(start_date as date) OR cast(? as date) = cast(end_date as date) OR  cast(? as date) between cast(start_date as date) and cast(end_date as date)",@current_date_calendar,@current_date_calendar, @current_date_calendar)
    else
      start_date =  @current_date_calendar.beginning_of_month.to_date
      end_date=  @current_date_calendar.end_of_month.to_date
      @activities_search = @activities_search.where("start_date::date between ?::date and ?::date OR end_date::date between ?::date and ?::date OR ?::date between start_date::date and end_date::date OR ?::date between start_date::date and end_date::date ",start_date,end_date,start_date,end_date, start_date,end_date ) 
    end


    @num_records = @activities_search.count
  
    @activities = @activities_search.paginate(page: params[:page], per_page: Kaminari.config.default_per_page)
    respond_to do |format|
      format.html
      format.js { render action: 'search.js.erb' }
    end
  rescue => e
    begin
      Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def search
    respond_to do |format|
      format.js
    end
  rescue => e
    begin
      Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def boroughs
    @boroughs = Activity.includes(:addresses).distinct.where('addresses.district=?', params[:district]).order('addresses.borough').pluck('addresses.borough', 'addresses.borough')
    respond_to do |format|
      format.json { render json: @boroughs }
    end
  rescue => e
    begin
      Rails.logger.error("COD-00004: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def show
   
    @timetables = @activity.timetables.where(timetables: { execution_date: params[:day] })
    @day = params[:day].to_json
    respond_to do |format|
      format.html
      format.js
    end
  rescue => e
    begin
      Rails.logger.error("COD-00005: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  protected

    def search_complete
      timetables_aux = @search_q.result

      @num_records = timetables_aux.length
      @timetables = timetables_aux.paginate(page: params[:page], per_page: Kaminari.config.default_per_page)
    end

    def load_query
      if params[:day]
        @search_q = Timetable.select('execution_date,activities.id, activities.name').joins(:activity).group('execution_date, activities.id').search(execution_date_eq: params[:day])
        @day = params[:day]
      else
        @search_q = Timetable.select('execution_date, activities.id').joins(:activity).order(:execution_date).group('execution_date, activities.id').search(execution_date_gteq: Time.now)
        @day = nil
      end
    end

    def load_query_search
      if params[:day]
        @day = params[:day]
        @search_q = Timetable.select('execution_date, activities.id, activities.name').joins(:activity).order(:execution_date).group('execution_date, activities.id').search(execution_date_eq: @day)
      else
        @day = nil
        @search_q = Timetable.select('execution_date, activities.id, activities.name').joins(:activity).order(:execution_date).group('execution_date, activities.id').search(params[:q])
      end
    end

    def set_list_days_show
      @list_days_activity = @activity.timetables.pluck('timetables.execution_date').to_json
    end

    def set_list_days
      @list_days = Activity.includes(:timetables).distinct.activities_present(Time.now).order('timetables.execution_date').pluck('timetables.execution_date').to_json
    end

    def set_locations
      params[:day] ||= @activity.timetables.minimum(:execution_date).try :strftime, '%Y-%m-%d'
      @locations = Event.includes(:address, :activity, :timetables).where(activities: { id: params[:id] }, timetables: { execution_date: params[:day] }).as_json(only: [:id], include: { address: { only: [:latitude, :longitude] } })
    end

    def set_boroughs
      @boroughts = Borought.where("id in (?)",  Activity.joins(:addresses).select(:borought_id))    
    end

    def set_areas
      @areas = Area.all.order(:name)
    end

    def set_districts
      @districts = District.where("id in (?)",  Activity.joins(:addresses).select(:district_id))
    end

    def set_activity
      @activity = Activity.find(params[:id])
    rescue
      redirect_to activities_path, alert: "No existe la actividad"
    end

    def activity_params
      params.require(:activity).permit(:name, :description, :active)
    end

    def allow_iframe
      response.headers.delete 'X-Frame-Options'
    end

  private

  def load_data_calendar
    @current_date_calendar ||= Date.today   
    if !params[:set_date].blank?
      begin
        @current_date_calendar = Date.parse(params[:set_date])    
      rescue
        convert_date=params[:set_date].split('-')
        begin
          params[:set_date]="#{convert_date[0]}-#{convert_date[1]}-1"
          @current_date_calendar = Date.parse("#{convert_date[0]}-#{convert_date[1]}-1")  
        rescue
          @current_date_calendar=Date.today
        end
      end
    end

    start_date =  @current_date_calendar.beginning_of_month.to_date
    end_date=  @current_date_calendar.end_of_month.to_date
    @activities_unpag = Activity.all.order(start_date: :desc).where("start_date::date between ?::date and ?::date OR end_date::date between ?::date and ?::date OR ?::date between start_date::date and end_date::date OR ?::date between start_date::date and end_date::date ",start_date,end_date,start_date,end_date, start_date,end_date ) 
  end
end
