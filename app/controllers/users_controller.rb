class UsersController < AdminController
  require 'rubygems'
  require 'zip'

  before_action :authenticate_user!
  respond_to :html, :js, :json
 
  protected

    def user_params
      params.require(:user).permit(:locale, :profileable_id)
    end

    def valid_access?
      authorized_current_user?
    end

    def authorized_current_user?
      @authorized_current_user ||= current_user
    end
end
