class NewCampaingsController < ApplicationController

    def index
      Ahoy::Event.create(name: "Visit", properties: {title: "Campañas"}.to_json, user_id: current_user.try(:id), visit_id: current_visit.try(:id), time:Time.now)
      @new_campaings = NewCampaing.all
      params[:q] ||= NewCampaing.ransack_default
      @search_q = @new_campaings.search(params[:q])
      @new_campaings = @search_q.result.paginate(page: params[:page], per_page: params[:per_page]||15)
      respond_with(@new_campaings)      
    rescue => e
      begin
        Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def show
      @new_campaing = NewCampaing.find(params[:id])
      if @new_campaing.blank? || !@new_campaing.try(:publicable)
        redirect_to new_campaings_path, alert: I18n.t('mensaje_erro_campana')
      end
    rescue => e
      begin
        Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
      redirect_to new_campaings_path, alert: "No existe la campaña"
    end
  end
  