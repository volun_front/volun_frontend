class Profile::UsersController < ProfileController
  before_action :load_requests, only: [:index]
  before_action :load_projects, only: [:index]
  before_action :load_meetings, only: [:index]
  before_action :load_activities, only: [:index]
  before_action :load_projects_digitals, only: [:index]

  def index
    Ahoy::Event.create(name: "Visit", properties: {title: "Area Personal"}.to_json, user_id: current_user.try(:id), visit_id: current_visit.try(:id), time:Time.now)

    if current_user.loggable_type.to_s == "Volunteer"
      @hash_fechas={}
      @hash_projects={}
      v_trackings =  Volun::Tracking.joins(:tracking_type).where("volun_trackings.volunteer_id = ? and tracking_types.alias_name = 'subscribe' and volun_trackings.project_id IS NOT NULL",current_user.loggable_id).order("volun_trackings.tracked_at asc")
      v_trackings.find_each do |track|
        project = Project.find_by(id: track.project_id)
      
        if !project.blank?
          @hash_projects.merge!({project.name =>project.id  })
          project.albums.find_each do |album|
            if !album.year.blank? && @hash_fechas[album.year].blank?
              @hash_fechas.merge!({album.year => album.year })
            end
          end
        end
      end

      volun = Volunteer.find_by(id: current_user.loggable_id)
      volun.projects.find_each do |project|
        @hash_projects.merge!({project.name =>project.id})
        project.albums.find_each do |album|
          if !album.year.blank? && @hash_fechas[album.year].blank?
            @hash_fechas.merge!({album.year => album.year })
          end
        end
      end
    end
  rescue => e
    log_error("COD-00001",e) 
  end

  def download_zip    
    proyect_year = params[:search_proyect]
    proyect = params[:search_proyect_id]

    archives = []
    if proyect.blank?
      volun = Volunteer.find( current_user.loggable_id)
      projects= volun.projects 

      projects.each do |project|      
        project.albums.each do |album|          
          if proyect_year.blank? || album.year.to_i == proyect_year.to_i
            album.links.each do |link|
              aux_path = link.path.split('?')
              archives.push({filename: link.file_file_name, path: aux_path[0] })
            end
          end
        end
      end
    else
      project = Project.find(proyect.to_i)
      project.albums.each do |album|
        if proyect_year.blank? || album.year.to_i == proyect_year.to_i
          album.links.each do |link|
            aux_path = link.path.split('?')
            archives.push({filename: link.file_file_name, path: aux_path[0] })
          end
        end
      end
    end

    if archives.count > 0 && !archives.blank?
      data = 0
      zipfile_name = "archive_#{current_user.loggable_id}.zip"
      FileUtils.mkdir_p 'temporary_dir'
      Tempfile.open(zipfile_name, 'temporary_dir') do |tempfile|
        Zip::File.open(tempfile.path, Zip::File::CREATE) do |zipfile|
          archives.each do |arch|
            if File.exist?("public"+arch[:path])
              zipfile.add(arch[:filename],"public#{arch[:path]}") 
              data = 1
            end
          end
        end
        if data.to_i == 1
          send_file(tempfile.path, type: 'application/zip',:disposition => 'attachment', :filename => zipfile_name)
        else
          redirect_to myarea_path, alert: I18n.t('zipzip.no_data')
        end
      end
    else
      redirect_to myarea_path, alert: I18n.t('zipzip.no_data')
    end
  rescue => e
    log_error("COD-00002",e) 
  end

  protected

  def load_projects
    if valid_access? && (params[:type].blank? || params[:type].to_s=="myProjects")
      if current_user.loggable_type == Entity.name
        @projects_actives = Project.all.where(entity_id: current_user.loggable_id )
      else
        @projects_actives = Project.joins("INNER JOIN projects_volunteers as pv on pv.project_id=projects.id ").where("pv.volunteer_id = ?", current_user.loggable_id )
      end
    else
      @projects_actives=[]
    end
  end

  def load_projects_digitals
    if valid_access? && params[:type].to_s=="myProjectsDigitals" && current_user.loggable_type.to_s=="Volunteer"
      @projects_digitals = Project.joins("INNER JOIN projects_volunteers as pv on pv.project_id=projects.id ").where("pv.volunteer_id = ? AND (projects.permit_debate=true OR projects.permit_faq=true)", current_user.loggable_id )
    else
      @projects_digitals=[]
    end
  end

  def load_meetings
    if valid_access? && params[:type].to_s=="myMeetings" && current_user.loggable_type.to_s=="Volunteer"
      @meetings = Meeting.where("id IN (?)", VolunProyMeeting.where("confirm = true AND projects_volunteer_id IN (?)", ProjectsVolunteer.where(volunteer_id: current_user.loggable_id).select(:id)).select(:meeting_id))
    else
      @meetings=[]
    end
  end

  def load_activities
    if valid_access? && params[:type].to_s=="myActivities" && current_user.loggable_type.to_s=="Entity"
      @activities = Activity.unscoped.where(entity_id: current_user.loggable_id)
    else
      @activities = []
    end
  end

  def load_requests
    if valid_access? && params[:type].to_s=="myRequests"
      @requests = RequestForm.unscoped.where(user_id: current_user.id)
    else
      @requests = []
    end
  end

  def valid_access?
    authorized_current_user?
  end

  def authorized_current_user?
    @authorized_current_user ||= current_user
  end
  
end
