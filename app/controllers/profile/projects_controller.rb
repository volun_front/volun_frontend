class Profile::ProjectsController < ProfileController
  before_action :load_project, except: [:vote_comment_down,:vote_comment_up]
  before_action :load_topic, except: [:show,:faq,:topic,:new_topic,:create_topic]
  before_action :load_comment, only: [:vote_comment_down,:vote_comment_up]
  before_action :permited_faq, only: [:faq]
  before_action :permited_debate, except: [:faq, :show]
  

  def show
  rescue => e
    log_error("COD-00001",e) 
  end

  def faq
    @digital_faqs = @project.digital_faqs.active.order(order: :asc)
  rescue => e
    log_error("COD-00002",e) 
  end

  def topic
    @digital_topics= @project.digital_topics.active

    if !params[:search_text].blank?
      @digital_topics = @digital_topics.where("translate(UPPER(cast(digital_topics.title as varchar)), 'ÁÉÍÓÚ', 'AEIOU') LIKE translate(UPPER(cast(? as varchar)), 'ÁÉÍÓÚ', 'AEIOU') OR translate(UPPER(cast(digital_topics.body as varchar)), 'ÁÉÍÓÚ', 'AEIOU') LIKE translate(UPPER(cast(? as varchar)), 'ÁÉÍÓÚ', 'AEIOU')", "%#{params[:search_text]}%","%#{params[:search_text]}%")
    end
    if !params[:search_type].blank?
      @digital_topics = @digital_topics.where("id #{params[:search_type].to_s == "unfollow" ? "not" : ""} in (?)", Digital::Follow.where(userable: current_user).select(:digital_topic_id))
    end
  rescue => e
    log_error("COD-00003",e)
  end

  def new_topic
    @digital_topic = Digital::Topic.new(project: @project)
  rescue => e
    log_error("COD-00004",e) 
  end

  def create_topic
    @digital_topic = Digital::Topic.new(digital_topic_params)
    @digital_topic.project = @project
    @digital_topic.moderate_status = ModerateStatus.find_by(code: 'pending')

    if @digital_topic.save
      redirect_to topic_digital_path(@project), notice: "Se ha guardado correctamente el foro de debate"
    else
      render :new_topic
    end
  rescue => e
    log_error("COD-00005",e) 
  end

  def vote_topic_up
    exist = Digital::Vote.find_by(userable: current_user, votable: @digital_topic)
    if exist.blank? 
      vote = Digital::Vote.new(userable: current_user, votable: @digital_topic, value: 1)
      if vote.save
        if params[:show].blank?
          redirect_to topic_digital_path(@project), notice: "Has votado el foro de debate"
        else
          redirect_to topic_show_digital_path(@project,vote), notice: "Has votado el foro de debate"
        end
      else
        if params[:show].blank?
          redirect_to topic_digital_path(@project), alert: "No puedes realizar el voto"
        else
          redirect_to topic_show_digital_path(@project,vote), alert: "No puedes realizar el voto"
        end
      end
    elsif exist.value < 0
      exist.value = 1
      if exist.save
        if params[:show].blank?
          redirect_to topic_digital_path(@project), notice: "Has votado el foro de debate"
        else
          redirect_to topic_show_digital_path(@project,exist), notice: "Has votado el foro de debate"
        end
      else
        if params[:show].blank?
          redirect_to topic_digital_path(@project), alert: "No puedes realizar el voto"
        else
          redirect_to topic_show_digital_path(@project,exist), alert: "No puedes realizar el voto"
        end
      end
    else
      if params[:show].blank?
        redirect_to topic_digital_path(@project), alert: "Ya has votado el foro de debate"
      else
        redirect_to topic_show_digital_path(@project,exist), alert: "Ya has votado el foro de debate"
      end
    end
  rescue => e
    log_error("COD-00006",e) 
  end

  def vote_topic_down
    exist = Digital::Vote.find_by(userable: current_user, votable: @digital_topic)
    if exist.blank? 
      vote = Digital::Vote.new(userable: current_user, votable: @digital_topic, value: -1)
      if vote.save
        if params[:show].blank?
          redirect_to topic_digital_path(@project), notice: "Has votado el foro de debate"
        else
          redirect_to topic_show_digital_path(@project,@digital_topic), notice: "Has votado el foro de debate"
        end
      else
        if params[:show].blank?
          redirect_to topic_digital_path(@project), alert: "No puedes realizar el voto"
        else
          redirect_to topic_show_digital_path(@project,@digital_topic), alert: "No puedes realizar el voto"
        end
      end
    elsif exist.value > 0
      exist.value = -1
      if exist.save
        if params[:show].blank?
          redirect_to topic_digital_path(@project), notice: "Has votado el foro de debate"
        else
          redirect_to topic_show_digital_path(@project,@digital_topic), notice: "Has votado el foro de debate"
        end
      else
        if params[:show].blank?
          redirect_to topic_digital_path(@project), alert: "No puedes realizar el voto"
        else
          redirect_to topic_show_digital_path(@project,@digital_topic), alert: "No puedes realizar el voto"
        end
      end
    else
      if params[:show].blank?
        redirect_to topic_digital_path(@project), alert: "Ya has votado el foro de debate"
      else
        redirect_to topic_show_digital_path(@project,@digital_topic), alert: "Ya has votado el foro de debate"
      end
    end
  rescue => e
    log_error("COD-00007",e) 
  end

  def follow_topic_up
    exist = Digital::Follow.find_by(userable: current_user, digital_topic: @digital_topic)
    if exist.blank?
      follow = Digital::Follow.new(userable: current_user, digital_topic: @digital_topic)
      if follow.save
        if params[:show].blank?
          redirect_to topic_digital_path(@project), notice: "Estás siguiendo el foro de debate"
        else
          redirect_to topic_show_digital_path(@project,@digital_topic), notice: "Estás siguiendo el foro de debate"
        end
      else
        if params[:show].blank?
          redirect_to topic_digital_path(@project), alert: "No puedes seguir el debate indicado"
        else
          redirect_to topic_show_digital_path(@project,@digital_topic), alert: "No puedes seguir el debate indicado"
        end
      end
    else
      if params[:show].blank?
        redirect_to topic_digital_path(@project), alert: "Ya sigues el foro de debate"
      else
        redirect_to topic_show_digital_path(@project,@digital_topic), alert: "Ya sigues el foro de debate"
      end
    end
  rescue => e
    log_error("COD-00008",e) 
  end

  def follow_topic_down
    exist = Digital::Follow.find_by(userable: current_user, digital_topic: @digital_topic)
    if !exist.blank?     
      if exist.destroy
        if params[:show].blank?
          redirect_to topic_digital_path(@project), notice: "Has dejado de seguir el foro de debate"
        else
          redirect_to topic_show_digital_path(@project,@digital_topic), notice: "Has dejado de seguir el foro de debate"
        end
      else
        if params[:show].blank?
          redirect_to topic_digital_path(@project), alert: "No puedes dejar de seguir el debate indicado"
        else
          redirect_to topic_show_digital_path(@project,@digital_topic), alert: "No puedes dejar de seguir el debate indicado"
        end
      end
    else
      if params[:show].blank?
        redirect_to topic_digital_path(@project), alert: "Ya has dejado de seguir el foro de debate"
      else
        redirect_to topic_show_digital_path(@project,@digital_topic), alert: "Ya has dejado de seguir el foro de debate"
      end
    end
  rescue => e
    log_error("COD-00009",e) 
  end

  def topic_show
    @digital_comments = @digital_topic.digital_comments.where(digital_comment: nil).active.order(created_at: :desc)
    if !params[:search_text].blank?
      @digital_comments = @digital_comments.where("translate(UPPER(cast(digital_comments.body as varchar)), 'ÁÉÍÓÚ', 'AEIOU') LIKE translate(UPPER(cast(? as varchar)), 'ÁÉÍÓÚ', 'AEIOU')", "%#{params[:search_text]}%")
    end
    @digital_comments= @digital_comments.paginate(page: params[:page], per_page: 20)
  rescue => e
    log_error("COD-00010",e) 
  end

  def create_comment
    @digital_comment = Digital::Comment.new(digital_comment_params)
    @digital_comment.userable = current_user
    @digital_comment.digital_topic = @digital_topic
    @digital_comment.moderate_status = ModerateStatus.find_by(code: 'pending')

    if @digital_comment.save
      redirect_to  topic_show_digital_path(@project,@digital_topic), notice: "Has comentado el debate"
    else
      redirect_to  topic_show_digital_path(@project,@digital_topic), alert: "No se ha podido añadir el comentario al debate"
    end
  rescue => e
    log_error("COD-00011",e) 
  end

  def vote_comment_up
    exist = Digital::Vote.find_by(userable: current_user, votable: @digital_comment)
    if exist.blank? 
      vote = Digital::Vote.new(userable: current_user, votable: @digital_comment, value: 1)
      if vote.save        
        redirect_to topic_show_digital_path(@digital_topic.project,@digital_topic), notice: "Has votado el comentario"
      else       
        redirect_to topic_show_digital_path(@digital_topic.project,@digital_topic), alert: "No puedes realizar el voto"     
      end
    elsif exist.value < 0
      exist.value = 1
      if exist.save        
        redirect_to topic_show_digital_path(@digital_topic.project,@digital_topic), notice: "Has votado el comentario"
      else        
        redirect_to topic_show_digital_path(@digital_topic.project,@digital_topic), alert: "No puedes realizar el voto"
      end
    else
      redirect_to topic_show_digital_path(@digital_topic.project,@digital_topic), alert: "Ya has votado el comentario"      
    end
  rescue => e
    log_error("COD-00012",e) 
  end

  def vote_comment_down
    exist = Digital::Vote.find_by(userable: current_user, votable: @digital_comment)
    if exist.blank? 
      vote = Digital::Vote.new(userable: current_user, votable: @digital_comment, value: -1)
      if vote.save        
        redirect_to topic_show_digital_path(@digital_topic.project,@digital_topic), notice: "Has votado el comentario"
      else       
        redirect_to topic_show_digital_path(@digital_topic.project,@digital_topic), alert: "No puedes realizar el voto"     
      end
    elsif exist.value > 0
      exist.value = -1
      if exist.save        
        redirect_to topic_show_digital_path(@digital_topic.project,@digital_topic), notice: "Has votado el comentario"
      else        
        redirect_to topic_show_digital_path(@digital_topic.project,@digital_topic), alert: "No puedes realizar el voto"
      end
    else
      redirect_to topic_show_digital_path(@digital_topic.project,@digital_topic), alert: "Ya has votado el comentario"      
    end
  rescue => e
    log_error("COD-00013",e) 
  end

  private

  def digital_topic_params
    params.require(:digital_topic).permit(:id,:title,:body)
  end

  def digital_comment_params
    params.require(:digital_comment).permit(:id,:body,:digital_comment_id)
  end


  def load_project
    @project = Project.find_by(id: params[:id])
    if @project.blank? || !@project.blank? && @project.volunteers.where(id: current_user.loggable_id).count <= 0 || current_user.loggable_type.to_s != "Volunteer"
      redirect_to myarea_path, alert: "No existe el proyecto asociado"
    end
  rescue
    redirect_to myarea_path, alert: "No existe el proyecto asociado"
  end

  def load_topic
    @digital_topic = Digital::Topic.active.find_by(id: params[:topic_id])
    project = @digital_topic.try(:project)
    if @digital_topic.blank? || !project.blank? && project.volunteers.where(id: current_user.loggable_id).count <= 0 || current_user.loggable_type.to_s != "Volunteer"
      redirect_to myarea_path, alert: "No existe el proyecto asociado"
    end
  rescue
    redirect_to myarea_path, alert: "No existe el proyecto asociado"
  end

  def load_comment
    @digital_comment = Digital::Comment.active.find_by(id: params[:comment_id])
  end

  def permited_faq
    if !@project.permit_faq
      redirect_to topic_digital_path(@project), alert: "No tiene acceso a este apartado"
    end
  end

  def permited_debate
    if !@project.permit_debate
      redirect_to topic_digital_path(@project), alert: "No tiene acceso a este apartado"
    end
  rescue    
    if !@digital_topic.project.permit_debate
      redirect_to topic_digital_path(@digital_topic.project), alert: "No tiene acceso a este apartado"
    end
  end

end
