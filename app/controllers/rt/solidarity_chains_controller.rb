class Rt::SolidarityChainsController < AuditGeneratesController
    include ActionView::Helpers::UrlHelper
    respond_to :html, :js, :json
    invisible_captcha only: [:create], honeypot: :subtitle
    before_action :set_solidarity_chains, only: [:new, :create]

    def new
        @rt_solidarity_chain = Rt::SolidarityChain.new
    rescue => e
        begin
          Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    def create
        @rt_solidarity_chain = Rt::SolidarityChain.new(rt_solidarity_chain_params)
        if @rt_solidarity_chain.save
            begin
                message=I18n.t('solidarity_chain.message.body')
                subject=I18n.t('solidarity_chain.message.subject')
                GlobalMailSMS.send({email: @rt_solidarity_chain.email,phone_number: @rt_solidarity_chain.phone_number},message,subject)
            rescue => e
                begin
                  Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
                rescue
                end
                
            end

            if current_user.blank?
                current = nil
            else 
                current = current_user.id
            end
            @attr[:operation_type] = "#{I18n.t('audit.register')}" 
            @attr[:operation] = "#{I18n.t('audit_enum.register', id: @rt_solidarity_chain.id)}"
            @attr[:new_data] = JSON.parse(@rt_solidarity_chain.attributes.to_json).except("created_at", "updated_at")
            AuditGenerate.generate(@attr)
            redirect_to new_rt_solidarity_chain_path, notice: t('solidarity_chain.response')
        else
            respond_with(@rt_solidarity_chain)
        end
    rescue => e
        begin
          Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    def tarjeta
        pdf_filename = File.join(Rails.root,"/app/assets/images/tarjeta.pdf")
        send_file(pdf_filename, :filename => "tarjeta.pdf", :type => "application/pdf")
    rescue => e
        begin
          Rails.logger.error("COD-00004: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    def show_image
        @solidary = Rt::SolidarityChain.find(params[:id])
        respond_to do |format|
            format.html
            format.js { render 'shared/popup_image'  }
        end
    rescue => e
        begin
          Rails.logger.error("COD-00005: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    private
        def audit_data
            @attr= {
            operation_type: '', 
            user_id: current_user.try(:id),
            resource_id: Resource.find_by(name: Rt::SolidarityChain.name).try(:id),
            operation: '',
            new_data: '',
            old_data: ''
            }
        end

    protected

        def rt_solidarity_chain_params
            params.require(:rt_solidarity_chain).permit(:full_name,:phone_number,:email,:action_description,:url_image,:alias)
        end

        def set_solidarity_chains
            @style_form_text_area="appearance: none;
            -moz-appearance: none;
            /* Firefox */
            -webkit-appearance: none;
            /* Safari and Chrome */
            height: 150px;
            border-radius: 6px;
            background-color: #f2f2f2;
            border: solid 1px rgba(29, 29, 29, 0.1);
            padding-left: 20px;
            font-size: 14px;
            font-weight: normal;
            font-style: normal;
            font-stretch: normal;
            line-height: 1.29;
            letter-spacing: 0.7px;
            color: #1d1d1d;
            margin-bottom: 30px;"
            @style_form="appearance: none;
            -moz-appearance: none;
            /* Firefox */
            -webkit-appearance: none;
            /* Safari and Chrome */
            height: 35px;
            border-radius: 6px;
            background-color: #f2f2f2;
            border: solid 1px rgba(29, 29, 29, 0.1);
            padding-left: 20px;
            font-size: 14px;
            font-weight: normal;
            font-style: normal;
            font-stretch: normal;
            line-height: 1.29;
            letter-spacing: 0.7px;
            color: #1d1d1d;
            margin-bottom: 30px;"
            @style_error="appearance: none;
            -moz-appearance: none;
            /* Firefox */
            -webkit-appearance: none;
            /* Safari and Chrome */
            height: 35px;
            border-radius: 6px;
            background-color: rgba(208, 1, 26, 0.05);
            border: solid 1px #d0011b;
            padding-left: 20px;
            font-size: 14px;
            font-weight: normal;
            font-style: normal;
            font-stretch: normal;
            line-height: 1.29;
            letter-spacing: 0.7px;
            color: #1d1d1d;
            margin-bottom: 30px;"
            @style_error_text_area="appearance: none;
            -moz-appearance: none;
            /* Firefox */
            -webkit-appearance: none;
            /* Safari and Chrome */
            height: 150px;
            border-radius: 6px;
            background-color: rgba(208, 1, 26, 0.05);
            border: solid 1px #d0011b;
            padding-left: 20px;
            font-size: 14px;
            font-weight: normal;
            font-style: normal;
            font-stretch: normal;
            line-height: 1.29;
            letter-spacing: 0.7px;
            color: #1d1d1d;
            margin-bottom: 30px;"
            @solidarity_chains=Rt::SolidarityChain.aproved.paginate(page: params[:page], per_page: params[:per_page]||10)
        end
end
