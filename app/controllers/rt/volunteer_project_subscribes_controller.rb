class Rt::VolunteerProjectSubscribesController < AuditGeneratesController
  include ActionView::Helpers::UrlHelper
  include Rt::VolunteerSubscribesHelper

  invisible_captcha only: [:create], honeypot: :subtitle
  before_action :set_rt_volunteer_project_subscribe, only: [:show, :edit, :update, :destroy]
  before_action :user_exists, only: :create
  before_action :set_fields, only: :new
  respond_to :html, :js, :json
  before_action :set_district, only: [:new,:create]
  before_action :set_info_source, only: [:new,:create]

  def new
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def index
    session.delete(:project)
    redirect_to projects_path
  rescue => e
    begin
      Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def create
    params[:project_id] ||= params[:rt_volunteer_project_subscribe][:project_id]
    project_volunteer_create(@attr, current_user)
  rescue => e
    begin
      Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  protected
    
    def user_exists
      return unless rt_volunteer_project_subscribe_params[:project_id] == ""
      return unless User.where(email: rt_volunteer_project_subscribe_params[:email]).exists?
      redirect_to new_user_session_path, alert: I18n.t('user.exist') unless rt_volunteer_project_subscribe_params[:email].blank?
    end

    def set_fields
      session[:project] ||= params[:project_id]
      if current_user && current_user.loggable_type == Volunteer.name
        @rt_volunteer_project_subscribe = Rt::VolunteerProjectSubscribe.new(project_id: session[:project],
                                                             name: current_user.loggable.name,
                                                             last_name: current_user.loggable.last_name,
                                                             phone_number: current_user.loggable.phone_number,
                                                             email: current_user.loggable.email)
        @rt_volunteer_project_subscribe.request_form.user_id = current_user.id
      else
        @rt_volunteer_project_subscribe = Rt::VolunteerProjectSubscribe.new
      end
    end

    def set_rt_volunteer_project_subscribe
      @rt_volunteer_project_subscribe = Rt::VolunteerProjectSubscribe.find(params[:id])
    end

    def rt_volunteer_project_subscribe_params
      params
        .require(:rt_volunteer_project_subscribe)
        .permit(
          :name, 
          :last_name, 
          :last_name_alt, 
          :phone_number, 
          :phone_number_alt, 
          :email, 
          :is_adult,
          :project_id, 
          :district_id,
          :district,
          :info_source_id,
          :postal_code,
          request_form: [:user_id]
        )
    end

  private

    def audit_data
      @attr= {
        operation_type: '', 
        user_id: current_user.try(:id),
        resource_id: Resource.find_by(name: Rt::VolunteerProjectSubscribe.name).try(:id),
        operation: '',
        new_data: '',
        old_data: ''
      }
    end

    def set_district
      districts=District.all
      @hash_district={}
      districts.each do |d|
        @hash_district.merge!({d.name=>d.id})
      end
    end

    def set_info_source
      info_sources=InfoSource.all.order("name ASC")
      @hash_info_source={}
      info_sources.each do |d|
        @hash_info_source.merge!({d.name=>d.id})
      end
    end
    
end
