class Rt::EntitySubscribesController < ApplicationController
  include ActionView::Helpers::UrlHelper
  before_action :set_rt_entity_subscribe, only: [:show, :edit, :update, :destroy]
  before_action :user_exists, only: [:create]
  respond_to :html, :js, :json
end
