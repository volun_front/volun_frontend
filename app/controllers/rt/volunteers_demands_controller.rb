class Rt::VolunteersDemandsController < AuditGeneratesController
  before_action :authenticate_user!
  respond_to :html, :js, :json
  before_action :set_project, only: [:new]
  before_action :set_district, only: [:new,:create]
  
  def new
    @rt_volunteers_demand = Rt::VolunteersDemand.new
    load_rt_project_publishing if @project
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def create
    @rt_volunteers_demand = Rt::VolunteersDemand.new(rt_volunteers_demand_params)
    @rt_volunteers_demand.request_form.user_id = current_user.id unless current_user.blank?
    if @rt_volunteers_demand.save
      begin
        message=I18n.t('volunteers_demand.message.body')
        subject=I18n.t('volunteers_demand.message.subject')
        GlobalMailSMS.send_rt(current_user.loggable,message,subject)
      rescue => e
        begin
          Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
        
      end
      if current_user.blank?
        current = nil
      else 
        current = current_user.id
      end
      @attr[:operation_type] = "#{I18n.t('audit.register')}" 
      @attr[:operation] = "#{I18n.t('audit_enum.register', id: @rt_volunteers_demand.id)}"
      @attr[:new_data] = JSON.parse(@rt_volunteers_demand.attributes.to_json).except("created_at", "updated_at")
      AuditGenerate.generate(@attr)

      redirect_to user_path(current_user), notice: t('volunteers_demand.response')
    else
      respond_with(@rt_volunteers_demand)
    end
  rescue => e
    begin
      Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  protected

    def load_rt_project_publishing
      @rt_volunteers_demand.notes = @project.name
      @rt_volunteers_demand.description = @project.description.html_safe
    end

    def set_project
      @project = Project.unscoped.find_by_id params[:project_id] if params[:project_id]
    end

    def rt_volunteers_demand_params
      params.require(:rt_volunteers_demand).permit(:notes, :description, :execution_start_date, :execution_end_date, :road_type, 
        :road_name, :number_type, :road_number, :postal_code, :town, :province, :province_id, :district, :district_id, :project_id, :requested_volunteers_num, :volunteers_profile, 
        :volunteer_functions_1, :volunteer_functions_2, :volunteer_functions_3, :volunteer_functions_4, :volunteer_functions_5)
    end

  private
    def set_district
      districts=District.all
      @hash_district={}
      districts.each do |d|
        @hash_district.merge!({d.name=>d.id})
      end
    end

    def audit_data
      @attr= {
        operation_type: '', 
        user_id: current_user.try(:id),
        resource_id: Resource.find_by(name: Rt::VolunteersDemand.name).try(:id),
        operation: '',
        new_data: '',
        old_data: ''
      }
    end
end
