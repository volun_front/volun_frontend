# class for publishing project
class Rt::ProjectPublishingsController < AuditGeneratesController
  before_action :authenticate_user!
  respond_to :html, :js, :json
  before_action :set_project, only: [:new]
  before_action :set_district, only: [:new,:create]


  def new
    @rt_project_publishing = Rt::ProjectPublishing.new
    load_rt_project_publishing if @project
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def create
    @rt_project_publishing = Rt::ProjectPublishing.new(rt_project_publishing_params)
    @rt_project_publishing.request_form.user_id = current_user.id unless current_user.blank?
    if @rt_project_publishing.save
      if current_user.blank?
        current = nil
      else 
        current = current_user.id
      end
      @attr[:operation_type] = "#{I18n.t('audit.register')}" 
      @attr[:operation] = "#{I18n.t('audit_enum.register', id: @rt_project_publishing.id)}"
      @attr[:new_data] = JSON.parse(@rt_project_publishing.attributes.to_json).except("created_at", "updated_at")
      AuditGenerate.generate(@attr)
      redirect_to user_path(current_user), notice: t('project_publishing.response')
    else
      respond_with(@rt_project_publishing)
    end
  rescue => e
    begin
      Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  protected

    def load_rt_project_publishing
      @rt_project_publishing.notes = @project.name
      @rt_project_publishing.description = @project.description.html_safe
    end

    def set_project
      @project = Project.unscoped.find_by_id params[:project_id] if params[:project_id]
    end

    def rt_project_publishing_params
      params.require(:rt_project_publishing).permit(:notes, :description, :road_type, 
        :road_name, :number_type, :road_number, 
        :postal_code, :town, :province,:province_id, :entity_id, :project_id,:district,:district_id)
    end

  private
    def set_district
      districts=District.all
      @hash_district={}
      districts.each do |d|
        @hash_district.merge!({d.name=>d.id})
      end
    end

    def audit_data
      @attr= {
        operation_type: '', 
        user_id: current_user.try(:id),
        resource_id: Resource.find_by(name: Rt::ProjectPublishing.name).try(:id),
        operation: '',
        new_data: '',
        old_data: ''
      }
    end
end
