class Rt::ProjectUnpublishingsController < AuditGeneratesController
  before_action :authenticate_user!
  respond_to :html, :js, :json

  def new
    @rt_project_unpublishing = Rt::ProjectUnpublishing.new
    @rt_project_unpublishing.project_id = params[:project_id]
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def create
    @rt_project_unpublishing = Rt::ProjectUnpublishing.new(rt_project_unpublishing_params)
    @rt_project_unpublishing.request_form.user_id = current_user.id unless current_user.blank?
    if @rt_project_unpublishing.save
      if current_user.blank?
        current = nil
      else 
        current = current_user.id
      end
      @attr[:operation_type] = "#{I18n.t('audit.register')}" 
      @attr[:operation] = "#{I18n.t('audit_enum.register', id: @rt_project_unpublishing.id)}"
      @attr[:new_data] = JSON.parse(@rt_project_unpublishing.attributes.to_json).except("created_at", "updated_at")
      AuditGenerate.generate(@attr)

      redirect_to user_path(current_user), notice: t('project_unpublishing.response')
    else
      respond_with(@rt_project_unpublishing)
    end
  rescue => e
    begin
      Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  private
    def audit_data
      @attr= {
        operation_type: '', 
        user_id: current_user.try(:id),
        resource_id: Resource.find_by(name: ProjectUnpublishing.name).try(:id),
        operation: '',
        new_data: '',
        old_data: ''
      }
    end

  protected
    def rt_project_unpublishing_params
      params.require(:rt_project_unpublishing).permit(:notes, :project_id)
    end
end
