# controller for request amendments of volunteer
class Rt::VolunteerAmendmentsController < AuditGeneratesController
  before_action :authenticate_user!
  respond_to :html, :js, :json

  def new
    @rt_volunteer_amendment = Rt::VolunteerAmendment.new
    set_amendment_volunteer
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def create
    @rt_volunteer_amendment = Rt::VolunteerAmendment.new(rt_volunteer_amendment_params)
    @rt_volunteer_amendment.request_form.user_id = current_user.id unless current_user.blank?
    begin
      @rt_volunteer_amendment.volunteer_id = current_user.loggable.id
    rescue
    end
    if @rt_volunteer_amendment.save
      begin
        message=I18n.t('volunteer_amendment.message.body')
        subject=I18n.t('volunteer_amendment.message.subject')
        GlobalMailSMS.send_rt(current_user.loggable,message,subject)
      rescue => e
        begin
          Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
        
      end
      if current_user.blank?
        current = nil
      else 
        current = current_user.id
      end
      @attr[:operation_type] = "#{I18n.t('audit.register')}" 
      @attr[:operation] = "#{I18n.t('audit_enum.register', id: @rt_volunteer_amendment.id)}"
      @attr[:new_data] = JSON.parse(@rt_volunteer_amendment.attributes.to_json).except("created_at", "updated_at")
      AuditGenerate.generate(@attr)

      redirect_to user_path(current_user), notice: t('volunteer_amendment.response')
    else
      respond_with(@rt_volunteer_amendment)
    end
  rescue => e
    begin
      Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end
  
  private
    def audit_data
      @attr= {
        operation_type: '', 
        user_id: current_user.try(:id),
        resource_id: Resource.find_by(name: Rt::VolunteerAmendment.name).try(:id),
        operation: '',
        new_data: '',
        old_data: ''
      }
    end
  protected

    def rt_volunteer_amendment_params
      params.require(:rt_volunteer_amendment).permit(:volunteer_id, :address_id, :phone_number, :phone_number_alt, :road_type, :road_name, :number_type, :road_number, :postal_code, :town, :province_id, :email)
    end

    def set_amendment_volunteer
      @rt_volunteer_amendment.phone_number = Volunteer.find_by_id(current_user.loggable_id).phone_number
      @rt_volunteer_amendment.phone_number_alt = Volunteer.find_by_id(current_user.loggable_id).phone_number_alt
      @rt_volunteer_amendment.email = current_user.email
      @rt_volunteer_amendment.road_type = Volunteer.includes(:address).find_by_id(current_user.loggable_id).address.road_type
      @rt_volunteer_amendment.road_name = Volunteer.includes(:address).find_by_id(current_user.loggable_id).address.road_name
      @rt_volunteer_amendment.number_type = Volunteer.includes(:address).find_by_id(current_user.loggable_id).address.road_number_type
      @rt_volunteer_amendment.road_number = Volunteer.includes(:address).find_by_id(current_user.loggable_id).address.road_number
      @rt_volunteer_amendment.postal_code = Volunteer.includes(:address).find_by_id(current_user.loggable_id).address.postal_code
      @rt_volunteer_amendment.town = Volunteer.includes(:address).find_by_id(current_user.loggable_id).address.town
      @rt_volunteer_amendment.province = Volunteer.includes(:address).find_by_id(current_user.loggable_id).address.province
    end
end
