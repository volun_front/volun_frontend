class Rt::VolunteerUnsubscribesController < AuditGeneratesController
  before_action :authenticate_user!
  respond_to :html, :js, :json

  def new
    @rt_volunteer_unsubscribe = Rt::VolunteerUnsubscribe.new
    @rt_volunteer_unsubscribe.project_id = params[:project_id]
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def create
    @rt_volunteer_unsubscribe = Rt::VolunteerUnsubscribe.new(rt_volunteer_unsubscribe_params)
    @rt_volunteer_unsubscribe.request_form.user_id = current_user.id unless current_user.blank?
    if @rt_volunteer_unsubscribe.save
      begin
        message=I18n.t('volunteer_unsubscribe.message.body')
        subject=I18n.t('volunteer_unsubscribe.message.subject')
        GlobalMailSMS.send_rt(current_user.loggable,message,subject)
      rescue => e
        begin
          Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
        
      end
      if current_user.blank?
        current = nil
      else 
        current = current_user.id
      end
      @attr[:operation_type] = "#{I18n.t('audit.register')}" 
      @attr[:operation] = "#{I18n.t('audit_enum.register', id: @rt_volunteer_unsubscribe.id)}"
      @attr[:new_data] = JSON.parse(@rt_volunteer_unsubscribe.attributes.to_json).except("created_at", "updated_at")
      AuditGenerate.generate(@attr)

      redirect_to user_path(current_user), notice: t('volunteer_unsubscribe.response')
    else
      respond_with(@rt_volunteer_unsubscribe)
    end
  rescue => e
    begin
      Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end
  private
    def audit_data
      @attr= {
        operation_type: '', 
        user_id: current_user.try(:id),
        resource_id: Resource.find_by(name: Rt::VolunteerUnsubscribe.name).try(:id),
        operation: '',
        new_data: '',
        old_data: ''
      }
    end
  protected

    def rt_volunteer_unsubscribe_params
      params.require(:rt_volunteer_unsubscribe).permit(:project_id, :unsubscribe_level_id, :notes)
    end

    
end
