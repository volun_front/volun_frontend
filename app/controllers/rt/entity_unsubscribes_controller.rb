class Rt::EntityUnsubscribesController < AuditGeneratesController
  before_action :authenticate_user!
  respond_to :html, :js, :json

  def new
    @rt_entity_unsubscribe = Rt::EntityUnsubscribe.new
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def create
    @rt_entity_unsubscribe = Rt::EntityUnsubscribe.new(rt_entity_unsubscribe_params)
    @rt_entity_unsubscribe.request_form.user_id = current_user.id unless current_user.blank?
    if @rt_entity_unsubscribe.save
      if current_user.blank?
        current = nil
      else 
        current = current_user.id
      end
      @attr[:operation_type] = "#{I18n.t('audit.register')}" 
      @attr[:operation] = "#{I18n.t('audit_enum.register', id: @rt_entity_unsubscribe.id)}"
      @attr[:new_data] = JSON.parse(@rt_entity_unsubscribe.attributes.to_json).except("created_at", "updated_at")
      AuditGenerate.generate(@attr)

      redirect_to user_path(current_user), notice: t('entity_unsubscribe.response')
    else
      respond_with(@rt_entity_unsubscribe)
    end
  rescue => e
    begin
      Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  private

    def audit_data
      @attr= {
        operation_type: '', 
        user_id: current_user.try(:id),
        resource_id: Resource.find_by(name: Rt::EntityUnsubscribe.name).try(:id),
        operation: '',
        new_data: '',
        old_data: ''
      }
    end

  protected
  
    def rt_entity_unsubscribe_params
      params.require(:rt_entity_unsubscribe).permit(:user_id, :notes)
    end
end
