class Rt::OthersController < AuditGeneratesController
  before_action :authenticate_user!
  respond_to :html, :js, :json

  def new
    @rt_other = Rt::Other.new
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def create
    @rt_other = Rt::Other.new(rt_other_params)
    @rt_other.request_form.user_id = current_user.id unless current_user.blank?
    if @rt_other.save
      if current_user.blank?
        current = nil
      else 
        current = current_user.id
      end
      @attr[:operation_type] = "#{I18n.t('audit.register')}" 
      @attr[:operation] = "#{I18n.t('audit_enum.register', id: @rt_other.id)}"
      @attr[:new_data] = JSON.parse(@rt_other.attributes.to_json).except("created_at", "updated_at")
      AuditGenerate.generate(@attr)
      redirect_to user_path(current_user), notice: t('other.response')
    else
      respond_with(@rt_other)
    end
  rescue => e
    begin
      Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  private
    def audit_data
      @attr= {
        operation_type: '', 
        user_id: current_user.try(:id),
        resource_id: Resource.find_by(name: Rt::Other.name).try(:id),
        operation: '',
        new_data: '',
        old_data: ''
      }
    end
  protected
    def rt_other_params
      params.require(:rt_other).permit(:description, :user_id)
    end
end
