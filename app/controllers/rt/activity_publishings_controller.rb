class Rt::ActivityPublishingsController < AuditGeneratesController
  before_action :authenticate_user!
  respond_to :html, :js, :json

  def new
    @rt_activity_publishing = Rt::ActivityPublishing.new
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def create
    @rt_activity_publishing = Rt::ActivityPublishing.new(rt_activity_publishing_params)
    @rt_activity_publishing.request_form.user_id = current_user.id unless current_user.blank?
    if @rt_activity_publishing.save
      if current_user.blank?
        current = nil
      else 
        current = current_user.id
      end
      @attr[:operation_type] = "#{I18n.t('audit.register')}" 
      @attr[:operation] = "#{I18n.t('audit_enum.register', id: @rt_activity_publishing.id)}"
      @attr[:new_data] = JSON.parse(@rt_activity_publishing.attributes.to_json).except("created_at", "updated_at")
      AuditGenerate.generate(@attr) 
     
      redirect_to user_path(current_user), notice: t('activity_publishing.response')
    else
      respond_with(@rt_activity_publishing)
    end
  rescue => e
    begin
      Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  private
    def audit_data
      @attr= {
        operation_type: '', 
        user_id: current_user.try(:id),
        resource_id: Resource.find_by(name: Rt::ActivityPublishing.name).try(:id),
        operation: '',
        new_data: '',
        old_data: ''
      }
    end
  protected

    def rt_activity_publishing_params
      params.require(:rt_activity_publishing).permit(:name, :organizer, :description, :execution_date, :execution_hour, :road_type, :road_name, :number_type, :road_number, :postal_code, :town, :province, :entity_id, :project_id, :dates_text_free, :hours_text_free, :places_text_free, :observations_text_free)
    end
end
