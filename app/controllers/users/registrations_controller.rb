class Users::RegistrationsController < Devise::RegistrationsController
  before_action :authenticate_user!, only: [:create]
  before_action :configure_sign_up_params, only: [:create]
  before_action :audit_data
  respond_to :html, :js, :json

  def update
    @attr[:old_data]= JSON.parse(@user.attributes.to_json).except("created_at", "updated_at")
    if @user.update(user_params)
      if current_user.blank?
        current = nil
      else 
        current = current_user.id
      end

      # if @user.loggable_type.to_s == "Volunteer"
      #   begin
      #     Ci360.new(@user.loggable).insert_ci360_code            
      #   rescue
      #   end
      #     begin
      #     Ci360Autentica.new(@user.loggable).generate_user(params[:user][:password])
      #   rescue
      #   end
      #   begin
      #     Ci360Autentica.new(user.loggable).auto_change_password(params[:user][:password])
      #   rescue
      #   end
      # end
      
      @attr[:operation_type] = "#{I18n.t('audit.update')}" 
      @attr[:operation] = "#{I18n.t('audit_enum.update', id: @user.id)}"
      @attr[:new_data] = JSON.parse(@user.attributes.to_json).except("created_at", "updated_at")

      AuditGenerate.generate(@attr)

      redirect_to projects_path, notice: t('application.modificacionOk')
    else
      render action: 'edit'
    end
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

private

  def configure_account_update_params
    devise_parameter_sanitizer.for(:account_update) << :attribute
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:user) { |u| u.permit(:password, :password_confirmation) }
  end

  def user_params
    params.require(:user).permit(:password, :password_confirmation)
  end

  def audit_data
    @attr= {
      operation_type: '', 
      user_id: current_user.try(:id),
      resource_id: Resource.find_by(name: User.name).try(:id),
      operation: '',
      new_data: '',
      old_data: ''
    }
  end

end
