class Users::PasswordsController < Devise::PasswordsController
  def update
    generated_password_reset_token = Devise.token_generator.digest(self, :reset_password_token, params[:user][:reset_password_token])
    user = User.find_by(reset_password_token: generated_password_reset_token)
    # if user.loggable_type.to_s == "Volunteer"
    #   begin
    #     Ci360.new(user.loggable).insert_ci360_code            
    #   rescue
    #   end
    #   begin
    #     Ci360Autentica.new(user.loggable).generate_user(params[:user][:password])
    #   rescue
    #   end
    #   begin
    #     Ci360Autentica.new(user.loggable).auto_change_password(params[:user][:password])
    #   rescue
    #   end
    # end

    super  
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end
end
