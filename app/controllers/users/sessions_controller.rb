class Users::SessionsController < Devise::SessionsController
  

  def create
    if !current_user.blank?
      redirect_to root_path
    elsif !params[:user].blank?
      user = User.find_by(email: params[:user][:username].downcase)
      
      if user.blank?
        volunteer = Volunteer.find_by(email: params[:user][:username].downcase)
        user = volunteer.try(:user)
      end

      if !user.blank? && user.try(:loggable_type).to_s == "Volunteer"
        if !user.blank? && (user.valid_password?(params[:user][:password]))
          sign_in(user)
          redirect_to root_path
        else
          super do |resource|
            Ahoy::Event.create(name: "Sign in", properties: {title: "Inicio de sesión"}.to_json, user_id: current_user.try(:id), visit_id: current_visit.try(:id), time:Time.now)
          end
        end
      else
        super do |resource|
          Ahoy::Event.create(name: "Sign in", properties: {title: "Inicio de sesión"}.to_json, user_id: current_user.try(:id), visit_id: current_visit.try(:id), time:Time.now)
        end
      end
    else
      super do |resource|
        Ahoy::Event.create(name: "Sign in", properties: {title: "Inicio de sesión"}.to_json, user_id: current_user.try(:id), visit_id: current_visit.try(:id), time:Time.now)
      end
    end
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end
end
