class WelcomeController < ApplicationController

  respond_to :html, :js, :json
  skip_before_action :verify_authenticity_token

  before_action :load_data_calendar, only: [:index]

  def index
    @participation_projects = ProjectsVolunteer.joins(:project).all.select("projects.subtype_pt as subtype_pt, count(1) as total").group("projects.subtype_pt")
    @areas_percent = AreasProject.joins("INNER JOIN areas ON areas.id = areas_projects.area_id").group("areas.name").count
    Ahoy::Event.create(name: "Visit", properties: {title: "Página principal"}.to_json, user_id: current_user.try(:id), visit_id: current_visit.try(:id), time:Time.now)
    @num_activities = Activity.all.where("EXTRACT(YEAR FROM start_date) = #{Date.today.year} AND EXTRACT(MONTH FROM start_date) = #{Date.today.month}").count
    @projects_featured = Project.includes(:areas).featured
    @project_urgent = Project.urgent.limit(1)
    @locations = Event.includes(:address, :project).where(eventable_type: Project.name).as_json(only: [:id], include: { address: { only: [:latitude, :longitude] }, project: { only: [:id, :pt_extendable_type, :name] }  })
    params[:day] ||= Time.now
    @search_q = Timetable.select('execution_date, activities.id').joins(:activity).order(:execution_date).group('execution_date, activities.id').search(execution_date_gteq: Time.now)
    @events = @search_q.result.limit(2)
    @list_days = Activity.includes(:timetables).distinct.activities_present(Time.now).order('timetables.execution_date').pluck('timetables.execution_date').to_json

    @activities_search = @activities_unpag
      
    @activities = @activities_search.paginate(page: params[:page], per_page: Kaminari.config.default_per_page)

    @components = @component_home.child_components.order(:order)

    respond_to do |format|
      format.html
      format.js
    end
    
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  

  private

  def load_data_calendar
    @current_date_calendar = Date.today       

    start_date =  @current_date_calendar.beginning_of_month.to_date
    end_date=  @current_date_calendar.end_of_month.to_date
    @activities_unpag = Activity.all.order(start_date: :desc).where("start_date::date between ?::date and ?::date OR end_date::date between ?::date and ?::date OR ?::date between start_date::date and end_date::date OR ?::date between start_date::date and end_date::date ",start_date,end_date,start_date,end_date, start_date,end_date ) 
  end

end
