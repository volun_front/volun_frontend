class PagesController < ApplicationController

  def show
    render action: params[:id]
  rescue ActionView::MissingTemplate
    head 404
  end

  def whoami
    @component=VirtualCommunity::Component.find_by(denomination: '¿Quiénes somos?')   
  end

  def volunteers
    @component=VirtualCommunity::Component.find_by(denomination: '¿Qué puedes hacer tú?')   
  end

  def entities
    @component=VirtualCommunity::Component.find_by(denomination: 'Entidades colaboradoras')   
  end

  def conditions
    @component=VirtualCommunity::Component.find_by(denomination: 'Condiciones de uso')   
  end

  def accesibility
    @component=VirtualCommunity::Component.find_by(denomination: 'Accesibilidad')   
  end

  def mta_privacy
    @component=VirtualCommunity::Component.find_by(denomination: 'Política privacidad MTA')
  rescue => e
    begin
      Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end
end
