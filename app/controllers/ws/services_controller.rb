class Ws::ServicesController < Ws::GenericApiController
    
    before_action :skip_trackable
    before_action :checktokenUser
    before_action :authenticate!, unless: ->{user_authenticated?}
    require 'base64'
    
    # GET /volun/ws/type_request
    # ENTRADA => {version}
    # SALIDA => {count,value => {id,name,description,normalFlow}}
    def type_request        
        type_list = []    
        permit_params = params.permit(:version)  

        if permit_params[:version].blank? || !permit_params[:version].blank? && permit_params[:version].to_s!="true"
            Service.where(active: true).where("code::int <= 8 and code::int >=1").order(:mta_order).each {|s| type_list.push({id: s.code.to_i, nombre: s.name}) }  
        elsif !permit_params[:version].blank? && permit_params[:version].to_s=="true"
            Service.where(active: true).order(:mta_order).each {|s| type_list.push({id: s.code.to_i, name: s.name, description: s.description, normalFlow: s.normal_flow}) }  
        end      
        AuditGenerateHelper.input_parameters("type_request", current_user, current_user)
        success({count: type_list.count.to_i, value: type_list})
    rescue => e
        begin
            Rails.logger.error("ERROR-WS: type_request -> #{e}")
        rescue
        end
        error("service")  
    end

    # GET /volun/ws/request/questions
    # ENTRADA => {}
    # SALIDA => {count,value => {descripcion,id,tipo_campo}}
    def request_questions
        question_list = []
        ServiceQuestion.where(active: true).each do |s|
            question_list.push({ "id": s.id.to_i, "descripcion": s.description, "tipo_campo": s.question_type })
        end
        AuditGenerateHelper.input_parameters("request_questions", current_user, current_user)
        success({count: question_list.count.to_i, value: question_list})
    rescue => e
        begin
            Rails.logger.error("ERROR-WS: request_questions -> #{e}")
        rescue
        end
        error("service")
    end

    # POST /volun/ws/request/search
    # ENTRADA => {version,type,code_cid360,status,cod_servicios,cod_distritos,distritos,cod_barrios,barrios,items,skip,orden,fecha_desde,fecha_hasta, hora_desde, hora_hasta}
    # SALIDA => {count,value => {descripcion,fecha,hora,lugar,datos_adicionales,id,id_tipo,tipo,distrito,barrio,status,id_voluntario,nombre_voluntario,imagen_voluntario,id_ciudadano}}
    def request_search
        if strong_request_search_params
            permit_params = params.permit(:version,:type,:code_cid360,{:status => []},{:cod_servicios => []},{:cod_distritos => []},{:distritos => []},{:cod_barrios => []},{:barrios => []},:items,:skip,:orden,:fecha_desde,:fecha_hasta,:hora_desde,:hora_hasta) 
        
            begin
                Rails.logger.error("INFO-WS: POST /volun/ws/request/search -> #{permit_params}")
            rescue
            end
            
            aux_status = []
            aux_key_status = []
            aux_services = []
            aux_distritos = []
            aux_barrios = []
            aux_cod_distritos = []
            aux_cod_barrios = []
            error_status=false
            permit_params[:cod_servicios].each {|x| aux_services.push(x.to_i) if !x.blank?} if !permit_params[:cod_servicios].blank?
            if !permit_params[:status].blank?
                permit_params[:status].each do |x| 
                    if !x.blank?
                        if !Service.status_type_list.include?(x.to_sym) 
                            if !Service.status_key_list.include?(x.to_sym)
                                error_status=true
                                break
                            else
                                aux_key_status.push(x)
                            end
                        else
                            aux_status.push(x) 
                        end
                    end                
                end 
            end
            permit_params[:cod_distritos].each {|x| aux_cod_distritos.push(Address.district_list.find_by("cast(code as int)=cast(? as int)", x.to_i).try(:id)) if !x.blank?} if !permit_params[:cod_distritos].blank?
            permit_params[:cod_barrios].each {|x| Borought.where("cast(code as int)=cast(? as int)", x.to_i).select(:id).each {|x| aux_cod_barrios.push(x.id)} if !x.blank?} if !permit_params[:cod_barrios].blank?
            permit_params[:distritos].each {|x| aux_distritos.push(Address.district_list.find_by(name: x.to_s).try(:id)) if !x.blank?} if !permit_params[:distritos].blank?
            permit_params[:barrios].each {|x| aux_barrios.push(Borought.find_by(name: x.to_s).try(:id)) if !x.blank?} if !permit_params[:barrios].blank?

            if !permit_params[:type].blank? && !(["citizen","volunteer"].include? permit_params[:type].to_s.downcase)
                error("error34", :type)
            elsif !permit_params[:code_cid360].blank? && permit_params[:type].blank?
                error("error13", :type)
            elsif permit_params[:code_cid360].blank? && !permit_params[:type].blank?
                error("error34", :code_cid360)
            elsif error_status
                error("error37")
            elsif !permit_params[:orden].blank? && !(["asc","desc"].include?(permit_params[:orden].to_s.downcase))
                error("error62")
            elsif !permit_params[:fecha_desde].blank? && permit_params[:fecha_desde].to_s.match(/\d{2}\/\d{2}\/\d{4}|\d{4}-\d{2}-\d{2}/).to_s.strip.blank?
                error("error71")
            elsif !permit_params[:fecha_hasta].blank? && permit_params[:fecha_hasta].to_s.match(/\d{2}\/\d{2}\/\d{4}|\d{4}-\d{2}-\d{2}/).to_s.strip.blank?
                error("error71")
            elsif !permit_params[:hora_desde].blank? && permit_params[:hora_desde].to_s.match(/[\d{1}|\d{2}]\:\d{2}/).to_s.strip.blank?
                error("error72")
            elsif !permit_params[:hora_hasta].blank? && permit_params[:hora_hasta].to_s.match(/[\d{1}|\d{2}]\:\d{2}/).to_s.strip.blank?
                error("error72")
            elsif aux_status.count > 0 && aux_key_status.count > 0
                error("error63")
            elsif aux_key_status.count > 1
                error("error64")
            else               
                CitizenService.where(status: ["active","prioritary"]).each do |sr|
                    permit_create = Setting.find_by(key: "VolunBackend.ws_permit_create_service")

                    fecha = Time.parse("#{sr.date_request} #{sr.hour_request}")
                    horas = permit_create.blank? ? 48 :  permit_create.value.to_i

                    if fecha < Time.zone.now
                        sr.status = "expired"
                    elsif fecha - (horas.to_i/2).hours >= Time.now 
                        sr.status = "active"
                    else 
                        sr.status = "prioritary"
                    end
                    sr.status_service = StatusService.find_by(code:  sr.status)
                    sr.save(validate: false)
                end
                if permit_params[:version].blank? || !permit_params[:version].blank? && permit_params[:version].to_s!="true"
                    services = CitizenService.where(canceled_at: nil).where("citizen_services.citizen_id IS NOT NULL and citizen_services.service_id in (?)",Service.where(active: true).where("code::int <= 8 and code::int >=1").select(:id)).where("citizen_services.district_id is not null AND citizen_services.borought_id is not null")
                        .order(date_request: (permit_params[:orden].blank? ? :asc : permit_params[:orden]))
                elsif !permit_params[:version].blank? && permit_params[:version].to_s=="true"
                    services = CitizenService.where(canceled_at: nil).where("citizen_services.citizen_id IS NOT NULL").where("citizen_services.district_id is not null AND citizen_services.borought_id is not null")
                        .order(date_request: (permit_params[:orden].blank? ? :asc : permit_params[:orden]))
                end
             

                if !aux_key_status.blank?
                    if aux_key_status[0].to_s=='list_active'
                        services = services.where("status IN (?) OR status = 'accept' AND (date_request = ? AND hour_request >= ? OR date_request > ?)", ['active','prioritary'], Time.zone.now, (Time.zone.now - 2.hours).strftime("%H:%M"), Time.zone.now)
                    elsif aux_key_status[0].to_s=='list_inactive'
                        services = services.where("status IN (?) OR status = 'accept' AND (date_request = ? AND hour_request < ? OR date_request < ?)", ['canceled','finished','undone','expired'], Time.zone.now, (Time.zone.now - 2.hours).strftime("%H:%M"), Time.zone.now)
                    end
                end

                if current_user.loggable.class.name.to_s == "Citizen"
                    if current_user.loggable.tester
                        services = services.where(citizen_id: current_user.loggable.id)
                    else
                        services = services.where(citizen_id: current_user.loggable.id).not_tester
                    end
                elsif current_user.loggable.class.name.to_s == "Volunteer"
                    if current_user.loggable.tester
                        services = services.where("citizen_services.volunteer_id is null OR volunteer_id = ?", current_user.loggable.id)
                    else
                        services = services.where("citizen_services.volunteer_id is null OR volunteer_id = ?", current_user.loggable.id).not_tester
                    end
                end
                services = services.where("citizen_services.citizen_id = ?", permit_params[:code_cid360]) if !permit_params[:code_cid360].blank? && permit_params[:type].to_s.downcase == "citizen"
                services = services.where("citizen_services.volunteer_id = ? OR citizen_services.volunteer_id = ?",permit_params[:code_cid360],  current_user.loggable.id) if !permit_params[:code_cid360].blank? && permit_params[:type].to_s.downcase == "volunteer"
                services = services.where("citizen_services.status IN (?)", aux_status) if !aux_status.blank?
                services = services.where("citizen_services.service_id IN (?)", aux_services) if !aux_services.blank?
                services = services.where("citizen_services.district_id IN (?)", aux_distritos) if !aux_distritos.blank?
                services = services.where("citizen_services.borought_id IN (?)", aux_barrios) if !aux_barrios.blank?
                services = services.where("citizen_services.district_id IN (?)", aux_cod_distritos) if !aux_cod_distritos.blank?
                services = services.where("citizen_services.borought_id IN (?)", aux_cod_barrios) if !aux_cod_barrios.blank?

                if !permit_params[:fecha_desde].blank? && !permit_params[:fecha_hasta].blank?
                    services = services.where("cast(citizen_services.date_request as date)  Between cast(? as date) and cast(? as date) ", Time.parse(permit_params[:fecha_desde]),Time.parse(permit_params[:fecha_hasta])) 
                elsif !permit_params[:fecha_desde].blank? 
                    services = services.where("cast(citizen_services.date_request as date) >= cast(? as date) ", Time.parse(permit_params[:fecha_desde]))
                elsif !permit_params[:fecha_hasta].blank?
                    services = services.where("cast(citizen_services.date_request as date)  <= cast(? as date)", Time.parse(permit_params[:fecha_hasta]))
                end

                if !permit_params[:hora_desde].blank? && !permit_params[:hora_hasta].blank?
                    services = services.where("to_number(citizen_services.hour_request,'99999')  Between ? and ? ",permit_params[:hora_desde].to_s.gsub(":","").to_i,permit_params[:hora_hasta].to_s.gsub(":","").to_i)           
                elsif !permit_params[:hora_desde].blank? 
                    services = services.where("to_number(citizen_services.hour_request,'99999')  >= ? ", permit_params[:hora_desde].to_s.gsub(":","").to_i)             
                elsif !permit_params[:hora_hasta].blank?
                    services = services.where("to_number(citizen_services.hour_request,'99999')   <= ? ", permit_params[:hora_hasta].to_s.gsub(":","").to_i)
                end

                services = services.distinct.sort_by {|x| [(permit_params[:orden].blank? || permit_params[:orden].to_s.upcase == "ASC"  ? x.date_request.to_time.to_i : -(x.date_request.to_time.to_i)),x.hour_request_numeric]}
                contador = services.count

                services = services.drop(permit_params[:skip].to_i) if !permit_params[:skip].blank?
                services = services.first(permit_params[:items].to_i) if !permit_params[:items].blank?

                service_list = []

                services.each do |s|
                    begin
                        if !s.try(:volunteer).try(:logo).try(:file).blank?
                            data = File.open(s.volunteer.logo.file.path(:thumb)).read
                            encoded = Base64.encode64(data)                        
                        else
                            encoded = nil
                        end
                    rescue
                        encoded = nil
                    end

                    fecha = Time.zone.parse("#{s.date_request} #{s.hour_request}")
                    if !permit_params[:version].blank? && permit_params[:version].to_s=="true"
                        service_list.push({
                            descripcion: s.description,
                            fecha: fecha.strftime("%d/%m/%Y"),
                            hora: fecha.strftime("%H:%M"),
                            lugar: s.place,
                            datos_adicionales: s.additional_data,
                            id: s.id.to_i,
                            id_tipo: s.service_id.to_i,
                            tipo: s.service.try(:name),
                            distrito: s.district.try(:name),
                            cod_distrito: s.district.try(:code), 
                            barrio: s.borought.try(:name),
                            cod_barrio: s.borought.try(:code),
                            status: s.status,
                            id_voluntario: s.volunteer_id.to_i,
                            cid360_voluntario: s.volunteer.try(:id).to_i,
                            nombre_voluntario: s.volunteer.try(:full_name),
                            imagen_voluntario: encoded,
                            id_ciudadano: s.citizen_id.to_i,
                            cid360_ciudadano: s.citizen.try(:id).to_i
                        })
                    else
                        service_list.push({
                            descripcion: s.description,
                            fecha: fecha.strftime("%d/%m/%Y"),
                            hora: fecha.strftime("%H:%M"),
                            lugar: s.place,
                            datos_adicionales: s.additional_data,
                            id: s.id.to_i,
                            id_tipo: s.service_id.to_i,
                            distrito: s.district.try(:name),
                            cod_distrito: s.district.try(:code), 
                            barrio: s.borought.try(:name),
                            cod_barrio: s.borought.try(:code),
                            status: s.status,
                            id_voluntario: s.volunteer_id.to_i,
                            cid360_voluntario: s.volunteer.try(:id).to_i,
                            nombre_voluntario: s.volunteer.try(:full_name),
                            imagen_voluntario: encoded,
                            id_ciudadano: s.citizen_id.to_i,
                            cid360_ciudadano: s.citizen.try(:id).to_i
                        })
                    end
                end
                AuditGenerateHelper.input_parameters("request_search", current_user, current_user)
                success({count: contador.to_i, value: service_list })
            end
        end
    rescue => e
        begin
            Rails.logger.error("ERROR-WS: request_search -> #{e}")
        rescue
        end
        error("service")  
    end    

    # POST /volun/ws/request
    # ENTRADA => {codigo_de_servicio,descripcion,fecha,hora,lugar,datos_adicionales,cod_distrito,cod_barrio}
    # SALIDA => {id_servicio}
    def register_request
        if strong_request_params
            permit_params = params.permit(:codigo_de_servicio,:descripcion,:fecha,:hora,:lugar,:datos_adicionales,:cod_distrito,:cod_barrio) 

            invalid_param = valid_params(permit_params.except(:datos_adicionales,:descripcion))

            if invalid_param != true
                error("error13", invalid_param)
            elsif Service.where(code: permit_params[:codigo_de_servicio]).blank?
                error("error34", "codigo_de_servicio")        
            elsif current_user.loggable.class.name.to_s != "Citizen"
                error("token")  
            else    
                exist_district = Address.district_list.find_by("name = ? OR cast(code as int) = cast(? as int) ", permit_params[:cod_distrito].to_s, permit_params[:cod_distrito].to_s.rjust(2, '0'))

                if exist_district.blank?
                    error("error36")
                else
                    permit_params[:cod_distrito] = exist_district.code
                    begin
                        borought_numeric = !!Integer(permit_params[:cod_barrio])
                    rescue
                        borought_numeric = false
                    end

                    exist_borought = Address.borougth_list(exist_district.code).find_by("boroughts.name = ? OR cast(boroughts.code as int)= cast(? as int)",permit_params[:cod_barrio].to_s,permit_params[:cod_barrio].to_s.rjust(2, '0'))
                    if exist_borought.blank?
                        error("error35") 
                    else
                        permit_params[:cod_barrio] = exist_borought.code
                        begin
                            fecha = Time.zone.parse("#{permit_params[:fecha]} #{permit_params[:hora]}")

                            #################################################################################
                            # Desbloquear cuando sea necesario validarlo
                            #################################################################################
                            permit_create = Setting.find_by(key: "VolunBackend.ws_permit_create_service")
                            permit_create_tempo = Setting.find_by(key: "VolunBackend.ws_limit_create_service")
                            if !permit_create.blank? &&  fecha < Time.zone.now + permit_create.value.to_i.hours
                                error("error39", permit_create.value)     
                            elsif !permit_create_tempo.blank? && fecha >= Time.zone.now + permit_create_tempo.value.to_i.days
                                error("error50", permit_create_tempo.value)                       
                            elsif fecha <= Time.zone.now
                                error("error03")
                            elsif fecha > (Time.zone.now + 6.month)
                                error("error65", "#{(Time.zone.now + 6.month).strftime("%d/%m/%Y %H:%M")}")
                            else
                                if can_register_service(fecha)                
                                    if current_user.loggable.class.name.to_s == "Citizen"    
                                        sr = CitizenService.new(
                                            citizen_id: current_user.loggable_id, 
                                            service_id:  Service.find_by(code: permit_params[:codigo_de_servicio]).id,
                                            description: permit_params[:descripcion],
                                            date_request: fecha.strftime("%d/%m/%Y"),
                                            hour_request: fecha.strftime("%H:%M"),
                                            place: permit_params[:lugar],
                                            additional_data: permit_params[:datos_adicionales],
                                            district: Address.district_list.find_by("cast(districts.code as int) = cast(? as int)", permit_params[:cod_distrito]),
                                            borought: Address.borougth_list(permit_params[:cod_distrito]).find_by("cast(boroughts.code as int)=cast(? as int)", permit_params[:cod_barrio]),
                                            status: "active",
                                            status_service: StatusService.find_by(code:  "active"))
                                        if sr.save
                                            CitizenServiceTracking.create(:citizen_service=> sr,
                                                :tracking_type => TrackingType.find_by(alias_name: "subscribe"),
                                                :manager => nil,
                                                :tracked_at => Time.zone.now,
                                                :coments => "Servicio (#{sr.id}): Se ha generado en la aplicación móvil")
                                            CitizenTracking.create(:citizen=> sr.citizen,
                                                :tracking_type => TrackingType.find_by(alias_name: "subscribe"),
                                                :manager => nil,
                                                :tracked_at => Time.zone.now,
                                                :coments => "Servicio (#{sr.id}): Se ha generado el servicio en la aplicación móvil")
                                        
                                            if !sr.volunteer.blank?
                                                Volun::Tracking.create(:volunteer=> sr.volunteer,
                                                    :tracking_type => TrackingType.find_by(alias_name: "subscribe"),
                                                    :manager => nil,
                                                    :tracked_at => Time.zone.now,
                                                    :comments => "Servicio (#{sr.id}): Se ha generado el servicio en la aplicación móvil")
                                            end
                                            AuditGenerateHelper.input_parameters("create_request", sr, current_user)
                                            success({id_servicio: sr.id.to_i})
                                        else
                                            error("error03")
                                        end
                                    else
                                        error("token")
                                    end
                                else
                                    error("error55")
                                end
                            end
                        rescue
                            error("error34", "fecha y hora")
                        end
                    end
                end
            end
        end
    rescue => e
        begin
            Rails.logger.error("ERROR-WS: register_request -> #{e}")
        rescue
        end
        error("service")      
    end

    # PUT /volun/ws/request/:id
    # ENTRADA => {id,status,motivo}
    # SALIDA => {id_servicio}
    def update_request
        if strong_request_update_params
            permit_params = params.permit(:id,:status, :motivo) 

            if ["Volunteer", "Citizen"].include?(current_user.loggable.class.name.to_s)
                if current_user.loggable.class.name.to_s == "Volunteer"
                    service = CitizenService.find_by(" id = ? AND volunteer_id=?", permit_params[:id], current_user.loggable.id)
                elsif current_user.loggable.class.name.to_s == "Citizen" 
                    service = CitizenService.find_by(id: permit_params[:id], citizen_id: current_user.loggable.id)
                end

                invalid_param = valid_params(permit_params.except(:motivo))
                if invalid_param != true
                    error("error13", invalid_param)
                elsif service.blank?
                    error("error25") 
                elsif CitizenService.find_by(id: permit_params[:id]).blank? && !service.blank?
                    error("error34", "id")
                else                                                       
                    if !permit_params[:status].blank? && !Service.status_type_list.include?(permit_params[:status].to_sym) 
                        error("error37")
                    else
                        sr = CitizenService.find(permit_params[:id]) 
                        if !sr.blank?                       
                            sr.status = permit_params[:status].blank? ? sr.status : permit_params[:status]
                            sr.status_service = StatusService.find_by(code:  sr.status)
                            if current_user.loggable.class.name.to_s == "Volunteer"
                                sr.volunteer_reason = permit_params[:motivo]
                            elsif current_user.loggable.class.name.to_s == "Citizen" 
                                sr.citizen_reason = permit_params[:motivo]
                            end

                            if sr.save
                                begin
                                    if sr.status == "finished"
                                        CitizenServiceTracking.create(:citizen_service=> sr,
                                            :tracking_type => TrackingType.find_by(alias_name: "finished_service"),
                                            :manager => nil,
                                            :tracked_at => Time.zone.now,
                                            :coments => "Servicio (#{sr.id}): Se ha finalizado el servico en la aplicación móvil")
                                        CitizenTracking.create(:citizen=> sr.citizen,
                                            :tracking_type => TrackingType.find_by(alias_name: "finished_service"),
                                            :manager => nil,
                                            :tracked_at => Time.zone.now,
                                            :coments => "Servicio (#{sr.id}): Se ha finalizado el servico en la aplicación móvil")
                                    
                                        if !sr.volunteer.blank?
                                            Volun::Tracking.create(:volunteer=> sr.volunteer,
                                                :tracking_type => TrackingType.find_by(alias_name: "finished_service"),
                                                :manager => nil,
                                                :tracked_at => Time.zone.now,
                                                :comments => "Servicio (#{sr.id}): Se ha finalizado el servico en la aplicación móvil")
                                        end
                                    end
                                
                                    AuditGenerateHelper.input_parameters("update_request", sr, current_user)
                                rescue
                                end
                                success({id_servicio: sr.id.to_i})
                            else
                                error("error22", sr.errors.full_messages)
                            end
                        else
                            error("error23", permit_params[:id])
                        end
                    end                
                end
            else
                error("token")
            end
        end
    rescue => e
        begin
            Rails.logger.error("ERROR-WS: update_request -> #{e}")
        rescue
        end
        error("service")  
    end

    # POST /volun/ws/request/:id/cancel
    # ENTRADA => {id,motivo}
    # SALIDA => {id_servicio}
    def request_cancel
        if strong_request_cancel_params
            permit_params = params.permit(:id,:motivo) 
            invalid_param = valid_params(permit_params.except(:motivo))
            if invalid_param != true
                error("error13", invalid_param)
            else 
                if current_user.loggable.class.name.to_s == "Citizen"  
                    sr = CitizenService.find_by(id: permit_params[:id], citizen_id: current_user.loggable.id)                
                    if !sr.blank?                   
                        if  sr.canceled_at.blank?
                            sr.canceled_at = Time.zone.now
                            sr.citizen_reason = permit_params[:motivo]
                            sr.status = "canceled"
                            sr.status_service = StatusService.find_by(code:  sr.status)
                            sr.reason_manage_canceled = "Cancelado por la persona mayor"
                            sr.reason_manage_canceled_at = Time.zone.now

                            if sr.save
                                CitizenServiceTracking.create(:citizen_service=> sr,
                                    :tracking_type => TrackingType.find_by(alias_name: "cancel_service"),
                                    :manager => nil,
                                    :tracked_at => Time.zone.now,
                                    :coments => "Servicio (#{sr.id}): Se ha cancelado el servico en la aplicación móvil")
                                CitizenTracking.create(:citizen=> sr.citizen,
                                    :tracking_type => TrackingType.find_by(alias_name: "cancel_service"),
                                    :manager => nil,
                                    :tracked_at => Time.zone.now,
                                    :coments => "Servicio (#{sr.id}): Se ha cancelado el servico en la aplicación móvil")
                            
                                if !sr.volunteer.blank?
                                    Volun::Tracking.create(:volunteer=> sr.volunteer,
                                        :tracking_type => TrackingType.find_by(alias_name: "cancel_service"),
                                        :manager => nil,
                                        :tracked_at => Time.zone.now,
                                        :comments => "Servicio (#{sr.id}): Se ha cancelado el servico en la aplicación móvil")
                                end
                                AuditGenerateHelper.input_parameters("cancel_request", sr, current_user)
                                success({ id_servicio: sr.id.to_i })
                            else
                                error("error30")
                            end
                        else
                            error("error30")
                        end                    
                    else
                        error("error23", permit_params[:id], :bad_request)
                    end                
                else
                    error("token")
                end
            end
        end
    rescue => e
        begin
            Rails.logger.error("ERROR-WS: request_cancel -> #{e}")
        rescue
        end
        error("service") 
    end

    # POST /volun/ws/request/:id/accept
    # ENTRADA => {id}
    # SALIDA => {id_servicio}
    def request_accept
        if strong_request_accept_params
            permit_params = params.permit(:id) 
            invalid_param = valid_params(permit_params)
            if invalid_param != true
                error("error13", invalid_param)
            else 
                if current_user.loggable.class.name.to_s == "Volunteer"
                    if can_accept_service(permit_params[:id]) 
                        sr = CitizenService.find(permit_params[:id])    
                        if current_user.loggable.tester && sr.citizen.tester || !current_user.loggable.tester && !sr.citizen.tester       
                            if !sr.blank?
                                if ["active","prioritary"].include? sr.status
                                    if sr.volunteer.blank?
                                        begin
                                            fecha = Time.zone.now
                                            fecha_servicio = Time.zone.parse("#{sr.date_request} #{sr.hour_request}") 
                                            fecha_creacion = sr.created_at

                                            if fecha <= fecha_creacion || fecha > fecha_servicio
                                                error("error40")
                                            else
                                                sr.volunteer = current_user.loggable
                                                sr.date_accept_request = fecha.strftime("%d/%m/%Y")
                                                sr.hour_accept_request = fecha.strftime("%H:%M")
                                                sr.rejected_at = nil
                                                sr.volunteer_reason = nil
                                                sr.status = "accept"
                                                sr.status_service = StatusService.find_by(code:  sr.status)
                                        
                                                if sr.save
                                                    begin
                                                        if sr.citizen.enabled_app == "Mayor con App móvil" || sr.citizen.enabled_app.blank?
                                                            SMSApi.new.sms_deliver(sr.citizen.phone, "#{sr.volunteer.try(:full_name).to_s} será la persona que te acompañará a #{sr.try(:type_service).to_s} el #{Time.zone.parse(sr.try(:date_request).to_s).strftime("%d/%m/%Y")} a las #{sr.try(:hour_request).to_s}. Para confirmar el acompañamiento o cualquier aclaración del mismo debes ponerte en contacto con #{sr.volunteer.try(:full_name).to_s} a través del chat.")
                                                        else
                                                            SMSApi.new.sms_deliver(sr.citizen.phone, "#{sr.volunteer.try(:full_name).to_s} será la persona que te acompañará a #{sr.try(:type_service).to_s} el #{Time.zone.parse(sr.try(:date_request).to_s).strftime("%d/%m/%Y")} a las #{sr.try(:hour_request).to_s}. Para cualquier cambio o cancelación del acompañamiento debes ponerte en contacto con el 900 777 888.")
                                                        end
                                                    rescue =>error
                                                        begin
                                                            Rails.logger.error("ERROR-WS: request_accept(SMS) -> #{error}")
                                                        rescue
                                                        end
                                                    end
                                                    begin
                                                        CitizenServiceTracking.create(:citizen_service=> sr,
                                                            :tracking_type => TrackingType.find_by(alias_name: "accept_service"),
                                                            :manager => nil,
                                                            :tracked_at => Time.zone.now,
                                                            :coments => "Servicio (#{sr.id}): Se ha aceptado el servico en la aplicación móvil por el voluntario #{sr.volunteer.id_autonum}")
                                                        CitizenTracking.create(:citizen=> sr.citizen,
                                                            :tracking_type => TrackingType.find_by(alias_name: "accept_service"),
                                                            :manager => nil,
                                                            :tracked_at => Time.zone.now,
                                                            :coments => "Servicio (#{sr.id}): Se ha aceptado el servico en la aplicación móvil por el voluntario #{sr.volunteer.id_autonum}")
                                                    
                                                        if !sr.volunteer.blank?
                                                            Volun::Tracking.create(:volunteer=> sr.volunteer,
                                                                :tracking_type => TrackingType.find_by(alias_name: "accept_service"),
                                                                :manager => nil,
                                                                :tracked_at => Time.zone.now,
                                                                :comments => "Servicio (#{sr.id}): Se ha aceptado el servico en la aplicación móvil por el voluntario #{sr.volunteer.id_autonum}")
                                                        end
                                                        
                                                        AuditGenerateHelper.input_parameters("accept_request", sr, current_user)
                                                    rescue
                                                    end
                                                    success({ id_servicio: sr.id.to_i })
                                                else
                                                    error("error05")
                                                end
                                            end
                                        rescue
                                            error("error34", "fecha y hora")
                                        end
                                    else
                                        error("error05")
                                    end
                                else
                                    error("error05")
                                end
                            else
                                error("error23", permit_params[:id], :bad_request)
                            end
                        else
                            error("error05")
                        end
                    else
                        error("error56")
                    end
                else
                    error("token")
                end
            end
        end
    rescue => e
        begin
            Rails.logger.error("ERROR-WS: request_accept -> #{e}")
        rescue
        end
        error("service") 
    end

    # POST /volun/ws/request/:id/reject
    # ENTRADA => {id,motivo}
    # SALIDA => {id_servicio}
    def request_reject
        if strong_request_reject_params
            permit_params = params.permit(:id,:motivo) 
            invalid_param = valid_params(permit_params.except(:motivo))
            if invalid_param != true
                error("error13", invalid_param)
            else   
                if current_user.loggable.class.name.to_s == "Volunteer" 
                    sr = CitizenService.find_by(id: permit_params[:id], volunteer_id: current_user.loggable.id)
                    if !sr.blank?                     
                        if sr.rejected_at.blank?
                            id = sr.try(:volunteer).try(:id_autonum)
                            aux_volunteer = sr.volunteer
                            sr.volunteer = nil
                            sr.date_accept_request = nil
                            sr.hour_accept_request = nil
                            sr.rejected_at = Time.zone.now
                            sr.volunteer_reason = permit_params[:motivo]
                            permit_create = Setting.find_by(key: "VolunBackend.ws_permit_create_service")

                            fecha = Time.zone.parse("#{sr.date_request} #{sr.hour_request}")
                            horas = permit_create.blank? ? 48 :  permit_create.value.to_i

                            if fecha < Time.zone.now
                                sr.status = "expired"
                            elsif fecha - (horas.to_i/2).hours >= Time.zone.now 
                                sr.status = "active"
                            else 
                                sr.status = "prioritary"
                            end
                            sr.status_service = StatusService.find_by(code:  sr.status)
                    
                            if sr.save
                                begin
                                    SMSApi.new.sms_deliver(sr.citizen.phone, "#{aux_volunteer.try(:full_name).to_s} no va a poder acompañarle a #{sr.try(:type_service).to_s} el #{sr.try(:date_request).to_s} a las #{sr.try(:hour_request).to_s}. Su petición volverá a estar activa para que otra persona voluntaria pueda aceptarla.")
                                rescue =>error
                                    begin
                                        Rails.logger.error("ERROR-WS: request_reject(SMS) -> #{error}")
                                    rescue
                                    end
                                end


                                CitizenServiceTracking.create(:citizen_service=> sr,
                                    :tracking_type => TrackingType.find_by(alias_name: "reject_service"),
                                    :manager => nil,
                                    :tracked_at => Time.zone.now,
                                    :coments => "Servicio (#{sr.id}): Se ha rechazado el servico en la aplicación móvil por el voluntario #{id}")
                                CitizenTracking.create(:citizen=> sr.citizen,
                                    :tracking_type => TrackingType.find_by(alias_name: "reject_service"),
                                    :manager => nil,
                                    :tracked_at => Time.zone.now,
                                    :coments => "Servicio (#{sr.id}): Se ha rechazado el servico en la aplicación móvil por el voluntario #{id}")
                            
                                if !aux_volunteer.blank?
                                    Volun::Tracking.create(:volunteer=> aux_volunteer,
                                        :tracking_type => TrackingType.find_by(alias_name: "reject_service"),
                                        :manager => nil,
                                        :tracked_at => Time.zone.now,
                                        :comments => "Servicio (#{sr.id}): Se ha rechazado el servico en la aplicación móvil por el voluntario #{id}")
                                end
                                AuditGenerateHelper.input_parameters("reject_request", sr, current_user)
                                success({ id_servicio: sr.id.to_i })
                            else
                                error("error31")
                            end
                        else
                            error("error31")
                        end                   
                    else
                        error("error23", permit_params[:id], :bad_request)
                    end
                else
                    error("token") 
                end
            end
        end
    rescue => e
        begin
            Rails.logger.error("ERROR-WS: request_reject -> #{e}")
        rescue
        end
        error("service") 
    end

    # POST /volun/ws/request/:id/rate
    # ENTRADA => {id,valoracion}
    # SALIDA => {id_servicio}
    def request_vote
        if strong_request_vote_params
            permit_params = params.permit(:id,:valoracion)
            if current_user.loggable.class.name.to_s == "Citizen" 
                invalid_param = valid_params(permit_params)
                if invalid_param != true
                    error("error13", invalid_param)
                elsif permit_params[:valoracion].to_i < 0 || permit_params[:valoracion].to_i > 5
                    error("error48")
                else                   
                    sr = CitizenService.find_by(id: permit_params[:id], citizen_id: current_user.loggable.id)
                    if !sr.blank?
                        if sr.status.to_s != Service.status_type_list[4].to_s
                            error("error49")
                        elsif !sr.volunteer.blank?
                            srv = ServiceVote.find_by(citizen_service: sr, type_user: "Volunteer")             
                            if srv.blank?
                                sr.volunteer_vote_rel = ServiceVote.new(citizen_service: sr, type_user: "Volunteer", vote: permit_params[:valoracion], comments: permit_params[:comment])
                                sr.volunteer_vote = permit_params[:valoracion]
                                if sr.save
                                    AuditGenerateHelper.input_parameters("vote_request", sr, current_user)
                                    success({id_servicio: sr.id.to_i})
                                else
                                    error("error32")
                                end
                            else
                                error("error33",  "Voluntario" , :bad_request)
                            end
                        else
                            error("error41")
                        end
                    else
                        error("error23", permit_params[:id])
                    end
                end
            elsif current_user.loggable.class.name.to_s == "Volunteer" 
                invalid_param = valid_params(permit_params)
                if invalid_param != true
                    error("error13", invalid_param)
                elsif permit_params[:valoracion].to_i < 0 || permit_params[:valoracion].to_i > 5
                    error("error48")
                else     
                    sr = CitizenService.find_by(id: permit_params[:id], volunteer_id: current_user.loggable.id)
                    if !sr.blank?
                        if sr.status.to_s != Service.status_type_list[4].to_s
                            error("error49")
                        elsif !sr.citizen.blank?
                            srv = ServiceVote.find_by(citizen_service: sr, type_user: "Citizen")             
                            if srv.blank?
                                sr.citizen_vote_rel = ServiceVote.new(citizen_service: sr, type_user: "Citizen", vote: permit_params[:valoracion], comments: permit_params[:comment])
                                sr.citizen_vote = permit_params[:valoracion]
                                if sr.save
                                    sr.citizen.update_average_vote
                                    AuditGenerateHelper.input_parameters("vote_request", sr, current_user)
                                    success({id_servicio: sr.id.to_i})
                                else
                                    error("error32")
                                end
                            else
                                error("error33",  "Mayor" , :bad_request)
                            end
                        else
                            error("error41")
                        end
                    else
                        error("error23", permit_params[:id])
                    end
                end
            else
                error("token") 
            end
        end
    rescue => e
        begin
            Rails.logger.error("ERROR-WS: request_vote -> #{e}")
        rescue
        end
        error("service")  
    end

   
    # POST /volun/ws/request/:id/questions/rate
    # ENTRADA => {id,questions => [{id,response}]}
    # SALIDA => {id_servicio}
    def request_rate
        if strong_request_rate_params
            permit_params = params.permit(:id,{:questions => [:id, :response]})
           
            if current_user.loggable.class.name.to_s == "Volunteer"
                user_type = "Volunteer"
                service = CitizenService.find_by(id: permit_params[:id], volunteer_id: current_user.loggable.id)
            elsif current_user.loggable.class.name.to_s == "Citizen"
                user_type = "Citizen"
                service = CitizenService.find_by(id: permit_params[:id], citizen_id: current_user.loggable.id)
            else
                error("token") 
            end

            if service.blank? || permit_params[:questions].blank?
                error("error25") 
            else
                errores = []
                permit_params[:questions].each do |question|
                    aux_question = ServiceQuestion.find_by(id: question[:id])
                    if aux_question.blank? || question[:id].blank?
                        errores.push(question[:id])
                    else
                        exist = ServiceResponse.find_by(service_question_id: question[:id], citizen_service: service)
                        if exist.blank?
                            exist = ServiceResponse.new(
                                service_question_id: question[:id], 
                                response: question[:response], 
                                citizen_service: service, 
                                user_id: current_user.loggable.id, 
                                user_type: user_type)
                        else
                            exist.response = question[:response]
                        end

                        errores.push(question[:id]) if !exist.save
                    end
                end

                AuditGenerateHelper.input_parameters("request_rate", CitizenService.find(permit_params[:id]), current_user)

                if errores.count > 0
                    success({id_servicio: permit_params[:id].to_i, questions_error: errores })
                else
                    success(id_servicio: permit_params[:id].to_i)
                end
            end
        end
    rescue => e
        begin
            Rails.logger.error("ERROR-WS: request_rate -> #{e}")
        rescue
        end
        error("service")  
    end

    

    # GET /volun/ws/request/:id
    # ENTRADA => {id,version}
    # SALIDA => {descripcion,fecha,hora,lugar,datos_adicionales,id,id_tipo,tipo,distrito,cod_distrito,barrio,cod_barrio,status,id_voluntario,nombre_voluntario,nombre_voluntario,imagen_voluntario,id_ciudadano}
    def request_simple
        if strong_request_id_params  
            permit_params = params.permit(:id,:version)
            if ["Volunteer", "Citizen"].include?(current_user.loggable.class.name.to_s)
                if current_user.loggable.class.name.to_s == "Volunteer"
                    if current_user.loggable.tester
                        service = CitizenService.find_by(" citizen_services.id = ? AND (volunteer_id is null OR volunteer_id=?)", permit_params[:id], current_user.loggable.id)
                    else
                        service = CitizenService.not_tester.find_by(" citizen_services.id = ? AND (volunteer_id is null OR volunteer_id=?)", permit_params[:id], current_user.loggable.id)
                    end
                elsif current_user.loggable.class.name.to_s == "Citizen" 
                    service = CitizenService.find_by(id: permit_params[:id], citizen_id: current_user.loggable.id)
                end

                if permit_params[:version].blank? || !permit_params[:version].blank? && permit_params[:version].to_s!="true"
                   if service.service.code.to_i >= 9
                        service = nil
                   end
                end

                if !CitizenService.find_by(id: permit_params[:id]).blank? && service.blank?
                    error("token")
                elsif service.blank?
                    error("error25") 
                else
                    begin
                        if !service.try(:volunteer).try(:logo).try(:file).blank?
                            data = File.open(service.volunteer.logo.file.path(:thumb)).read
                            encoded = Base64.encode64(data)
                        else
                            encoded = nil
                        end
                    rescue
                        encoded = nil
                    end

                    if ["active","prioritary"].include? service.status
                        permit_create = Setting.find_by(key: "VolunBackend.ws_permit_create_service")
    
                        fecha = Time.zone.parse("#{service.date_request} #{service.hour_request}")
                        horas = permit_create.blank? ? 48 :  permit_create.value.to_i
    
                        if fecha < Time.zone.now
                            service.status = "expired"
                        elsif fecha - (horas.to_i/2).hours >= Time.zone.now 
                            service.status = "active"
                        else 
                            service.status = "prioritary"
                        end
                        service.status_service = StatusService.find_by(code:  service.status)
                        service.save
                    end


                    fecha = Time.zone.parse("#{service.date_request} #{service.hour_request}")
                    if !permit_params[:version].blank? && permit_params[:version].to_s=="true"
                        aux_res = {
                            descripcion: service.description,
                            fecha: fecha.strftime("%d/%m/%Y"),
                            hora: fecha.strftime("%H:%M"),
                            lugar: service.place,
                            datos_adicionales: service.additional_data,
                            id: service.id.to_i,
                            id_tipo: service.service_id.to_i,
                            tipo: s.service.try(:name),
                            distrito: service.district.blank? ? nil : service.district.try(:name),
                            cod_distrito:  service.district.try(:code), 
                            barrio: service.borought.blank? ? nil : service.borought.try(:name),
                            cod_barrio: service.borought.try(:code),
                            status: service.status,
                            id_voluntario: service.volunteer_id.to_i,
                            cid360_voluntario: service.volunteer.try(:id).to_i,
                            nombre_voluntario: service.volunteer.try(:full_name),
                            imagen_voluntario: encoded,
                            id_ciudadano: service.citizen_id.to_i,
                            cid360_ciudadano: service.citizen.try(:id).to_i
                        }
                    else
                        aux_res = {
                            descripcion: service.description,
                            fecha: fecha.strftime("%d/%m/%Y"),
                            hora: fecha.strftime("%H:%M"),
                            lugar: service.place,
                            datos_adicionales: service.additional_data,
                            id: service.id.to_i,
                            id_tipo: service.service_id.to_i,
                            distrito: service.district.blank? ? nil : service.district.try(:name),
                            cod_distrito:  service.district.try(:code), 
                            barrio: service.borought.blank? ? nil : service.borought.try(:name),
                            cod_barrio: service.borought.try(:code),
                            status: service.status,
                            id_voluntario: service.volunteer_id.to_i,
                            cid360_voluntario: service.volunteer.try(:id).to_i,
                            nombre_voluntario: service.volunteer.try(:full_name),
                            imagen_voluntario: encoded,
                            id_ciudadano: service.citizen_id.to_i,
                            cid360_ciudadano: service.citizen.try(:id).to_i
                        }
                    end
                    AuditGenerateHelper.input_parameters("request_simple", current_user, current_user)
                    success(aux_res)
                end
            else
                error("token")
            end
        end       
    rescue => e
        begin
            Rails.logger.error("ERROR-WS: request_simple -> #{e}")
        rescue
        end
        error("service")  
    end

    # POST /volun/ws/messages/
    # ENTRADA => {body,date_at,file,person_id}
    # SALIDA => {id}
    def create_message
        if strong_message_create_params
            permit_params = params.permit(:date_at,:body,:file,:person_id)            
            if ["Volunteer", "Citizen"].include?(current_user.loggable.class.name.to_s)
                if permit_params[:body].to_s.strip.blank? && permit_params[:file].to_s.strip.blank?
                    error("error78")
                elsif !permit_params[:date_at].blank? && permit_params[:date_at].to_s.match(/\d{2}\/\d{2}\/\d{4}[ ]\d{2}\:\d{2}|\d{4}-\d{2}-\d{2}[ ]\d{2}\:\d{2}/).to_s.strip.blank?
                    error("error71")
                elsif permit_params[:person_id].to_s.strip.match(/[0-9]+/).to_s.strip.blank?
                    error("error79")
                else 
                    file = nil
                    begin
                        if !permit_params[:file].blank?
                            StringIO.open(Base64.decode64(permit_params[:file])) do |data|                                
                                data.class.class_eval { attr_accessor :original_filename, :content_type }
                                data.original_filename = "logo.m4a"
                                data.content_type = "audio/m4a"
                                file = data                               
                            end
                        end
                    rescue
                    end
                    if current_user.loggable.class.name.to_s == "Volunteer"
                        person = Citizen.find_by(id: permit_params[:person_id])

                        message = WsMessage.new(citizen_id:  permit_params[:person_id],
                            volunteer_id: current_user.loggable.id, 
                            body: permit_params[:body],
                            attach: file,
                            type_author: "Volunteer",
                            date_at: Time.zone.parse(permit_params[:date_at])
                        )
                    elsif current_user.loggable.class.name.to_s == "Citizen"
                        person = Volunteer.find_by(id: permit_params[:person_id])
                        message = WsMessage.new(volunteer_id:  permit_params[:person_id],
                            citizen_id: current_user.loggable.id, 
                            body: permit_params[:body],
                            attach: file,
                            type_author: "Citizen",
                            date_at: Time.zone.parse(permit_params[:date_at])
                        )
                    end
                    if person.blank?
                        error("error79")
                    elsif message.save
                        AuditGenerateHelper.input_parameters("create_message", current_user, current_user)                       
                        success({id: message.id})
                    else
                        error("error80")
                    end
                end
            else
                error("token")
            end
        end
    rescue => e
        begin
            Rails.logger.error("ERROR-WS: create_message -> #{e}")
        rescue
        end
        error("service")
    end

    # PUT /volun/ws/messages/:id
    # ENTRADA => {id,body,file}
    # SALIDA => {id}
    def update_message
        if strong_message_update_params
            permit_params = params.permit(:id,:body,:file)            
            if ["Volunteer", "Citizen"].include?(current_user.loggable.class.name.to_s)
                if permit_params[:body].to_s.strip.blank? && permit_params[:file].to_s.strip.blank?
                    error("error78")
                elsif permit_params[:id].to_s.strip.match(/[0-9]+/).to_s.strip.blank?
                    error("token")                
                else 
                    if current_user.loggable.class.name.to_s == "Volunteer"
                        message = WsMessage.find_by(id: permit_params[:id], volunteer_id: current_user.loggable.id)
                    elsif current_user.loggable.class.name.to_s == "Citizen"
                        message = WsMessage.find_by(id: permit_params[:id], citizen_id: current_user.loggable.id)
                    end

                    if !message.blank? && message.type_author.to_s == current_user.loggable.class.name.to_s                        
                        begin
                            if !permit_params[:file].blank?
                                StringIO.open(Base64.decode64(permit_params[:file])) do |data|                                
                                    data.class.class_eval { attr_accessor :original_filename, :content_type }
                                    data.original_filename = "logo.m4a"
                                    data.content_type = "audio/m4a"
                                    message.attach = data                               
                                end
                            end
                        rescue
                            message.attach=nil
                        end
                        message.body = permit_params[:body]
                        if message.save
                            AuditGenerateHelper.input_parameters("update_message", current_user, current_user)                       
                            success({id: message.id})
                        else
                            puts message.errors.full_messages
                            puts "="*50
                            error("error80")
                        end
                    else
                        error("token")
                    end
                end
            else
                error("token")
            end
        end
    rescue => e
        begin
            Rails.logger.error("ERROR-WS: update_message -> #{e}")
        rescue
        end
        error("service")
    end

    # GET /volun/ws/messages/:id
    # ENTRADA => {id}
    # SALIDA => {id, body, file, date_at,type_author, volunteer_id,citizen_id}
    def get_message
        if strong_message_id_params
            permit_params = params.permit(:id)
            if permit_params[:id].to_s.strip.match(/[0-9]+/).to_s.strip.blank?
                error("token")     
            elsif ["Volunteer", "Citizen"].include?(current_user.loggable.class.name.to_s)
                if current_user.loggable.class.name.to_s == "Volunteer"
                    message = WsMessage.find_by(id: permit_params[:id],volunteer_id: current_user.loggable.id )
                elsif current_user.loggable.class.name.to_s == "Citizen"
                    message = WsMessage.find_by(id: permit_params[:id],citizen_id: current_user.loggable.id )
                end
                if !message.blank?
                    AuditGenerateHelper.input_parameters("get_message", current_user, current_user)
                    begin    
                        if !message.try(:attach).blank?
                            data = File.open(message.try(:attach).path).read
                            encoded = Base64.encode64(data)                      
                        else
                            encoded = nil
                        end
                    rescue
                        encoded = nil
                    end
        
                    aux_res = {
                        id: message.id, 
                        body: message.body, 
                        file: encoded, 
                        date_at: message.date_at.strftime("%d/%m/%Y %H:%M"), 
                        type_author: message.type_author,
                        volunteer_id: message.volunteer_id,
                        citizen_id: message.citizen_id
                    }
                    success(aux_res)
                else
                    error("token")
                end
            else
                error("token")
            end
        end
    rescue => e
        begin
            Rails.logger.error("ERROR-WS: get_message -> #{e}")
        rescue
        end
        error("service")
    end

    # POST /volun/ws/messages/list
    # ENTRADA => {person_id, limit}
    # SALIDA => {count, value => [{id, body, file, date_at,type_author, volunteer_id,citizen_id}]}
    def list_messages
        if strong_messages_params
            permit_params = params.permit(:person_id,:limit)
            if permit_params[:person_id].to_s.strip.match(/[0-9]+/).to_s.strip.blank?
                error("token")     
            elsif ["Volunteer", "Citizen"].include?(current_user.loggable.class.name.to_s)
                if current_user.loggable.class.name.to_s == "Volunteer"
                    messages = WsMessage.where(citizen_id: permit_params[:person_id],volunteer_id: current_user.loggable.id )
                elsif current_user.loggable.class.name.to_s == "Citizen"
                    messages = WsMessage.where(volunteer_id: permit_params[:person_id],citizen_id: current_user.loggable.id )
                end
                if !messages.blank?
                    AuditGenerateHelper.input_parameters("list_messages", current_user, current_user)
                    aux_res = []

                    if !permit_params[:limit].blank? && !permit_params[:limit].to_s.strip.match(/[0-9]+/).to_s.strip.blank?
                        messages = messages.limit(permit_params[:limit].to_i)
                    end
                    begin    
                        if !message.try(:attach).blank?
                            data = File.open(message.try(:attach).path).read
                            encoded = Base64.encode64(data)                      
                        else
                            encoded = nil
                        end
                    rescue
                        encoded = nil
                    end

                    messages.order(date_at: :desc).each do |message|
                        aux_res.push({
                            id: message.id, 
                            body: message.body,
                            file: encoded, 
                            date_at: message.date_at.strftime("%d/%m/%Y %H:%M"), 
                            type_author: message.type_author,
                            volunteer_id: message.volunteer_id,
                            citizen_id: message.citizen_id
                        })
                    end
                    success({count: aux_res.count, value: aux_res})
                else
                    success({count: 0, value: []})
                end
            else
                error("token")
            end
        end
    rescue => e
        begin
            Rails.logger.error("ERROR-WS: list_messages -> #{e}")
        rescue
        end
        error("service")
    end


    private

    def strong_request_id_params
        strong_params_validate([:id]).to_s == "true"
    end

    def strong_message_id_params
        strong_params_validate([:id]).to_s == "true"
    end

    def strong_message_create_params
        strong_params_validate([:person_id,:date_at]).to_s == "true"
    end

    def strong_message_update_params
        strong_params_validate([:id]).to_s == "true"
    end

    
    def strong_messages_params
        strong_params_validate([:person_id]).to_s == "true"
    end

    def strong_request_params
        strong_params_validate([:codigo_de_servicio,:descripcion,:fecha,:hora,:lugar,:datos_adicionales,:cod_distrito,:cod_barrio]).to_s == "true"
    end

    def strong_request_update_params
        strong_params_validate([:id,:status, :motivo]).to_s == "true"
    end

    def strong_request_search_params
        strong_params_validate([:type,:code_cid360,:status,:cod_servicios,:cod_distritos,:cod_barrios,:items,:skip,:orden]).to_s == "true"
    end

    def strong_request_cancel_params
        strong_params_validate([:id,:motivo]).to_s == "true"
    end

    def strong_request_accept_params
        strong_params_validate([:id]).to_s == "true"
    end

    def strong_request_reject_params
        strong_params_validate([:id,:motivo]).to_s == "true"
    end

    def strong_request_vote_params
        strong_params_validate([:id,:valoracion]).to_s == "true"
    end

    def strong_request_rate_params
        strong_params_validate([:id,:questions]).to_s == "true"
    end


    def valid_params(params)
        
       params.each do |key,value|
            unless key == "controller"
                return key if value.blank?
            end
        end

        true
    end

    def can_register_service(new_date)
        # services = CitizenService.where(citizen_id: current_user.loggable_id).all_prepared_active
        # return true if services.blank?
        # services.each do |s|
        #     if s.date_request.to_date.strftime("%d-%m-%Y") == new_date.to_s.to_date.strftime("%d-%m-%Y")
        #             # s_date = Time.zone.parse("#{s.date_request} #{s.hour_request}")
        #             # if s_date > new_date
        #             #     res = s_date >= ( new_date + 120.minutes )
        #             #     return false if res.to_s == "false"
        #             # elsif s_date < new_date
        #             #     res = new_date >= (  s_date  + 120.minutes )
        #             #     return false if res.to_s == "false"
        #             # elsif s_date == new_date
        #             #     return false
        #             # end
        #         return false
        #     end
        # end
        # true

        services = CitizenService.where(citizen_id: current_user.loggable_id).where("cast(date_request as date) = cast(? as date)", new_date).all_prepared_active
        if !services.blank?
            return false
        end  
        true
    end

    def can_accept_service(id)
        services = CitizenService.where(volunteer_id: current_user.loggable_id).all_prepared_active
        return true if services.blank?
        accept_service = CitizenService.find(id)
        services.each do |s|
            if s.date_request.to_date.strftime("%d-%m-%Y") == accept_service.date_request.to_date.strftime("%d-%m-%Y")
                s_date = Time.zone.parse("#{s.date_request} #{s.hour_request}")
                accept_date = Time.zone.parse("#{accept_service.date_request} #{accept_service.hour_request}")
                if s_date > accept_date
                    res = s_date >= ( accept_date + 120.minutes )
                    return false if res.to_s == "false"
                elsif s_date < accept_date
                    res = accept_date >= (  s_date  + 120.minutes )
                    return false if res.to_s == "false"
                elsif s_date == accept_date
                    return false
                end
            end
        end
        true
    end

end