class Ws::VolunteersController < Ws::GenericApiController
    
    before_action :skip_trackable, except: [:login]
    before_action :checktokenUser, except: [:login]
    before_action :authenticate!, unless: -> {user_authenticated?}, except: [:login]
    require 'base64'

    # POST /volun/ws/volunteer/login
    # ENTRADA => {nombre_usuario,password}
    # SALIDA => {cid360,autorizado,token}
    def login
        if strong_login_params 
            permit_params = params.permit(:nombre_usuario,:password)

            sign_out
            volun = Volunteer.find_by(email: permit_params[:nombre_usuario].to_s.strip.downcase)
           
            if !volun.try(:user).blank? && volun.user.valid_password?(permit_params[:password].to_s.strip)                
               
                sign_in(volun.user)
                autorizado = volun.projects.where("projects_volunteers.active=true AND (projects_volunteers.volunteer_type != 'DERIVATIE' OR projects_volunteers.volunteer_type is null) AND projects.accompany=true").first                
                AuditGenerateHelper.input_parameters("auth_volunteer", volun, current_user)

                if autorizado.blank?
                    error("error70")
                else
                    success({ 
                        cid360: volun.id.to_i,
                        autorizado: !autorizado.blank?,
                        token: !autorizado.blank? ? volun.user.authentication_token : ''
                    })
                end
            else
                error("error28")
            end
        end
    rescue => e
        begin
            Rails.logger.error("ERROR-WS: login -> #{e}")
        rescue
        end
        error("service")
    end

    # GET /volun/ws/volunteer/
    # ENTRADA => {}
    # SALIDA => {nombre,imagen}
    def get_volunteer
        if current_user.blank? || current_user.loggable.class.name.to_s != "Volunteer" 
            error("token")
        else        
            volun = current_user.loggable
            if !volun.blank?      
                begin
                    if !volun.try(:logo).try(:file).blank?
                        data = File.open(volun.logo.file.path(:thumb)).read
                        encoded = Base64.encode64(data)                      
                    else
                        encoded = nil
                    end
                rescue
                    encoded = nil
                end
                AuditGenerateHelper.input_parameters("get_volunteer", volun, current_user)
                success({ 
                    nombre: volun.full_name,
                    imagen: encoded
                })
            else
                error("error21", current_user.loggable.id, :bad_request)
            end
        end
    rescue => e
        begin
            Rails.logger.error("ERROR-WS: get_volunteer -> #{e}")
        rescue
        end
        error("service")
    end

    # GET /volun/ws/volunteer/:id
    # ENTRADA => {}
    # SALIDA => {cide360, nombre,imagen}
    def citizen_get_volunteer
        if current_user.blank? || current_user.loggable.class.name.to_s != "Citizen"
            error("token")
        else        
            volun = Volunteer.find_by(id: params[:id])
           
            if !volun.blank?  
                begin                 
                    if !volun.try(:logo).try(:file).blank?
                        data = File.open(volun.logo.file.path(:thumb)).read
                        encoded = Base64.encode64(data)                        
                    else
                        encoded = nil
                    end
                rescue
                    encoded = nil
                end
                AuditGenerateHelper.input_parameters("get_volunteer", volun, current_user)
                success({ 
                    cid360: volun.id.to_i,
                    nombre: volun.full_name,
                    imagen: encoded
                })
            else
                error("error21", params[:id], :bad_request)
            end
        end
    rescue => e
        begin
            Rails.logger.error("ERROR-WS: citizen_get_volunteer -> #{e}")
        rescue
        end
        error("service")
    end

    private

    def strong_login_params
        strong_params_validate([:nombre_usuario,:password]).to_s == "true"
    end

    
        
end