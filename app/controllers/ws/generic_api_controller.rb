class Ws::GenericApiController < ApplicationController
    respond_to :json
    protect_from_forgery unless: -> { request.format.json? }

    before_action :skip_trackable
    before_action :checktokenUser
    before_action :authenticate!, unless: -> {user_authenticated?}

    # POST /volun/ws/volunteer/refresh_login - /volun/ws/citizen/refresh_login
    # ENTRADA => {}
    # SALIDA => {token}
    def refresh_login
        begin
            Rails.logger.error("INFO-WS: Entrada en servicio de refresco - [#{Time.zone.now}]")
        rescue
        end
        if request.headers["HTTP_TOKEN_AUTHENTICATION"].blank?
            error("token")
        else
            user = User.find_by(authentication_token: request.headers["HTTP_TOKEN_AUTHENTICATION"])
            if !user.blank? && ["Volunteer", "Citizen"].include?(user.loggable_type.to_s)
                sign_out
                sign_in(user)
                user.save
                begin
                    Rails.logger.error("INFO-WS: Guardado y generación de token (Antes de respuesta)- [#{Time.zone.now}]")
                rescue
                end
                success({token: user.authentication_token})
            else
                error("error04")
            end
        end
    rescue  => e
        begin
            Rails.logger.error("ERROR-WS: refresh_login -> #{e}")
        rescue
        end
        error("service")   
    end
    
 # POST /ws/sms_generic/
    # ENTRADA => {message}
    # SALIDA => {respuesta}
    def sms_generic
      if strong_sms_generic_params
          permit_params = params.permit(:message)    
          if permit_params[:message].to_s.strip.blank?
            error("error81")
          else            
            phone = current_user.loggable_type == 'Volunteer' ? current_user.loggable.mobile_number : current_user.loggable.phone
          
            if  phone.to_s.strip.blank?
              error("error82")
            else
              begin
                  SMSApi.new.sms_deliver(phone.to_s.strip, permit_params[:message])
                  success({respuesta: "OK"})
              rescue =>error
                  begin
                      Rails.logger.error("ERROR-WS: send_sms(SMS) -> #{error}")
                  rescue
                  end
                  error("error75")
              end
            end
          end
      end
  rescue => e
      begin
          Rails.logger.error("ERROR-WS: send_sms -> #{e}")
      rescue
      end
     error("service")
  end

    protected

    def strong_sms_generic_params
      strong_params_validate([:message]).to_s == "true"
  end

        def strong_params_validate(aux_array)
            begin
                aux_array.each do |key|
                    if !params.key?(key)
                        return error("error13", key, :bad_request)
                    end
                end
                true
            rescue => e
                return error("error13", e.to_s.split(':')[1].strip(), :bad_request)
            end
        end

        def skip_trackable
            request.env['devise.skip_trackable'] = true
        end

        def error(error, code=nil, status = :error)
            json_response({:error => {mensaje: !code.blank? ? t("ws_accompany.errors.#{error}", code: code) : t("ws_accompany.errors.#{error}"), codigo: t("ws_accompany.cod_errors.#{error}")}}, status)
        end

        def success(message)
            json_response(message)
        end

        def json_citizen(citizen)
            begin    
                if !citizen.try(:logo).try(:file).blank?
                    data = File.open(citizen.logo.file.path(:thumb)).read
                    encoded = Base64.encode64(data)                      
                else
                    encoded = nil
                end
            rescue
                encoded = nil
            end

            {   
                id: citizen.id.to_i,               
                nombre: citizen.name,
                apellido1: citizen.last_name_1,
                apellido2: citizen.last_name_2,
                email: citizen.email,
                nif: citizen.document,
                tipo_nif: citizen.document_type_ws,
                movil: citizen.phone,
                fecha_nacimiento: citizen.birthday.blank? ? '' : Time.zone.parse(citizen.birthday.to_s).strftime("%d/%m/%Y"),
                pais: citizen.country,
                nivel_academico: citizen.academic_info,
                sexo: citizen.gender.try(:name),
                personas_en_casa: citizen.people_at_home.to_i,
                distrito: citizen.address.try(:district).try(:name),
                cod_distrito: citizen.address.try(:district).try(:code).to_s.rjust(2, '0'),
                barrio: citizen.address.try(:borought).try(:name),
                cod_barrio: citizen.address.try(:borought).try(:code).to_s.rjust(2, '0'),
                imagen: encoded
            }
        end

        def json_response(message, status = :ok)
            render json: message, status: status.to_s == "ok" ? 200 : status.to_s ==  "bad_request" ? 400 : 500
        end
        
        def checktokenUser
            if !request.headers[:HTTP_TOKEN_AUTHENTICATION].blank?
                user = User.find_by(authentication_token: request.headers[:HTTP_TOKEN_AUTHENTICATION])
                if !user.blank? && user.authentication_token_created_at + 6.hours >=  Time.zone.now
                    session[:current_user_id] = user.try :id
                    @current_user = user

                    !current_user.blank?
                else
                    session[:current_user_id] = nil
                    @current_user = nil
                    sign_out
                    error("token")
                end
            else
                session[:current_user_id] = nil
                @current_user = nil
                sign_out
                error("token")
            end
        end
end
