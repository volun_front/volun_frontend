class Ws::HelpersController < Ws::GenericApiController
    
    before_action :skip_trackable, except: [:citizen, :volunteer, :services, :padron]
    before_action :checktokenUser, except: [:citizen, :volunteer, :services, :padron]
    before_action :authenticate!, unless: ->{user_authenticated?}, except: [:citizen, :volunteer, :services, :padron]

    # GET /volun/ws/test/citizen
    # ENTRADA => {}
    # SALIDA => {token}
    def citizen
        citizen = Citizen.all.first
        user = citizen.user
        rand = SecureRandom.urlsafe_base64(40).tr('lIO0', 'sxyz')   
        token = [rand, Digest::SHA1.hexdigest([Time.zone.now,rand,user.try(:id) || citizen.id].join)].join
        success({token: token})
    rescue => e
        begin
            Rails.logger.error("ERROR-WS: helper_citizen -> #{e}")
        rescue
        end
        error("service")
    end

    # GET /volun/ws/test/volunteer
    # ENTRADA => {}
    # SALIDA => {token}
    def volunteer
        volun = Volunteer.all.first
        user = volun.user
        rand = SecureRandom.urlsafe_base64(40).tr('lIO0', 'sxyz')   
        token = [rand, Digest::SHA1.hexdigest([Time.zone.now,rand,user.try(:id) || volun.id].join)].join
        success({token: token})
    rescue => e
        begin
            Rails.logger.error("ERROR-WS: helper_volunteer -> #{e}")
        rescue
        end
        error("service")
    end
    
    # GET /volun/ws/test/services
    # ENTRADA => {}
    # SALIDA => {count,value={}}
    def services
        services = CitizenService.where(canceled_at: nil).order(date_request: :asc, hour_request: :asc).limit(5)

        service_list = []

        services.each do |s|
            begin
                if !s.try(:volunteer).try(:logo).try(:file).blank?
                    data = File.open(s.volunteer.logo.file.path).read
                    encoded = Base64.encode64(data)
                else
                    encoded = nil
                end
            rescue
                encoded = nil
            end

            fecha = Time.zone.parse("#{s.date_request} #{s.hour_request}")
        
            service_list.push({
                descripcion: s.description,
                fecha: fecha.strftime("%d/%m/%Y"),
                hora: fecha.strftime("%H:%M"),
                lugar: s.place,
                datos_adicionales: s.additional_data,
                id: s.id.to_i,
                id_tipo: s.service_id.to_i,
                distrito: s.district.try(:name),
                cod_distrito: s.district.try(:code), 
                barrio: s.borought.try(:name),
                cod_barrio: s.borought.try(:code),
                status: s.status,
                id_voluntario: s.volunteer_id.to_i,
                cid360_voluntario: s.volunteer.try(:id).to_i,
                nombre_voluntario: s.volunteer.try(:full_name),
                imagen_voluntario: encoded,
                id_ciudadano: s.citizen_id.to_i,
                cid360_ciudadano: s.citizen.try(:id).to_i
            })
        end
        success({count: service_list.count.to_i, value: service_list })
    rescue => e
        begin
            Rails.logger.error("ERROR-WS: helper_services -> #{e}")
        rescue
        end
        error("service")  
    end  

    # GET /volun/ws/test/padron
    # ENTRADA => {}
    # SALIDA => {response}
    def padron
        citizen = Citizen.all.first
        padron = Padron.new(citizen).search_citizien
        success(padron)
    rescue  => e
        begin
            Rails.logger.error("ERROR-WS: padron -> #{e}")
        rescue
        end
        error("service")
    end
end