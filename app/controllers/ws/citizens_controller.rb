class Ws::CitizensController < Ws::GenericApiController
    require 'digest'
    
    before_action :skip_trackable, except: [:citizen,:send_sms, :send_credentials, :login, :new_password]
    before_action :checktokenUser, except: [:citizen,:send_sms, :send_credentials, :login, :new_password]
    before_action :authenticate!, unless: ->{user_authenticated?}, except: [:citizen,:send_sms, :send_credentials, :login, :new_password]

    # POST /volun/ws/citizen
    # ENTRADA => {movil,email,documento,tipo_documento,provincia,fecha_nacimiento}
    # SALIDA => {id,nombre,apellido1,apellido2,email,nif,tipo_nif,movil, fecha_nacimiento,pais,nivel_academico,sexo,persona_en_casa,barrio,cod_barrio,distrito,cod_distrito}
    def citizen
        if strong_citizen_params
            permit_params = params.permit(:movil, :email,:documento,:tipo_documento,:provincia,:fecha_nacimiento) 
            invalid_param = valid_params(permit_params.except(:movil, :provincia, :fecha_nacimiento))

            begin
                Rails.logger.error("INFO-WS: POST /volun/ws/citizen -> #{permit_params}")
            rescue
            end
            if Setting.find_by(key: "BlokedMTA").try(:value).to_s == "true"
                error("error66")
            elsif invalid_param != true
                invalid_param == "documento" ? error("error14", nil, :bad_request) : error("error13", invalid_param, :bad_request)            
            else  
                begin
                    is_numeric = !!Integer(permit_params[:tipo_documento])
                rescue
                    is_numeric = false
                end
                if !is_numeric || ![1,2,3,4,5,6].include?(permit_params[:tipo_documento].to_i)
                    error("error38")
                elsif !permit_params[:movil].blank? && permit_params[:movil].to_s.match(/[6|7]\d{8}/).blank?
                    error("error44")
                elsif !permit_params[:fecha_nacimiento].blank? && permit_params[:fecha_nacimiento].to_s.match(/\d{2}\/\d{2}\/\d{4}|\d{4}-\d{2}-\d{2}/).to_s.strip.blank?
                    error("error68")
                else
                    if !Citizen.find_by(email: permit_params[:email].to_s.strip, email_verified: true, active: true).blank?
                        error("error15", nil, :bad_request)
                    elsif !Citizen.find_by(document: permit_params[:documento], email_verified: true, active: true).blank?
                        error("error16", nil, :bad_request)
                    elsif !Citizen.find_by(email: permit_params[:email].to_s.strip, active: false, unsubscribed: false).blank?
                        error("error59", nil, :bad_request)
                    elsif !Citizen.find_by(document: permit_params[:documento], active: false, unsubscribed: false).blank?
                        error("error59", nil, :bad_request)
                    else
                        citizen = Citizen.find_by(document: permit_params[:documento], email_verified: false)
                        
                        if !citizen.blank?
                            old_email = citizen.email
                            citizen.email = permit_params[:email].to_s.strip.downcase
                            citizen.phone = permit_params[:movil]
                            citizen.legal_msg = true
                            citizen.document = permit_params[:documento]
                            citizen.document_type = find_document_type(permit_params[:tipo_documento])
                            citizen.active = true if citizen.unsubscribed.to_s == "true"
                            citizen.unsubscribed = false if citizen.unsubscribed.to_s == "true"
                            citizen.province = !permit_params[:provincia].blank? ? Province.find_by(code: permit_params[:provincia].to_i) : nil
                            citizen.birthday = !permit_params[:fecha_nacimiento].blank? ? Date.parse(permit_params[:fecha_nacimiento]) : nil
                        else
                            citizen = Citizen.find_by(email: permit_params[:email].to_s.strip.downcase, email_verified: false)
                            if !citizen.blank?
                                old_email = citizen.email
                                citizen.legal_msg = true
                                citizen.email = permit_params[:email].to_s.strip.downcase
                                citizen.phone = permit_params[:movil]
                                citizen.document = permit_params[:documento]
                                citizen.document_type = find_document_type(permit_params[:tipo_documento])
                                citizen.province = !permit_params[:provincia].blank? ? Province.find_by(code: permit_params[:provincia].to_i) : nil 
                                citizen.birthday = !permit_params[:fecha_nacimiento].blank? ? Date.parse(permit_params[:fecha_nacimiento]) : nil                         
                            else
                                citizen = Citizen.new(
                                    phone: permit_params[:movil],
                                    email: permit_params[:email].to_s.strip.downcase,
                                    document: permit_params[:documento],
                                    legal_msg: true,
                                    padron: false,
                                    document_type: find_document_type(permit_params[:tipo_documento]),
                                    province: !permit_params[:provincia].blank? ? Province.find_by(code: permit_params[:provincia].to_i) : nil,
                                    birthday: !permit_params[:fecha_nacimiento].blank? ? Date.parse(permit_params[:fecha_nacimiento]) : nil
                                    )
                            end
                        end
                        citizen.enabled_app = "Mayor con App móvil"

                        addr_aux = citizen.padron ? nil : get_padron(citizen)
                        if !addr_aux.blank? || citizen.padron
                            citizen.address = addr_aux
                            user = User.find_by(email: citizen.email)

                            if user.blank? || user.try(:loggable_type).to_s == "Citizen"
                                ActiveRecord::Base.transaction do
                                    if citizen.save
                                        birthday = citizen.try(:birthday).try(:to_datetime)
                                        citizen_name = citizen.name.split(" ")
                                        password = "#{citizen_name[0]}" + "#{birthday.blank? ? Time.zone.now.year : birthday.to_date.year}" + "#{SecureRandom.hex(1)}"
                                        while password.size < 8
                                            password = password + "#{SecureRandom.hex(1)}"
                                        end
                                        
                                        if !old_email.blank?
                                            user = citizen.user
                                            if user.blank?
                                                user = User.new(
                                                    email: citizen.email,
                                                    password: password,
                                                    login: citizen.email.split("@")[0],
                                                    loggable: citizen,
                                                    confirmed_at: Time.zone.now
                                                )
                                            else
                                                user.email = citizen.email
                                                user.login = citizen.email.split("@")[0]
                                            end
                                        else
                                            user = User.new(
                                                email: citizen.email,
                                                password: password,
                                                login: citizen.email.split("@")[0],
                                                loggable: citizen,
                                                confirmed_at: Time.zone.now
                                            )
                                        end
                                        #Send email confirmation
                                        citizen.confirm_token = SecureRandom.urlsafe_base64.to_s
                                        #VOLUN-511 descomentar
                                        # citizen.validate_code_sms = SecureRandom.hex(5)
                                        
                                        citizen.user = user
                                        if citizen.save && user.save
                                            sign_out
                                            sign_in(citizen.user)
                                            AuditGenerateHelper.input_parameters("create_citizen", citizen, current_user)
                                            CitizenTracking.create(:citizen=> citizen,
                                                :tracking_type => TrackingType.find_by(alias_name: "subscribe"),
                                                :manager => nil,
                                                :tracked_at => Time.zone.now,
                                                :coments => "Se ha generado sin validar por aplicación móvil")
                                            # begin
                                            #     VolunteerMailer.send_confirmation_email(citizen.email, subject: I18n.t('mailer.email_validation.subject'), message: I18n.t('mailer.email_validation.body',full_name: citizen.full_name, url: "https://voluntariospormadrid.madrid.es/volun/confirmation_email/#{citizen.confirm_token}")).deliver_now
                                            # rescue => e
                                            #     begin
                                            #         Rails.logger.error("ERROR-WS-EMAIL: citizen -> #{e}")
                                            #     rescue
                                            #     end
                                            # end
                                            # begin
                                            #     SMSApi.new.sms_deliver(permit_params[:movil], "Código de validación del usuario: #{citizen.validate_code_sms}")
                                            # rescue =>error
                                            #     begin
                                            #         Rails.logger.error("ERROR-WS: send_sms(SMS) -> #{error}")
                                            #     rescue
                                            #     end
                                            # end

                                            success(json_citizen(citizen))
                                        else
                                            citizen.destroy                                                                           
                                            error("error09", user.errors.full_messages)
                                        end                                    
                                    else
                                        puts citizen.errors.full_messages
                                        error("error02")
                                    end 
                                end
                            else    
                                error("error69")
                            end
                        else
                            begin
                                Rails.logger.error("ERROR-WS: Fallo en padron")
                            rescue
                            end
                            !@code.blank? ? error(@code) : error("error60")
                        end
                    end
                end
            end        
        end
    rescue => e
        begin
            Rails.logger.error("ERROR-WS: citizen -> #{e}")
        rescue
        end
       error("service")
    end 


    # POST /volun/ws/sendsms/
    # ENTRADA => {movil,codigo}
    # SALIDA => {respuesta}
    def send_sms
        if strong_send_sms_params
            permit_params = params.permit(:movil,:codigo) 
            invalid_param = valid_params(permit_params)
           # codigo = SecureRandom.hex(5)
           
            if invalid_param != true
                error("error13", invalid_param, :bad_request)
            elsif !permit_params[:movil].blank? && permit_params[:movil].to_s.match(/[6|7]\d{8}/).blank?
                error("error44")
            elsif request.headers[:HTTP_WS_USER].blank? || request.headers[:HTTP_WS_PASSWORD].blank?
                error("error74")
            elsif !(Digest::SHA256.hexdigest(request.headers[:HTTP_WS_USER]) == Digest::SHA256.hexdigest(Rails.application.secrets.ws_sms_user) && Digest::SHA256.hexdigest(request.headers[:HTTP_WS_PASSWORD])== Digest::SHA256.hexdigest(Rails.application.secrets.ws_sms_pass)) 
                error("error73")
            else
                begin
                    SMSApi.new.sms_deliver(permit_params[:movil], "Código de validación del usuario: #{permit_params[:codigo]}")

                    success({respuesta: "OK"})
                rescue =>error
                    begin
                        Rails.logger.error("ERROR-WS: send_sms(SMS) -> #{error}")
                    rescue
                    end
                    error("error75")
                end
            end
        end
    rescue => e
        begin
            Rails.logger.error("ERROR-WS: send_sms -> #{e}")
        rescue
        end
       error("service")
    end


    # PUT /volun/ws/citizen/
    # ENTRADA => {movil,imagen}
    # SALIDA => {id,cid360,nombre,apellido1,apellido2,email, nif,tipo_nif,movil,fecha_nacimiento, pais, nivel_academico, sexo, personas_en_casa, distrito, cod_distrito, barrio, cod_barrio, imagen}
    def update_citizen
        if strong_update_citizen_params
            permit_params = params.permit(:movil,:imagen) 
            invalid_param = valid_params(permit_params)
            if invalid_param != true
                error("error13", invalid_param, :bad_request)
            else
                if current_user.loggable.class.name.to_s != "Citizen" 
                    error("token")
                else
                    citizen = current_user.loggable
                    citizen.phone = permit_params[:movil]
                    begin
                        if !permit_params[:imagen].blank?
                            StringIO.open(Base64.decode64(permit_params[:imagen])) do |data|
                                begin
                                    data.class.class_eval { attr_accessor :original_filename, :content_type }
                                    data.original_filename = "logo.jpg"
                                    data.content_type = "image/jpeg"
                                    citizen.logo.file = data
                                rescue
                                    data.class.class_eval { attr_accessor :original_filename, :content_type }
                                    data.original_filename = "logo.png"
                                    data.content_type = "image/png"
                                    citizen.logo.file = data
                                end
                            end
                        end
                    rescue
                    end
                            
                    if citizen.save
                        AuditGenerateHelper.input_parameters("update_citizen", citizen, current_user)
                        success(json_citizen(citizen).merge!({cid360: citizen.id.to_i}))
                    else
                        error("error47")
                    end
                end
            end
        end       
    rescue => e
        begin
            Rails.logger.error("ERROR-WS: update_citizen -> #{e}")
        rescue
        end
        error("service") 
    end

    # POST /volun/ws/citizen/login
    # ENTRADA => {nombre_usuario,password}
    # SALIDA => {id, cid360, nombre, apellido1,apellido2,email,nif,tipo_nif,movil,fecha_nacimiento,pais,nivel_academico,sexo,persona_en_casa,barrio, cod_barrio,distrito, cod_distrito,token}
    def login
        if strong_login_params
            permit_params = params.permit(:nombre_usuario,:password) 
            sign_out
            citizen = Citizen.find_by(email: permit_params[:nombre_usuario].to_s.strip, active: true)
            if !citizen.blank? && citizen.email_verified == true
                
                if !citizen.try(:user).blank? && citizen.user.valid_password?(permit_params[:password].to_s.strip)          
                    user = citizen.user
                    user.password = permit_params[:password].to_s.strip
                    user.password_confirmation = permit_params[:password].to_s.strip
                    user.save

                    sign_in(citizen.user)
                    AuditGenerateHelper.input_parameters("login", citizen, citizen.user)
                    success(json_citizen(citizen).merge!({cid360: citizen.id.to_i, token: citizen.user.authentication_token}))
                else
                    error("error28")
                end
            elsif citizen.blank?
                error("error28")
            else
                error("error52")
            end
        end
    rescue => e
        begin
            Rails.logger.error("ERROR-WS: citizen_login -> #{e}")
        rescue
        end
        error("service") 
    end

    # POST /volun/ws/citizen/new_password
    # ENTRADA => {nombre_usuario}
    # SALIDA => {password}
    def new_password
        if strong_new_password_params
            permit_params = params.permit(:nombre_usuario)
            sign_out
            citizen = Citizen.find_by(email: permit_params[:nombre_usuario])
            if !citizen.blank? && citizen.email_verified == true
                if !citizen.try(:user).blank?
                    birthday = citizen.try(:birthday).try(:to_datetime)          
                    pass = "#{citizen.name.first.upcase}" + "#{citizen.last_name_1.first.upcase}" + "#{birthday.blank? ? Time.zone.now.year : birthday.to_date.year}" + "#{SecureRandom.hex(1)}"
                    user = citizen.user

                    user.password = pass
                    user.password_confirmation = pass
                    user.save
                    AuditGenerateHelper.input_parameters("new_password", citizen, user)
                    send_mail_password(citizen, user.password) 
                else
                    error("error28")
                end
            elsif citizen.blank?
                error("error28")
            else
                error("error52")
            end
        end
    rescue => e
        begin
            Rails.logger.error("ERROR-WS: new_password -> #{e}")
        rescue
        end
        error("service")
    end

    # POST /volun/ws/citizen/change_password
    # ENTRADA => {password}
    # SALIDA => {}
    def change_password
        if strong_login_params
            permit_params = params.permit(:password)
            citizen = Citizen.find(current_user.loggable_id)
            if !citizen.blank? && current_user.loggable_type.to_s == "Citizen"               
                current_user.password = permit_params[:password].to_s.strip
                current_user.password_confirmation = permit_params[:password].to_s.strip
                current_user.save
                AuditGenerateHelper.input_parameters("change_password", citizen, current_user)
                send_mail_password(citizen, permit_params[:password].to_s.strip)                
            else
                error("error28")
            end
        end
    rescue => e
        begin
            Rails.logger.error("ERROR-WS: change_password -> #{e}")
        rescue
        end
        error("service")  
    end

    # POST /volun/ws/send_credentials
    # ENTRADA => {id,email,codigo}
    # SALIDA => {}
    def send_credentials
        if strong_send_credentials_params
            permit_params = params.permit(:id,:email) 
            invalid_param = valid_params(permit_params)
            if invalid_param != true
                error("error13", invalid_param, :bad_request)
            else
                citizen = Citizen.find_by(id: permit_params[:id], email: permit_params[:email])
                if citizen.try(:user).blank?
                    error("error26")
                elsif citizen.email_verified == true
                    error("error51")
                # elsif citizen.validate_code_sms != permit_params[:codigo]
                #     citizen.validate_sms_intents = citizen.validate_sms_intents+1
                #     if citizen.validate_sms_intents > 3
                #         citizen.validate_code_sms = SecureRandom.hex(5)
                #         citizen.validate_sms_intents = 0
                #         begin
                #             SMSApi.new.sms_deliver(permit_params[:movil], "Código de validación del usuario: #{citizen.validate_code_sms}")
                #         rescue =>error
                #             begin
                #                 Rails.logger.error("ERROR-WS: send_sms(SMS) -> #{error}")
                #             rescue
                #             end
                #         end
                #         citizen.save(validate: false)
                #         error("error77")
                #     else
                #         citizen.save(validate: false)
                #         error("error76")
                #     end                     
                else                    
                    user = citizen.user
                    birthday = citizen.try(:birthday).try(:to_datetime)
                    citizen_name = citizen.name.split(" ")                     
                    password = "#{citizen.name.first.upcase}" + "#{citizen.last_name_1.first.upcase}" + "#{birthday.blank? ? Time.zone.now.year : birthday.to_date.year}" + "#{SecureRandom.hex(1)}"
                    user.password = password
                    user.password_confirmation = password
                    user.save
                    
                    #VOLUN-511 descomentar siguiente línea
                    # citizen.validate_sms_at = Time.zone.now
                    citizen.email_verified = true
                    citizen.validated_at = Time.zone.now
                    citizen.save(validate: false)
                    CitizenTracking.create(:citizen=> citizen,
                        :tracking_type => TrackingType.find_by(alias_name: "subscribe"),
                        :manager => nil,
                        :tracked_at => Time.zone.now,
                        :coments => "Se ha validado el alta en las aplicaciones móviles")
                    AuditGenerateHelper.input_parameters("send_credentials", citizen, user)
                    
                    send_mail(citizen, password)
                end
            end
        end
    rescue => e
        begin
            Rails.logger.error("ERROR-WS: send_credentials -> #{e}")
        rescue
        end
        error("service")
    end

    # POST /volun/ws/citizen/unsubscribe
    # ENTRADA => {}
    # SALIDA => {mensaje}
    def unsubscribe
        citizen = Citizen.find_by(id: current_user.loggable_id, unsubscribed: false)
        if !citizen.blank? && current_user.loggable_type.to_s == "Citizen"
            #address_id = citizen.address_id
            #citizen.address_id = nil 
            citizen.phone = nil            
            #citizen.birthday = nil
            #citizen.country = nil
            #citizen.country_code = nil
            citizen.academic_info = nil
            #citizen.gender = nil
            citizen.info_source_id = nil
            citizen.people_at_home = nil
            citizen.start_date = nil
            citizen.last_sign_in = nil
            citizen.code_ci360 = nil           
            citizen.terms_of_service = nil
            citizen.access_token = nil
            citizen.authentication_token = nil
            citizen.authentication_token_created_at = nil
            citizen.code_cid360_autentication = nil
            citizen.validation_cid360 = nil
            citizen.active = false
            citizen.email_verified = false
            citizen.unsubscribed = true
            citizen.unsubscribed_at = Time.zone.now
            citizen.save(validate: false)
            #Address.find_by(id: address_id).destroy
            CitizenService.all.where(citizen_id: citizen.id).each do |service|
                if ["active", "accept"].include?(service.status.to_s)
                    service.status = "canceled"
                    service.status_service = StatusService.find_by(code:  service.status)
                end
                service.save(validate: false)

                CitizenServiceTracking.create(:citizen_service=> service,
                    :tracking_type => TrackingType.find_by(alias_name: "unsubscribe"),
                    :manager => nil,
                    :tracked_at => Time.zone.now,
                    :coments => "Se ha dado de baja en la aplicación móvil")
            end

            CitizenTracking.create(:citizen=> citizen,
                :tracking_type => TrackingType.find_by(alias_name: "unsubscribe"),
                :manager => nil,
                :tracked_at => Time.zone.now,
                :coments => "Se ha dado de baja en la aplicación móvil")

            success({mensaje: "Usuario dado de baja satisfactoriamente."})
        else
            error("error28")
        end
    rescue => e
        begin
            Rails.logger.error("ERROR-WS: unsubscribe -> #{e}")
        rescue
        end
        error("service")
    end

  
    private
        def strong_send_sms_params
            strong_params_validate([:movil,:codigo]).to_s == "true"
        end

        def strong_update_citizen_params
            strong_params_validate([:movil]).to_s == "true"
        end

        def strong_login_params
            strong_params_validate([:password]).to_s == "true"
        end

        def strong_new_password_params
            strong_params_validate([:nombre_usuario]).to_s == "true"
        end

        def strong_send_credentials_params
            strong_params_validate([:id,:email]).to_s == "true"
        end

        def strong_citizen_params
            strong_params_validate([:movil,:email,:documento,:tipo_documento]).to_s == "true"
        end

        def valid_params(params)
            regexp = /^[0-9]{8,8}[A-Za-z]$/
            params.each do |key,value|
                unless key == "controller"
                    return key if key == "nif" && value.match(/^[0-9]{8,8}[A-Za-z]$/).blank?
                    return key if value.blank?                
                end
            end
            true
        end

        def send_mail_password(citizen, pass)
            begin
              VolunteerMailer.send_email_ws(citizen.email, {message: I18n.t('mailer_ws.new_pass', id: citizen.email, pass: pass).html_safe, subject: "Nueva Contraseña Madrid te Acompaña"}).deliver_now
              success({mensaje: "Se ha enviado la nueva contraseña al mayor correctamente."})
            rescue Exception  => e             
              begin
                Rails.logger.error("ERROR-WS: #{I18n.t('mailer.error')}: \n#{e}")
            rescue
            end
              error("error54")
            end
        end

        def send_mail(citizen, pass)
            begin
              VolunteerMailer.send_email_ws(citizen.email, message: I18n.t('mailer_ws.success', id: citizen.email, pass: pass).html_safe, subject: "Credenciales Madrid te Acompaña").deliver_now
              success({mensaje: "Se han enviado las credenciales al mayor correctamente."})
            rescue Exception  => e
                begin
                    Rails.logger.error("ERROR-WS: #{I18n.t('mailer.error')}: \n#{e}")
                rescue
                end
              
              error("error29")
            end
        end

        def send_mail_hard(citizen, pass)
            begin
              VolunteerMailer.send_email_ws(citizen.try(:email), message: I18n.t('mailer_ws.success_hard', id: citizen.try(:email), pass: pass).html_safe, subject: "Credenciales Madrid te Acompaña").deliver_now
              success({mensaje: "Se han enviado las credenciales al mayor correctamente."})
            rescue Exception  => e
                begin
                    Rails.logger.error("ERROR-WS: #{I18n.t('mailer.error')}: \n#{e}")
                rescue
                end
              error("error29")
            end
        end
  
        def get_padron(citizen)
            padron = Padron.new(citizen).search_citizien
            @code = nil
            puts "="*50
            puts padron

            if !citizen.email.to_s.match(/^mayor[0-9]*@yopmail.com$/).blank?
                citizen.name = citizen.email.to_s.gsub('@yopmail.com','')
                citizen.last_name_1 = citizen.email.to_s.gsub('@yopmail.com','')
                citizen.last_name_2 = citizen.email.to_s.gsub('@yopmail.com','')
                citizen.country = "ESPAÑA"
                citizen.country_code = 108
                aux_birthday = Date.parse("1976-05-05")
                citizen.birthday = aux_birthday
                citizen.academic_info = ""
                #citizen.gender = ""
                citizen.people_at_home = 1
                citizen.start_date = Date.parse("1976-05-05")
    
                address = Address.new()
                
                address.normalize = false
                address.road_type = "Calle"
                address.road_name = "Principal"
                address.road_number = 1
                address.road_number_type = "N"
                address.country = "ESPAÑA"
                address.country_code = 108
                address.town = "Madrid" 
                address.province =  Province.find_by(code: 28)
                address.floor = ""
                address.door = ""
                address.borought = Borought.find_by("cast(code as int)=? and district_id=?",1,District.find_by("cast(code as int)=?",1))            
                address.district = District.find_by("cast(code as int)=?",1)
                address.postal_code = 28000

               
                address                
            elsif !padron.blank? && !padron[:datos_habitante].blank? && !padron[:datos_habitante][:item].blank?
                now = Time.zone.now.to_date
                date_bithday = Time.zone.parse(padron[:datos_habitante][:item][:fecha_nacimiento_string].to_s)
               
                if now.year - date_bithday.year - ((now.month > date_bithday.month || (now.month == date_bithday.month && now.day >= date_bithday.day)) ? 0 : 1) >= 65
                    not_birthday = false
                    citizen.name = padron[:datos_habitante][:item][:nombre]
                    citizen.last_name_1 = padron[:datos_habitante][:item][:apellido1]
                    citizen.last_name_2 = padron[:datos_habitante][:item][:apellido2]
                    citizen.country = padron[:datos_habitante][:item][:desc_pais_nacimiento]
                    citizen.country_code = padron[:datos_habitante][:item][:codigo_pais_nacimiento]
                    aux_birthday = padron[:datos_habitante][:item][:fecha_nacimiento_string].blank? ? '' : Date.parse(padron[:datos_habitante][:item][:fecha_nacimiento_string])
                    if citizen.birthday.blank?
                        citizen.birthday = aux_birthday
                        not_birthday= true
                    end
                    citizen.academic_info = padron[:datos_habitante][:item][:descripcion_nivel_instruccion]
                    citizen.gender = Gender.where("upper(name) = ?", ['VARÓN','HOMBRE'].include?(padron[:datos_habitante][:item][:descripcion_sexo].upcase) ? "VARÓN" : "MUJER").first
                    citizen.people_at_home = padron[:datos_vivienda][:item][:num_personas]
                    citizen.start_date = padron[:datos_vivienda][:item][:fecha_alta_inscripcion_string]
                    citizen.phone = padron[:datos_vivienda][:item][:telefono].blank? ? citizen.phone : padron[:datos_vivienda][:item][:telefono]
                    aux_province = padron[:datos_habitante][:item][:codigo_provincia_nacimiento]
                    aux_codigo_nac= padron[:datos_habitante][:item][:codigo_pais_nacimiento]

                    address = Address.new()
                    
                    address.normalize = false
                    address.road_type = padron[:datos_vivienda][:item][:sigla_via]
                    address.road_name = padron[:datos_vivienda][:item][:nombre_via]
                    address.road_number = padron[:datos_vivienda][:item][:numero_via]
                    address.road_number_type = padron[:datos_vivienda][:item][:nominal_via]
                    address.country = padron[:datos_habitante][:item][:desc_pais_nacimiento]
                    address.country_code = padron[:datos_habitante][:item][:codigo_pais_nacimiento]
                    address.town = padron[:datos_habitante][:item][:desc_municipio_nacimiento].blank? ? "Madrid" : padron[:datos_habitante][:item][:desc_municipio_nacimiento]
                    address.town_code = padron[:datos_habitante][:item][:codigo_municipio_nacimiento].blank? ? "" : padron[:datos_habitante][:item][:codigo_municipio_nacimiento]
                    address.province = padron[:datos_habitante][:item][:codigo_provincia_nacimiento].blank? ? Province.find_by(code: 28) : Province.find_by(code: padron[:datos_habitante][:item][:codigo_provincia_nacimiento].to_i)
                    address.province = Province.find_by(code: 999) if address.province.blank?
                    address.floor = padron[:datos_vivienda][:item][:planta]
                    address.door = padron[:datos_vivienda][:item][:puerta]
                    address.borought = Borought.find_by("cast(code as int)=? and district_id=?",padron[:datos_vivienda][:item][:codigo_barrio].to_i,District.find_by("cast(code as int)=?",padron[:datos_vivienda][:item][:codigo_distrito].to_i))            
                    address.district = District.find_by("cast(code as int)=?",padron[:datos_vivienda][:item][:codigo_distrito].to_i)
                    address.postal_code = padron[:datos_vivienda][:item][:codigo_postal]

                    

                    if (citizen.province.blank?) && not_birthday
                        address
                    elsif (citizen.province.try(:code).blank? || citizen.province.try(:code).to_i == 999) && aux_codigo_nac.to_i !=108 && citizen.birthday.to_date.strftime("%d/%m/%Y") == aux_birthday.strftime("%d/%m/%Y") && !not_birthday
                        address
                    elsif aux_codigo_nac.to_i ==108 && aux_province.to_i == citizen.province.try(:code).to_i && citizen.birthday.to_date.strftime("%d/%m/%Y") == aux_birthday.strftime("%d/%m/%Y") && !not_birthday
                        address
                    else
                        @code="error67"
                        nil
                    end
                else
                    @code = "error08"
                    nil
                end
            elsif padron.blank?
                @code = "error60"
                nil
            else
                @code = "error11"
                nil
            end
        rescue => e
            log_error("COD-00001",e) 
            nil
        end

        def find_document_type(document_type)
            if document_type.blank?
                document_type
            else
                case document_type.to_i
                when 1
                    DocumentType.find_by(name: "DNI")
                when 2
                    DocumentType.find_by(name: "PASAPORTE")
                when 3
                    DocumentType.find_by(name: "TRT")
                when 4
                    DocumentType.find_by(name: "TRP")
                when 5
                    DocumentType.find_by(name: "TRC")
                when 6
                    DocumentType.find_by(name: "NIE")
                end
            end
        end
           
end