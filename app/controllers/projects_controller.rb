class ProjectsController < ApplicationController
  before_action :authenticate_user!, only: :my_area
  before_action :set_project, only: :show
  before_action :set_areas, only: :index
  respond_to :html, :js, :json

  def index
    Ahoy::Event.create(name: "Visit", properties: {title: "Proyectos"}.to_json, user_id: current_user.try(:id), visit_id: current_visit.try(:id), time:Time.now)
    search_reload(params)
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def show
    params[:day] ||= @project.timetables.minimum(:execution_date).try :strftime, '%Y-%m-%d'
    @timetables = @project.timetables.where(timetables: { execution_date: params[:day] })
    @locations = Event.includes(:project, :address).where(projects: { id: params[:id] }).as_json(only: [:id], include: { address: { only: [:latitude, :longitude] }, project: { only: [:id, :pt_extendable_type, :name] } })
    volunteers_last_month= @project.volunteers.where("projects_volunteers.unsubscribe_date IS NULL").group("volunteers.gender_id").group_by_month("projects_volunteers.subscribe_date", range: 8.months.ago.midnight..Time.now).count
    @aux_last = [{name: I18n.t('gender.0'), data: []},{name: I18n.t('gender.1'), data: []},{name: I18n.t('gender.2'), data: []},{name: I18n.t('gender.blank'), data: []}]
    volunteers_last_month.each do |vol|
      if vol[0][0].to_i==0
        @aux_last[0][:data].push([vol[0][1],vol[1]])
      elsif vol[0][0].to_i==1
        @aux_last[1][:data].push([vol[0][1],vol[1]])
      elsif vol[0][0].to_i==2
        @aux_last[1][:data].push([vol[0][1],vol[1]])
      elsif vol[0][0].to_i==NULL
        @aux_last[1][:data].push([vol[0][1],vol[1]])
      end
    end
    @volunteers_gender= @project.volunteers.group("volunteers.gender_id").count
    
    respond_to do |format|
      format.html
      format.js
    end
  rescue => e
    begin
      Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  private

    def my_area
      params[:q] ||= Project.ransack_default
      @search = Project.includes(:areas).search(params[:q])
      @projects_actives = @search.result.page(params[:page])
      respond_to do |format|
        format.html
        format.js
      end
    end

    def boroughs
      @boroughs = Project.includes(:addresses).no_urgent.distinct.where('addresses.district_id=?', params[:district]).order('addresses.borough').pluck('addresses.borough', 'addresses.borough')
      respond_to do |format|
        format.json { render json: @boroughs }
      end
    end

    def search_reload(params)
      params[:q] ||= Project.ransack_default
      params[:order] ||="projects.order"
      params[:per_page] ||=12

      unless params[:q][:areas_name_in].blank?
        areas=params[:q][:areas_name_in]
        aux_areas= params[:q][:areas_name_in]
        areas= areas.to_s.gsub('[','').gsub(']','').gsub('\'','')
        areas= areas.split(',')
        params[:q][:areas_name_in]=areas
      end
      unless params[:q][:timetables_execution_date_eq].blank?
        params[:q][:timetables_execution_date_eq] = Date.parse(params[:q][:timetables_execution_date_eq]).strftime("%Y-%m-%d").to_s
      end
  
      @projects_actives_all=Project.includes(:areas).where(active: true,suspended: false).distinct      
      @search = @projects_actives_all.search(params[:q])
      projects_unpagined = @search.result.distinct.order(params[:order]).order(:name)
  
      if !params[:extend_search].blank? && params[:extend_search]=='1' && no_search_params && params[:page].blank?
        projects_unpagined = projects_unpagined.where(id: nil)
      end 
      @boroughs=[]
      @districts=[]
      params[:q][:areas_name_in]=aux_areas
      listado = ActiveRecord::Base.connection.exec_query(
        "SELECT distinct (select b.name from boroughts b where b.id=addresses.borought_id) as borough, (select d.name from districts d where d.id=addresses.district_id) as district FROM addresses, events, projects
        WHERE addresses.id=events.address_id AND projects.id=events.eventable_id AND
        events.eventable_type='Project' AND projects.active = true"
      )
        listado.each do |add|
          @boroughs.push(add["borough"]) if !add["borough"].blank?
          @districts.push(add["district"]) if !add["district"].blank?
        end
      
      @projects_actives =projects_unpagined.paginate(page: params[:page], per_page: params[:per_page])
      @locations = @projects_actives.map(&:events).as_json(only: [:id], include: { address: { only: [:latitude, :longitude] }, project: { only: [:id, :pt_extendable_type, :name] } }).flatten
    end

    def no_search_params
      params[:q].blank? || params[:q][:name_or_description_cont].blank? && params[:q][:timetables_execution_date_eq].blank? && params[:q][:addresses_district_eq].blank? && params[:q][:addresses_borough_eq].blank? && params[:q][:areas_name_in].blank?
    end

    def set_areas
      @areas = Area.where(active: true).order(:name) 
    end

    def set_project
      @project = Project.includes(:addresses).find(params[:id])
    rescue
      redirect_to projects_path, alert: "No existe el proyecto" 
    end
end
