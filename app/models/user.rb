class User < ActiveRecord::Base
  devise :database_authenticatable,
    :recoverable, :rememberable, :trackable, :validatable, :registerable

  belongs_to :loggable, polymorphic: true, foreign_key: "loggable_id", optional: true
  belongs_to :notice_type, class_name: "NoticeType", foreign_key: "notice_type_id", optional: true
  belongs_to :voluntees, class_name: "Volunteer", foreign_key: "loggable_id", optional: true
  belongs_to :project_volunteer, class_name: "ProjectsVolunteer", optional: true
  belongs_to :meetings, class_name: "Meeting", optional: true
  belongs_to :volun_proy_meeting, class_name: "VolunProyMeeting", optional: true
  
  has_many :audits, class_name: "Audit"

  attr_accessor :username

  scope :count_email, (->(email) { where('email = ?', email).count })

  validates :terms_of_service, presence: true, on: :create
  validates :password,   presence: true
    # format: {:with => /\A(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.{8})/x, message: "Debe tener 8 catacteres e incluir un número, una letra mayúscula y una minúscula"}
    # on: :create
  validates :password, length: { minimum: 8, maximun: 8 },
    confirmation: true,
    allow_nil: true,
    format: {:with => /\A(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.{8})/x, message: "Debe tener 8 catacteres e incluir un número, una letra mayúscula y una minúscula"},
    on: :update

  def self.find_for_database_authentication warden_conditions
    conditions = warden_conditions.dup
    login = conditions.delete(:username)
    return nil if login.blank? 
    pwd = conditions.delete(:password)
    user = self.where(conditions).where(["lower(email) = :value", {value: login.strip.downcase}]).first
    return user if user.blank?
    
    
    user = user.valid_password?(pwd) ? user : nil
    user
  end
end
