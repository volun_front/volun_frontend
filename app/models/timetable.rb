class Timetable < ActiveRecord::Base
  belongs_to :event, class_name: "Event", foreign_key: "event_id", optional: true
  has_one :activity, as: :eventable, through: :event, class_name: 'Activity',  foreign_key: "eventable_id"
  has_one :project, as: :eventable,  through: :event, class_name: 'Project',  foreign_key: "eventable_id"
  has_one :address, through: :event, class_name: 'Address',  foreign_key: "address_id"
end
