class Setting < ActiveRecord::Base
    DEFAULT_PREFIX = "#{Rails.application.class.module_parent_name}."
  
    has_many :links, class_name: "Link"
  
    ######## VALIDACIONES ########
    validates :key, presence: true, uniqueness: true
    
    ######## SCOPES ########
    default_scope { order(id: :asc) }

    class << self
        def [](key)
          where(key: "#{DEFAULT_PREFIX}#{key}").pluck(:value).first.presence
        end
    
        def []=(key, value)
          key = "#{DEFAULT_PREFIX}#{key}"
          setting = where(key: key).first || new(key: key)
          setting.value = value.presence
          setting.save!
          value
        end
    
        def road_types
          self['road_types'].split(',') rescue []
        end
    end
    
end
  