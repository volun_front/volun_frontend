class ShiftDefinition < ActiveRecord::Base

    include Recordable
    include Archivable

    has_and_belongs_to_many :projects, class_name: "Project"
    has_and_belongs_to_many :volunteers, class_name: "Volunteer"
    has_and_belongs_to_many :projects_volunteers, class_name: "ProjectsVolunteer"

    validates :name, uniqueness: true
    validates :name, presence: true

    scope :id,   ->(){ where("id is not null") }

    def self.main_columns
        [:id, :name, :description, :active]
    end

    def to_s
        name
    end

end
