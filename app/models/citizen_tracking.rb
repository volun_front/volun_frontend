class CitizenTracking < ApplicationRecord

    belongs_to :citizen ,class_name: "Citizen", foreign_key: "citizen_id", optional: true
    belongs_to :tracking_type, ->{order(:name)}, class_name: "TrackingType", foreign_key: "tracking_type_id", optional: true
    belongs_to :manager ,class_name: "Manager", foreign_key: "manager_id", optional: true  
    
end
  