class District < ActiveRecord::Base
  has_and_belongs_to_many :projects,  class_name: "Project"

  def to_s
    self.name
  end
end
