class Citizen < ActiveRecord::Base  

    belongs_to :address, class_name: "Address", foreign_key: "address_id", optional: true
    belongs_to :gender, optional: true
    belongs_to :province, optional: true
    belongs_to :nationality, optional: true
    belongs_to :document_type, optional: true
    
    has_many :citizen_services, class_name: "CitizenService", foreign_key: "citizen_id"
    has_many :trackings, class_name: "CitizenTracking", foreign_key: "citizen_id"
    has_one  :user, class_name: "User", foreign_key: 'loggable_id', as: :loggable 

    has_one  :logo,   -> { citizen_logo   }, class_name: 'Link', foreign_key: 'linkable_id', required: false, validate: false
    has_many :links, as: :linkable

    scope :not_market, ->() { where(market: false)}
    scope :not_tester, ->() { not_market.where(tester: false) }
    scope :tester, ->() { not_market.where(tester: true) }
    scope :all_active,   ->(){ not_market.where(active: true) }
    scope :for_ids_with_order, ->(ids) {
        # order = sanitize_sql_array(
        # # Esto no es compatible en rails 6
        #   ["position(id::text in ?)", ids.join(',')]
        # )
        # not_market.where(:id => ids).order(order)
        
        # SOLUCIÓN 1 
        not_market.where(:id => ids).order(id: :asc)
    
      }
    
    validates :name, presence: true

    def document_type_ws
        case document_type.name.try(:upcase)
        when "DNI"
            1
        when "PASAPORTE"
            2
        when "TRT"
            3
        when "TRP"
            4
        when "TRC"
            5
        when "NIE"
            6
        end
    end

    def update_average_vote
        cs = self.citizen_services.where.not(citizen_vote: nil)
        if !cs.blank?
            self.update(average_vote: cs.average("citizen_vote::integer").to_f.round(2))
        end
    end

    
end