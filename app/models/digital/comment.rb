class Digital::Comment < ActiveRecord::Base
  self.table_name = "digital_comments"
  
  ######## RELACIONES ########
  belongs_to :moderable, polymorphic: true, optional: true
  belongs_to :userable, polymorphic: true, optional: true
  belongs_to :digital_comment, class_name: "Digital::Comment", foreign_key: "digital_comment_id",  optional: true
  belongs_to :digital_topic, class_name: "Digital::Topic", foreign_key: "digital_topic_id", optional: true
  belongs_to :moderate_status, class_name: "ModerateStatus", foreign_key: "moderate_status_id", optional: true
  has_many :digital_votes, as: :votable, class_name: "Digital::Vote"
  has_many :digital_comments, class_name: "Digital::Comment", foreign_key: "digital_comment_id"

  ######## VALIDACIONES ########
  validates :body, presence: true

  scope :active,   ->(){ where(active: true).where("digital_comments.moderate_status_id is not null and digital_comments.moderate_status_id in (?)", ModerateStatus.where("code in ('accept','edit')").select(:id)) }


end
