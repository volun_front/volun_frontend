class Digital::Faq < ActiveRecord::Base  
  self.table_name = "digital_faqs"
  
  ######## RELACIONES ########
  belongs_to :project, class_name: "Project", foreign_key: "project_id"

  scope :active,   ->(){ where(active: true) }

  def to_s
    title
  end

end
