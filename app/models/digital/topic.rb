class Digital::Topic < ActiveRecord::Base
  self.table_name = "digital_topics"
  
  ######## RELACIONES ########
  belongs_to :moderable, polymorphic: true, optional: true
  belongs_to :userable, polymorphic: true, optional: true
  belongs_to :moderate_status, class_name: "ModerateStatus", foreign_key: "moderate_status_id", optional: true
  belongs_to :project, class_name: "Project", foreign_key: "project_id"

  has_many :digital_comments, class_name: "Digital::Comment", foreign_key: "digital_topic_id"
  has_many :digital_follows, class_name: "Digital::Follow", foreign_key: "digital_topic_id"
  has_many :digital_votes, as: :votable, class_name: "Digital::Vote"

  accepts_nested_attributes_for :digital_comments, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :digital_follows, reject_if: :all_blank, allow_destroy: true

  validates_associated :digital_comments
  validates_associated :digital_follows

  ######## VALIDACIONES ########
  validates :title,:body, presence: true
  validates :title, uniqueness: true

  scope :active,   ->(){ where(active: true).where("digital_topics.moderate_status_id is not null and digital_topics.moderate_status_id in (?)", ModerateStatus.where("code in ('accept','edit')").select(:id)) }

  
  def to_s
    title
  end

end
