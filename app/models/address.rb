class Address < ActiveRecord::Base
  ROAD_NUMBER_TYPES = %w(num km.)
  GRADERS = [*'A'..'Z']

  belongs_to :nationality, class_name: "Nationality", foreign_key: "nationality_id", optional: true
  belongs_to :province, class_name: "Province", foreign_key: "province_id", optional: true
  belongs_to :district, class_name: "District", foreign_key: "district_id", optional: true
  belongs_to :borought, class_name: "Borought", foreign_key: "borought_id", optional: true
  validates :road_number_type, :road_type, :road_name, :road_number, :postal_code, :town, :province, presence: true

  def self.district_list
    District.where(active: true)
  end

  def self.borougth_list(district)
    Borought.joins(:district).where("boroughts.active= true").where("cast(districts.code as int)=cast(? as int)", district)    
  end

end
