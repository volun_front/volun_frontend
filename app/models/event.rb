class Event < ActiveRecord::Base
  belongs_to :eventable, polymorphic: true,  foreign_key: "eventable_id", optional: true
  belongs_to :address,  class_name: "Address", foreign_key: "address_id", optional: true
  belongs_to :project,  (-> { includes(:events).where(events: { eventable_type: Project.name  }) }), foreign_key: 'eventable_id'
  belongs_to :activity, (-> { includes(:events).where(events: { eventable_type: Activity.name }) }), foreign_key: 'eventable_id'

  has_many :timetables,  class_name: "Timetable", foreign_key: "event_id"

  default_scope { where(publish: true) }

end
