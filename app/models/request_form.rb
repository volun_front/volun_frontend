class RequestForm < ActiveRecord::Base
  belongs_to :request_type, class_name: "RequestType", foreign_key: "request_type_id", optional: true
  belongs_to :req_rejection_type, class_name: "ReqRejectionType", foreign_key: "req_rejection_type_id", optional: true
  belongs_to :user, class_name: "User", foreign_key: "user_id", optional: true
  belongs_to :rt_extendable, polymorphic: true, optional: true
  belongs_to :req_reason, class_name: "ReqReason", foreign_key: "req_reason_id", optional: true

  accepts_nested_attributes_for :rt_extendable

end
