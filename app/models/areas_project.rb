class AreasProject < ActiveRecord::Base
  belongs_to :area,  class_name: "Area", foreign_key: "area_id", optional: true
  belongs_to :project,  class_name: "Project", foreign_key: "project_id", optional: true
end
