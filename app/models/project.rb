# class de projects of voluntaries
class Project < ActiveRecord::Base
  belongs_to :pt_extendable, polymorphic: true, optional: true
  belongs_to :father, optional: true, class_name: "Project", foreign_key: "father_id"
  belongs_to :entity, -> { where(active: true).order('entities.name asc') }, required: true, class_name: "Entity", foreign_key: "entity_id"
  belongs_to :district, class_name: "District", foreign_key: "district_id", optional: true
  
  has_and_belongs_to_many :projects_volunteers, class_name: "ProjectsVolunteer"
  has_many :volunteer_projects_relation, class_name: "VolunteerProjectsRelation"
  has_and_belongs_to_many :volunteers, :through => :projects_volunteers
  has_and_belongs_to_many :areas, -> { where(active: true).order('areas.name asc') }, class_name: "Area"
  has_and_belongs_to_many :inactive_areas, -> { where(active: false).order('areas.name asc') }, class_name: 'Area'
  has_and_belongs_to_many :collectives, -> { where(active: true).order('collectives.name asc') }, class_name: "Collective"
  has_and_belongs_to_many :inactive_collectives, -> { where(active: false).order('collectives.name asc') }, class_name: 'Collective'
  has_and_belongs_to_many :coordinations, -> { where(active: true).order('coordinations.name asc') }, class_name: "Coordination"
  has_and_belongs_to_many :inactive_coordinations, -> { where(active: false).order('coordinations.name asc') }, class_name: 'Coordination'
  
  has_many :derivaties_volunteers, class_name: "DerivatiesVolunteer"
  has_many :documents, class_name: "Document"
  has_many :albums, class_name: "Album"
  has_many :activities, class_name: "Activity"
  has_many :subprojects, class_name: "Project", foreign_key: "father_id"
  has_many :events, as: :eventable
  has_many :events, as: :eventable
  has_many :addresses, through: :events
  has_many :timetables, through: :events
  has_many :trackings,         :class_name => 'Pro::Tracking'#, foreign_key: "project_id"
  has_many :volun_trackings,   :class_name => 'Volun::Tracking'
  has_many :volun_contacts,    :class_name => 'Volun::Contact'
  has_many :volun_assessments, :class_name => 'Volun::Assessments'
  has_many :links, as: :linkable
  has_many :images, -> { project_images }, class_name: 'Link', foreign_key: 'linkable_id'
  has_many :videos, -> { project_videos }, class_name: 'Link', foreign_key: 'linkable_id'
  has_many :docs,   -> { project_docs   }, class_name: 'Link', foreign_key: 'linkable_id'
  has_many :urls,   -> { project_urls   }, class_name: 'Link', foreign_key: 'linkable_id'
  has_many :entity_organizations, class_name: "EntityOrganization"
  has_many :meetings, class_name: "Meeting"
  has_many :digital_faqs, class_name: "Digital::Faq", foreign_key: 'project_id'
  has_many :digital_topics, class_name: "Digital::Topic", foreign_key: 'project_id'

  has_one  :logo,   -> { project_logo   }, class_name: 'Link', foreign_key: 'linkable_id',  required: false, validate: false
  

  accepts_nested_attributes_for :events
  accepts_nested_attributes_for :links
  accepts_nested_attributes_for :albums

  default_scope { where(publish: true, active: true,suspended: false) }
  scope :all_active,   ->(){ where(active: true).count }
  scope :featured, (-> { where(outstanding: true).limit(3) })
  scope :no_urgent, (-> { where(urgent: false) })
  scope :urgent, (-> { where(urgent: true) })
  scope :entity_projects, (->(id) { where('entity_id = ?', id) })
  scope :volun_project_with_date, ->(year) { where("id IN 
    (select volun_trackings.project_id from volun_trackings, tracking_types where volun_trackings.project_id is not null AND cast(volun_trackings.tracked_at as date) >= '#{year}' AND volun_trackings.tracking_type_id = tracking_types.id AND tracking_types.alias_name='subscribe') AND id IN 
    (select volun_trackings.project_id from volun_trackings, tracking_types where volun_trackings.project_id is not null AND cast(volun_trackings.tracked_at as date) <= '#{year}' AND volun_trackings.tracking_type_id = tracking_types.id AND tracking_types.alias_name='unsubscribe')")}

  def self.ransack_default
    { s: 'projects.id desc' }
  end

  def select_icon_by_project_type
    # case self.project_type.id.to_i
    #   when 3
    #     ActionController::Base.helpers.asset_path('project_blue.png').to_s
    #   when 4
    #     ActionController::Base.helpers.asset_path('project_green.png').to_s
    #   else
        ActionController::Base.helpers.asset_path('project_yellow.png').to_s
    # end
  end

  def self.main_columns
    %i(      
      name      
      entity        
    )
  end

  def self.subproyect_columns
    %i(      
      name
      entity     
    )
  end

  def self.matriz_columns
    %i(
      name
      entity
    )
  end

end
