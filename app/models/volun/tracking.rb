class Volun::Tracking < ActiveRecord::Base
  belongs_to :volunteer, class_name: "Volunteer", foreign_key: "volunteer_id", optional: true
  belongs_to :tracking_type, class_name: "TrackingType", foreign_key: "tracking_type_id", optional: true
  belongs_to :project, class_name: "Project", foreign_key: "project_id", optional: true
  belongs_to :manager, class_name: "Manager", foreign_key: "manager_id", optional: true
  belongs_to :request_form, class_name: "RequestForm", foreign_key: "request_form_id", optional: true
  default_scope { order(updated_at: :desc)}

  validates :volunteer_id, :tracking_type_id, :tracked_at, presence: true

  def self.main_columns
    %i(volunteer tracking_type project manager tracked_at automatic comments)
  end

  # def self.to_csv(volun_trackings)
  #   CSV.generate(col_sep:';', encoding:'ISO-8859-1') do |csv|
  #     csv << main_columns.map{ |column_name| human_attribute_name(column_name) } + [RequestForm.model_name.human]
  #     volun_trackings.each do |volun_tracking|
  #       csv << main_columns.map{ |column_name| 
  #         if column_name.to_s=="automatic"
  #           if (volun_tracking.public_send column_name).to_s=="true"
  #             "Sí"
  #           else
  #             "No"
  #           end
  #         else
  #           volun_tracking.public_send column_name
  #         end
  #         } + [(volun_tracking.request_form.request_type unless volun_tracking.request_form.nil?)]
  #     end
  #   end
  # end

end
