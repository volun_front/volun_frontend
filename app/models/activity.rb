class Activity < ActiveRecord::Base
  
  belongs_to :project
  belongs_to :entity

  has_many :events, class_name: "Event", foreign_key: 'eventable_id', as: :eventable
  has_many :links, class_name: "Link", foreign_key: 'linkable_id', as: :linkable
  has_many :timetables, through: :events
  has_many :addresses, through: :events
  
  has_and_belongs_to_many :areas_activities, optional: true
  has_and_belongs_to_many :areas, ->{ where(active: true).order('areas.name asc') }, :through => :areas_activities
  has_many :images, -> { activity_images }, class_name: 'Link', foreign_key: 'linkable_id'
  has_many :videos, -> { activity_videos }, class_name: 'Link', foreign_key: 'linkable_id'
  has_many :docs,   -> { activity_docs   }, class_name: 'Link', foreign_key: 'linkable_id'
  has_many :urls,   -> { activity_urls   }, class_name: 'Link', foreign_key: 'linkable_id'
  has_one  :logo,   -> { activity_logo   }, class_name: 'Link', foreign_key: 'linkable_id', required: false, validate: false


  accepts_nested_attributes_for :events
  accepts_nested_attributes_for :links
  accepts_nested_attributes_for :videos, reject_if:  :all_blank, allow_destroy: true
  accepts_nested_attributes_for :docs,   reject_if:  :all_blank, allow_destroy: true
  accepts_nested_attributes_for :urls,   reject_if:  :all_blank, allow_destroy: true
  accepts_nested_attributes_for :logo,   reject_if:  :all_blank, allow_destroy: true

  default_scope { where(publish: true, active: true) }
  scope :activities_present, (->(day) { where("timetables.execution_date >= ? and events.eventable_type='Activity'", day) })

  def to_s
    name
  end

  def self.ransack_default
    { s: 'id desc' }
  end
end
