class Audit < ActiveRecord::Base
	belongs_to :user,  class_name: "User", foreign_key: "user_id", optional: true
    belongs_to :resource,  class_name: "Resource", foreign_key: "resource_id", optional: true
    accepts_nested_attributes_for :user
    accepts_nested_attributes_for :resource
end
