class Album < ActiveRecord::Base

    belongs_to :project, class_name: "Project", foreign_key: "project_id", optional: true
    has_many :links, as: :linkable, class_name: "Link", foreign_key: "linkable_id"
    has_many :images, -> { album_images }, class_name: 'Link', foreign_key: 'linkable_id'

    accepts_nested_attributes_for :links
end
