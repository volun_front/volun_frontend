class NewCampaing < ActiveRecord::Base

    has_many :links, as: :linkable, class_name: "Link", foreign_key: "linkable_id"
    
    accepts_nested_attributes_for :links

    def self.main_column
        %i(title
            operation)
    end

    def campaing_attributes
        {
            title:          self.title
        }
    end

    def self.ransack_default
        { s: 'new_campaing.id desc' }
      end
end
