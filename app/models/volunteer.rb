class Volunteer < ActiveRecord::Base
  belongs_to :address, class_name: "Address", foreign_key: "address_id", optional: true
  belongs_to :id_number_type, required: true, class_name: "IdNumber", foreign_key: "id_number_id"
  has_many :volun_proy_meetings, class_name: "VolunProyMeeting"
  has_and_belongs_to_many :projects, (-> { where(active: true).order('projects.name asc') }), class_name: "Project"
  scope :all_active,   ->(){ where(active: true).count }

  has_one  :user,  as: :loggable, class_name: "User", foreign_key: "loggable_id"

  validates :is_adult, presence: true

  def self.ransack_default
    { s: 'volunteer.id desc' }
  end

  def full_name
    "#{name} #{last_name} #{last_name_alt}".strip
  rescue
    "#{name} #{last_name} #{last_name_alt}"
  end
end
