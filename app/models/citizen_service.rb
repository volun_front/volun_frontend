class CitizenService < ActiveRecord::Base

    belongs_to :citizen, class_name: "Citizen", foreign_key: "citizen_id", optional: true
    belongs_to :service, class_name: "Service", foreign_key: "service_id", optional: true
    belongs_to :volunteer, class_name: "Volunteer", foreign_key: "volunteer_id", optional: true
    belongs_to :status_service, class_name: "StatusService", foreign_key: "status_service_id", optional: true
    belongs_to :volunteer_vote_rel, class_name: "ServiceVote", foreign_key: "volunteer_vote_id", optional: true
    belongs_to :citizen_vote_rel, class_name: "ServiceVote", foreign_key: "citizen_vote_id", optional: true
    belongs_to :borought, class_name: "Borought", foreign_key: "borought_id", optional: true
    belongs_to :district, class_name: "District", foreign_key: "district_id", optional: true

    has_many :service_responses, class_name: "ServiceResponse", foreign_key: "citizen_service_id"
    has_many :service_votes, class_name: "ServiceVote", foreign_key: "citizen_service_id"
    has_many :trackings, class_name: "CitizenServiceTracking", foreign_key: "citizen_service_id"
    
    validates :citizen_id, presence: true
    validates :service_id, presence: true
    validates :date_request, presence: true
    validates :hour_request, presence: true
    validates :hour_request, format: { with: /\d{2}:\d{2}/ }
    validates :hour_accept_request, format: {  with: /\d{2}:\d{2}/  }, allow_blank: true

    validate :hour_request_valid
    validate :hour_accept_valid
    validate :unique_service_in_day

    scope :not_market, -> () { where("citizen_id in (select citizens.id from citizens where citizens.market = false)")}
    scope :not_tester, ->() { not_market.where("citizen_id in (select citizens.id from citizens where citizens.tester = false)") }
    scope :tester, ->() { not_market.where("citizen_id in (select citizens.id from citizens where citizens.tester = true)") }
    scope :all_prepared_active, -> () { where("status in ('active','accept','prioritary')")}
    scope :all_active,   ->(){ where(active: true) }
    scope :status,   ->(status){ where(status: status) }
    scope :for_ids_with_order, ->(ids) {
        # order = sanitize_sql_array(
        #   ["position(id::text in ?)", ids.join(',')]
        # )
        # where(:id => ids).order(order)

        where(:id => ids).order(id: :asc)
      }  

    def type_service
      return '' if self.service.blank?
      self.service.name
    end

    def hour_request_valid
      if !self.hour_request.blank? && get_time(self.hour_request).to_s == '-'
          errors.add(:hour_request, "Hora incorrecta del servicio")
      end
  end

  def hour_accept_valid        
      if !self.hour_accept_request.blank? && get_time(self.hour_accept_request).to_s == '-'
          errors.add(:hour_accept_request, "Hora incorrecta de aceptación del servicio")
      end
  end
  

  def unique_service_in_day
      if self.id.blank?
          services = CitizenService.where(citizen_id: self.citizen_id).where("cast(date_request as date) = cast(? as date)",self.date_request).all_prepared_active
      else
          services = CitizenService.where(citizen_id: self.citizen_id).where("id != ? and cast(date_request as date) = cast(? as date)",self.id, self.date_request).all_prepared_active
      end
      if !services.blank?
          errors.add(:date_request, "Servicio mayor no se ha podido #{self.id.blank? ? "crear" : "modificar"}. Ya existen peticiones de este mayor para este día")
      end        
  end

  private

  def get_time(str)
    DateTime.strptime(str, '%H:%M')
  rescue
      "-"
  end

end