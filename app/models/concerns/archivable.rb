module Archivable
  extend ActiveSupport::Concern

  included do

    def archived?
      !active?
    end

  end
end