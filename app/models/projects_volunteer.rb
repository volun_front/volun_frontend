# areas of project
class ProjectsVolunteer < ActiveRecord::Base
  belongs_to :volunteer, class_name: "Volunteer", foreign_key: "volunteer_id", optional: true
  belongs_to :project, class_name: "Project", foreign_key: "project_id", optional: true
  belongs_to :shift_definition, class_name: "ShiftDefinition", foreign_key: "shift_definition_id", optional: true

  scope :gender, -> (){ Volunteer.group(:gender) }

  def description_turn
    if !self.day.blank? && !self.shift_definition.blank?
        self.try(:day) + " - " + self.shift_definition.try(:name)
    end
  end
end
