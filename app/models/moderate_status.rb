class ModerateStatus < ActiveRecord::Base
  self.table_name = "moderate_statuses"
  
  def to_s
    title
  end

end
