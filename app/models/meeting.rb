class Meeting < ActiveRecord::Base

    belongs_to :project, class_name: "Project", foreign_key: "project_id", optional: true
    belongs_to :volun_proy_meeting, class_name: "VolunProyMeeting", foreign_key: "volun_proy_meeting_id", optional: true

    validates :name, uniqueness: true
    validates :name, presence: true

    def self.main_columns
        [:id, :name, :date, :hour, :place]
    end

    def to_s
        name
    end


end
