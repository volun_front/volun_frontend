class Rt::VolunteerAmendment < ActiveRecord::Base
  include RtCommons
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  VALID_PHONE_NUMBER_REGEX = /\d[0-9]\)*\z/

  belongs_to :volunteer, foreign_key: 'volunteer_id', class_name: "Volunteer", optional: true
  belongs_to :address, class_name: "Address", foreign_key: "address_id", optional: true
  belongs_to :province, class_name: "Province", foreign_key: "province_id", optional: true
  belongs_to :district, class_name: "District", foreign_key: "district_id", optional: true
  belongs_to :borought, class_name: "Borought", foreign_key: "borought_id", optional: true
  

  validates :phone_number, format: { with: VALID_PHONE_NUMBER_REGEX }, unless: -> { phone_number.blank?}
  validates :phone_number_alt, format: { with: VALID_PHONE_NUMBER_REGEX, if: -> { phone_number_alt.present? } }
  validates :email, format: { with: VALID_EMAIL_REGEX }, unless: -> { email.blank?}
  validates :road_type, :road_name, :number_type, :road_number, :postal_code, :province,
            :town, presence: true
  VALID_CODIGOPOSTAL_REGEX = /\A(\d{5})\z/
  validates :postal_code, format: { with: VALID_CODIGOPOSTAL_REGEX }, unless: -> { postal_code.blank?}
  validate  :email_duplicated
  validate  :data_mod


  def email_duplicated
    exists=Rt::VolunteerAmendment.where(email: self.email)
    email_exist=Volunteer.where(email: self.email)

    unless email_exist.blank?
      email_exist.each do |exist|
        if exist.blank? || exist.id!=self.request_form.user.loggable.id 
          errors.add(:email, :email_invalid) 
          break
        end
      end
    end

    unless exists.blank?
      exists.each do |exist|
        if exist.request_form.user.blank? || exist.request_form.user.loggable.id!=self.request_form.user.loggable.id 
          errors.add(:email, :taken) 
          break
        end
      end
    end
  end

  def data_mod
    if self.request_form.try(:user).try(:id).blank?
      errors.add(:base, :without_change)
    else
      original=User.find(self.request_form.user.id)
      voluntario=original.loggable
      direccion=voluntario.address

      if original.email==self.email && voluntario.phone_number==self.phone_number && voluntario.phone_number_alt==self.phone_number_alt && direccion.road_number==self.road_number && direccion.road_type==self.road_type && direccion.road_name==self.road_name && direccion.postal_code==self.postal_code && direccion.province==self.province
        errors.add(:base, :without_change)
      end
    end
  end

  def set_data
    "<h4 style = 'margin-top: 10px'>Solicitud de modificación de datos personales</h4>"
  end
end
