class Rt::ProjectPublishing < ActiveRecord::Base
  include RtCommons 
  belongs_to :project, class_name: "Project", foreign_key: "project_id", optional: true
  belongs_to :province, class_name: "Province", foreign_key: "province_id",optional: true
  belongs_to :district, class_name: "District", foreign_key: "district_id", optional: true
  belongs_to :borought, class_name: "Borought", foreign_key: "borought_id",optional: true
  belongs_to :entity, class_name: "Entity", foreign_key: "entity_id", optional: true
  
  validates :notes, :description, presence: true 
  VALID_CODIGOPOSTAL_REGEX = /\A(\d{5})\z/ 
  validates :postal_code, format: { with: VALID_CODIGOPOSTAL_REGEX },allow_blank: true 
  validate :need_district 
 
 
  def need_district 
  if (self.road_number =="" || self.road_type.to_s =="" || self.road_name.to_s =="" || self.postal_code.to_s =="" || self.province.to_s =="" || self.town.to_s =="") && self.district.blank?
      errors.add(:district, :without_address) 
    end 
  end 

  def set_data
    unless self.notes.blank? 
      "<h4 style = 'margin-top: 10px'><b>Nombre:</b> #{self.notes}</h4>"
    end
  end
end 
