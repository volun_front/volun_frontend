class Rt::VolunteersDemand < ActiveRecord::Base
  include RtCommons
  belongs_to :project, class_name: "Project", foreign_key: "project_id", optional: true
  belongs_to :province, class_name: "Province", foreign_key: "province_id",optional: true
  belongs_to :district, class_name: "District", foreign_key: "district_id", optional: true
  belongs_to :borought, class_name: "Borought", foreign_key: "borought_id",optional: true
  belongs_to :entity, class_name: "Entity", foreign_key: "entity_id", optional: true

  VALID_CODIGOPOSTAL_REGEX = /\A(\d{5})\z/
  validates :notes, :road_type, :road_name, :number_type, :road_number, :postal_code, :province,
            :town, :description, :execution_start_date, :requested_volunteers_num, :volunteers_profile,
            :volunteer_functions_1, presence: true
  validates :postal_code, format: { with: VALID_CODIGOPOSTAL_REGEX }, unless: -> { postal_code.blank?}
  validates_numericality_of :requested_volunteers_num

  def set_data
    unless (self.requested_volunteers_num.blank? && self.notes.blank?)
    	if self.notes.blank?
    		"<h4 style = 'margin-top: 10px'><b>Número de voluntarios solicitado:</b> #{self.requested_volunteers_num}</h4>"
    	elsif self.requested_volunteers_num.blank?
    		"<h4 style = 'margin-top: 10px'><b>Proyecto:</b> #{self.notes}</h4>"
    	else
    		"<h4 style = 'margin-top: 10px'><b>Proyecto:</b> #{self.notes}</h4>
    		<h4 style = 'margin-top: 10px'><b>Número de voluntarios solicitado:</b> #{self.requested_volunteers_num}</h4>"
    	end
    end
  end
end
