# request publish activity
class Rt::ActivityPublishing < ActiveRecord::Base
  include RtCommons
  belongs_to :project, class_name: "Project", foreign_key: "project_id", optional: true

  VALID_CODIGOPOSTAL_REGEX = /\A(\d{5})\z/
  validates :name, :description, :organizer, presence: true
  validates :dates_text_free, :hours_text_free, :places_text_free, presence: true
  def to_s
    name
  end

  def set_data
    unless (self.name.blank? && self.organizer.blank?)
    	if self.name.blank?
    		"<h4 style = 'margin-top: 10px'><b>Organizadores:</b> #{self.organizer}</h4>"
    	elsif self.organizer.blank?
    		"<h4 style = 'margin-top: 10px'><b>Nombre:</b> #{self.name}</h4>"
    	else
    		"<h4 style = 'margin-top: 10px'><b>Nombre:</b> #{self.name}</h4>
    		<h4 style = 'margin-top: 10px'><b>Organizadores:</b> #{self.organizer}</h4>"
    	end
    end
  end
end
