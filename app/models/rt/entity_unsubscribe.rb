class Rt::EntityUnsubscribe < ActiveRecord::Base
  include RtCommons
  validates :notes, presence: true
  def set_data
    "<h4 style = 'margin-top: 10px'>Solicitud de baja de entidad</h4>"
  end
end
