# model for volunteer appoinment
class Rt::VolunteerAppointment < ActiveRecord::Base
  include RtCommons
  validates :notes, presence: true

  def set_data
    if self.notes.blank?
      "<h4 style = 'margin-top: 10px'>No hay notas referentes a esta solicitud</h4>"
    else
      "<h4 style = 'margin-top: 10px'>#{self.notes.to_s}</h4>"
    end
  end
end
