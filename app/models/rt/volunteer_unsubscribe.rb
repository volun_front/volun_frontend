# model for volunteer unsubscribe
class Rt::VolunteerUnsubscribe < ActiveRecord::Base
  include RtCommons
  belongs_to :project, required: true, class_name: "Project", foreign_key: "project_id"
  belongs_to :unsubscribe_level, class_name: "UnsubscribeLevel", foreign_key: "unsubscribe_level_id", optional: true
  validates :notes, presence: true

  def to_s
    name
  end

  def set_data
    if self.project_id.blank?
      "<h4 style = 'margin-top: 10px'>Solicitud de baja como voluntario</h4>"
    else
      "<h4 style = 'margin-top: 10px'>Solicitud de baja como voluntario en el proyecto #{Project.find(self.project_id).name.to_s}</h4>"
    end
  end
end
