# model of volunteer subscribe
class Rt::VolunteerSubscribe < ActiveRecord::Base
  include RtCommons
  @terms_of_service=""
  belongs_to :info_source, class_name: "InfoSource", foreign_key: "info_source_id", optional: true
  belongs_to :district, class_name: "District", foreign_key: "district_id", optional: true
  belongs_to :channel, class_name: "Channel", foreign_key: "channel_id", optional: true
  belongs_to :entry_way, class_name: "EntryWay", foreign_key: "entry_way_id", optional: true

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :name, presence: true
  validates :last_name, presence: true
  validates :email, presence: true
  validates :phone_number_alt, format: { with: /[6|7]\d{8}/ }, presence: true
  validates :phone_number, format: { with: /[8|9]\d{8}/ }, allow_blank: true
  validates :email, format: { with: VALID_EMAIL_REGEX , if: -> { email.present? }}#, uniqueness: { scope: :project_id, case_sensitive: false }
  validate :valid_terms_of_service
  validates :is_adult, presence: true

  def valid_terms_of_service
    if (@terms_of_service.blank? || @terms_of_service.to_s=="false")
      errors.add(:base, :invalid_terms_of_service) 
    end
  end

  def set_terms_of_service(terminos)
    @terms_of_service=terminos
  end

  def to_s
    name
  end

  def set_data
    if self.project_id.blank?
      "<h4 style = 'margin-top: 10px'>Solicitud de alta como voluntario</h4>"
    else
      if Project.where(id: self.project_id)[0].blank?
        "<h4 style = 'margin-top: 10px'>Solicitud de alta como voluntario en proyecto</h4>"
      else
        "<h4 style = 'margin-top: 10px'>Solicitud de alta como voluntario en el proyecto #{Project.where(id: self.project_id)[0].name.to_s}</h4>"
      end
    end
  end
end
