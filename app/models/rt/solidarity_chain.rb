class Rt::SolidarityChain < ActiveRecord::Base
    include RtCommons

    VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
    VALID_PHONE_NUMBER_REGEX = /[6|7|8|9]\d{8}/
    validates :full_name, presence: true
    validates :phone_number, presence: true
    validates :phone_number, format: { with: VALID_PHONE_NUMBER_REGEX }, length: {is: 9}, unless: -> { phone_number.blank?}
    validates :email, presence: true
    validates :email, format: { with: VALID_EMAIL_REGEX }, unless: -> { email.blank?}
    validates :action_description, presence: true, length: { maximum: 1000}
    validates :alias, length: {maximum: 25}
    has_attached_file :url_image,
            styles: lambda{ |a|
            return {} unless a.content_type.in?  %w(image/jpeg image/jpg image/png) 
            { thumb:  '100x100#', small:  '200x200#', medium: '300x300>' }
            },
            processors: lambda { |a| 
              begin
                a.is_video? ? [ :ffmpeg ] : [ :thumbnail ] 
              rescue => exception
                [ :thumbnail ] 
              end
            },
            default_url: '/images/missing.png',
            url: '/SolidaryChain/:id/:attachment/:style/:filename'
    validates_attachment_content_type :url_image,content_type: [
        'image/jpg',
        'image/jpeg',
        'image/png',
        'video/avi',
        'video/mov',
        'video/mp4',
        'video/x-flv',
        'video/quicktime',
        'video/mpeg',
        'video/mpeg4'
      ] 
    
    validates_attachment_size :url_image, :in => 0.megabytes..10.megabytes

    scope :aproved, -> (){ joins("INNER JOIN request_forms ON rt_solidarity_chains.id=request_forms.rt_extendable_id").where("request_forms.rt_extendable_type='Rt::SolidarityChain' AND request_forms.request_type_id=13 AND request_forms.req_status_id=3").order("request_forms.updated_at desc")}

    after_save :update_url_image

    def translate_date
       self.request_form.updated_at.strftime('%d %B %Y')
    end

    private 
      def update_url_image
        update_column :url_image, url_image.url 
      end
end
