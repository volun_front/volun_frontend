class Rt::ProjectUnpublishing < ActiveRecord::Base
  include RtCommons
  belongs_to :project, required: true, class_name: "Project", foreign_key: "project_id"
  validates :notes, presence: true
end
