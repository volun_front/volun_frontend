# request unpublish activity
class Rt::ActivityUnpublishing < ActiveRecord::Base
  include RtCommons
  belongs_to :activity, required: true, class_name: "Activity", foreign_key: "activity_id"
  validates :notes, presence: true

  def set_data
    unless self.notes.blank? 
      "<h4 style = 'margin-top: 10px'><b>Observaciones:</b> #{self.notes}</h4>"
    end
  end
end
