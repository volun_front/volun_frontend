class Rt::Other < ActiveRecord::Base
  include RtCommons
  validates :description, presence: true

  def set_data
    if self.description.blank?
      "<h4 style = 'margin-top: 10px'>No existe información relativa a esta solicitud</h4>"
    else
      "<h4 style = 'margin-top: 10px'>#{self.description.html_safe}</h4>"
    end
  end
end
