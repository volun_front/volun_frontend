class TrackingType < ActiveRecord::Base

  AUTOMATIC_TRACKINGS = {
    project_subscribe:   'project_subscribe'
  }

  has_many :trackings, class_name: 'Volun::Tracking'

  validates :name, presence: true, uniqueness: true
  validates :alias_name, uniqueness: true, if: -> {alias_name.present?}

  scope :all_active,   ->(){ where(active: true) } 
  scope :all_volunteers, ->(){all_active.where("system=false AND (tracking_types_type='Volunteer' OR tracking_types_type='' OR tracking_types_type IS NULL)")}

  def self.get_volunteer_project_subscribe
    where(alias_name: AUTOMATIC_TRACKINGS[:project_subscribe]).take!
  end

  def to_s
    name
  end

end
