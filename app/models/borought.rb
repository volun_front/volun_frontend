class Borought < ActiveRecord::Base

  belongs_to :district, class_name: "District", foreign_key: "district_id"

  def to_s
    name
  end

  def district_code
    self.district.code
  rescue
    ""
  end
end
