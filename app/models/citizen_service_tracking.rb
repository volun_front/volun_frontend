class CitizenServiceTracking < ApplicationRecord

    belongs_to :citizen_service, class_name: "CitizenService", foreign_key: "citizen_service_id", optional: true
    belongs_to :tracking_type, ->{order(:name)}, class_name: "TrackingType", foreign_key: "tracking_type_id", optional: true
    belongs_to :manager ,class_name: "Manager", foreign_key: "manager_id", optional: true, required: false
  
end  