class VirtualCommunity::Component < ActiveRecord::Base
 
  self.table_name = "virtual_community_components"
  
  ######## RELACIONES ########
  belongs_to :component_type, class_name: "::VirtualCommunity::ComponentType", foreign_key: "virtual_community_component_type_id",  optional: true
  belongs_to :component_parent, class_name: "::VirtualCommunity::Component", foreign_key: "virtual_community_component_id",  optional: true
  has_many :child_components, class_name: "::VirtualCommunity::Component", foreign_key: "virtual_community_component_id"


  
  has_attached_file :image_header,
                    styles: lambda{ |a|
                      return {} unless a.content_type.in? %w(image/jpeg image/png image/jpg image/gif)
                      { thumb:  '100x100#', small:  '200x200#', medium: '300x300>' }
                    },
                    default_url: '/images/missing.png',
                    url: '/system/:class/:id/:attachment/:style/:filename'
  validates_attachment_content_type :image_header, content_type: /\Aimage\/.*\z/ 
  
 

end
