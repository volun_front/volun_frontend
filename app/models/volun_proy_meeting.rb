class VolunProyMeeting < ActiveRecord::Base

    belongs_to :meetings, class_name: "Meeting", foreign_key: "meeting_id", optional: true
    belongs_to :projects_volunteer, class_name: "ProjectsVolunteer", foreign_key: "projects_volunteer_id", optional: true
    belongs_to :volunteer, class_name: "Volunteer", foreign_key: "volunteer_id", optional: true
    belongs_to :users, class_name: "User", foreign_key: "user_id", optional: true

    def self.main_columns
        [ :confirm, :asist ]
    end
end
