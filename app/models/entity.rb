class Entity < ActiveRecord::Base
  belongs_to :entity_type,  class_name: "EntityType", foreign_key: "entity_type_id", optional: true
  belongs_to :req_reason,  class_name: "ReqReason", foreign_key: "req_reason_id", optional: true
  belongs_to :address,  class_name: "Address", foreign_key: "address_id", optional: true

  has_many :projects,  class_name: "Project"
  has_many :links, as: :linkable,  class_name: "Link", foreign_key: "linkable_id"
  has_one  :logo,   -> { entity_logo   }, class_name: 'Link', foreign_key: 'linkable_id'
  has_many :images, -> { entity_images }, class_name: 'Link', foreign_key: 'linkable_id'
  has_many :docs,   -> { entity_docs   }, class_name: 'Link', foreign_key: 'linkable_id'
  has_many :urls,   -> { entity_urls   }, class_name: 'Link', foreign_key: 'linkable_id'
  
  has_one  :user, as: :loggable,  class_name: "User", foreign_key: "loggable_id"

  accepts_nested_attributes_for :user
  accepts_nested_attributes_for :address

  validates :name, uniqueness: true
  validates :name, :vat_number, :email, :representative_name, :representative_last_name, :contact_name,
            :contact_last_name, :entity_type_id, :req_reason_id, presence: true
  validates :email, uniqueness: { case_sensitive: false }
  validates :phone_number, format: { with: /[6|7|8|9]\d{8}/ }, allow_blank: false
  validates :phone_number_alt, format: { with: /[6|7|8|9]\d{8}/ }, allow_blank: true
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i }, allow_blank: false
  #validates :vat_number, spanish_vat: true
  validates :vat_number,uniqueness: true, unless: -> {vat_number_ayto }
  validates :other_subscribe_reason, presence: true, if: -> { req_reason_id == 4 }

  def to_s
    name
  end

  alias_method :full_name, :to_s

  private
    def vat_number_ayto
      cif_ayto(self.vat_number)
    end

    def cif_ayto(cif)
      key=ActiveRecord::Base.connection.exec_query("SELECT value FROM settings WHERE key = 'VolunFrontend.cifAytoMadrid'")
      cif_ayto_mad = nil
      key.each do|k|
        cif_ayto_mad = k["value"]
      end
      return false if cif_ayto_mad.blank?
      cif == cif_ayto_mad
    end
end
