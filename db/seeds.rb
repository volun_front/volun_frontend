#[NoticeType, Req::Status, ProjectType, RequestType, EventType, UnsubscribeLevel, LinkType, Role].each do |model|
#  model.kinds.each do |kind_name , kind_num|
#    model.create!(id: kind_num, kind: kind_num, description: kind_name)
#  end
#end

# If devise_auth is false then uweb login is mandatory, otherwise devise login is mandatory
#

require 'database_cleaner'

DatabaseCleaner.clean_with :truncation
Faker::Config.locale = I18n.locale

REQUEST_TYPES = {
  1  => 'rt_volunteer_subscribe',
  2  => 'rt_volunteer_unsubscribe',
  3  => 'rt_volunteer_amendment',
  4  => 'rt_volunteer_appointment',
  5  => 'rt_entity_subscribe',
  6  => 'rt_entity_unsubscribe',
  7  => 'rt_volunteers_demand',
  8  => 'rt_project_publishing',
  9  => 'rt_project_unpublishing',
  10 => 'rt_activity_publishing',
  11 => 'rt_activity_unpublishing',
  12 => 'rt_other',
  13 => 'rt_solidarity_chain'
}

PROJECT_TYPES = {
  1 => 'pt_social',
  2 =>  'pt_centre',
  3 =>  'pt_permanent',
  4 =>  'pt_punctual',
  5 =>  'pt_entity',
  6 =>  'pt_subvention',
  7 =>  'pt_other'

}

REQUEST_REASONS = {
  0 => 'Publicar proyectos',
  1 => 'Demandar voluntarios',
  2 => 'Difundir actividad en agenda',
  3 => 'Otros'
}

LINK_TYPES = {
  1 => 'logo',
  2 => 'image',
  3 => 'url',
  4 => 'video',
  5 => 'document'
}

REQUEST_STATUS = {
  1 => 'Pending',
  2 => 'Processing',
  3 => 'Approved',
  4 => 'Rejected'

}

AREA_NAMES = {
  'Derechos Sociales' => 'fa fa-handshake-o',
  'Ambiental' => 'fa fa-envira',
  'Cultural' => 'fa fa-building-o',
  'Deportivo' => 'fa fa-soccer-ball-o',
  'Educativo' => 'fa fa-mortar-board',
  'Ocio y tiempo libre' => 'fa fa-angellist',
  'Comunitario y/o de ciudad' => 'fa fa-group',
  'Animales' => ' fa fa-paw',
  'Participación' => 'fa fa-universal-access',
  'On line' => 'fas fa-globe',
  'Cuidados a las personas' => 'fa fa-child',
  'Sensibilización' => 'fa fa-heart-o',
  'Comunicación' => 'fa fa-laptop',
  'Cooperación' => 'fa fa-language',
  'Responsabilidad social' => 'fa fa-puzzle-piece',
  'Emergencias' => 'fa fa-life-saver',
  'Otros' => 'fa fa-tasks'
}

COLLECTIVE_NAMES = [
  'Menores',
  'Jóvenes',
  'Mayores',
  'Diversidad funcional',
  'Mujer',
  'Inmigrantes',
  'Refugiados',
  'Personas sin hogar',
  'Reclusos y exreclusos',
  'Personas enfermas',
  'Exclusión social',
  'Voluntariado',
  'Toda la población',
  'Otros'
]

DISTRICTS = {

  '01' => 'CENTRO',
  '02' => 'ARGANZUELA',
  '03' => 'RETIRO',
  '04' => 'SALAMANCA',
  '05' => 'CHAMARTIN',
  '06' => 'TETUAN',
  '07' => 'CHAMBERI',
  '08' => 'FUENCARRAL-EL PARDO',
  '09' => 'MONCLOA-ARAVACA',
  '10' => 'LATINA',
  '11' => 'CARABANCHEL',
  '12' => 'USERA',
  '13' => 'PUENTE VALLECAS',
  '14' => 'MORATALAZ',
  '15' => 'CIUDAD LINEAL',
  '16' => 'HORTALEZA',
  '17' => 'VILLAVERDE',
  '18' => 'VILLA DE VALLECAS',
  '19' => 'VICÁLAVARO',
  '20' => 'SAN BLAS',
  '21' => 'BARAJAS',
  '22' => 'OTRO MUNICIPIO',
  '99' => 'OTROS'

}

PROVINCES = {

  '1'  => 'ARABA-ALAVA',
  '2'  => 'ALBACETE',
  '3'  => 'ALICANTE-ALACANT',
  '4'  => 'ALMERIA',
  '5'  => 'AVILA',
  '6'  => 'BADAJOZ',
  '7'  => 'ILLES BALEARS',
  '8'  => 'BARCELONA',
  '9'  => 'BURGOS',
  '10' => 'CACERES',
  '11' => 'CADIZ',
  '12' => 'CASTELLON-CASTELLO',
  '13' => 'CIUDAD REAL',
  '14' => 'CORDOBA',
  '15' => 'A CORUÑA',
  '16' => 'CUENCA',
  '17' => 'GIRONA',
  '18' => 'GRANADA',
  '19' => 'GUADALAJARA',
  '20' => 'GIPUZKOA',
  '21' => 'HUELVA',
  '22' => 'HUESCA',
  '23' => 'JAEN',
  '24' => 'LEON',
  '25' => 'LLEIDA',
  '26' => 'LA RIOJA',
  '27' => 'LUGO',
  '28' => 'MADRID',
  '29' => 'MALAGA',
  '30' => 'MURCIA',
  '31' => 'NAVARRA',
  '32' => 'OURENSE',
  '33' => 'ASTURIAS',
  '34' => 'PALENCIA',
  '35' => 'LAS PALMAS',
  '36' => 'PONTEVEDRA',
  '37' => 'SALAMANCA',
  '38' => 'SANTA CRUZ DE TENERIFE',
  '39' => 'CANTABRIA',
  '40' => 'SEGOVIA',
  '41' => 'SEVILLA',
  '42' => 'SORIA',
  '43' => 'TARRAGONA',
  '44' => 'TERUEL',
  '45' => 'TOLEDO',
  '46' => 'VALENCIA',
  '47' => 'VALLADOLID',
  '48' => 'BIZKAIA',
  '49' => 'ZAMORA',
  '50' => 'ZARAGOZA',
  '51' => 'CEUTA',
  '52' => 'MELILLA'

}

ROAD_TYPES = {
    'ACCESO'     => '13',
    'ARROYO'     => '1',
    'AUTOPISTA'  => '10',
    'AUTOVIA'    => '364',
    'AVENIDA'    => '13063',
    'BULEVAR'    => '199',
    'CALLE'      => '176374',
    'CALLEJON'   => '159',
    'CAMINO'     => '1604',
    'CAMINOALTO' => '28',
    'CARRERA'    => '50',
    'CARRETERA'  => '831',
    'CAÑADA'     => '107',
    'COLONIA'    => '364 ',
    'COSTANILLA' => '107 ',
    'CUESTA'     => '113',
    'GALERIA'    => '10 ',
    'GLORIETA'   => '288',
    'PARQUE'     => '30',
    'PARTICULAR' => '21',
    'PASADIZO'   => '6',
    'PASAJE'     => '',
    'PASEO'      => '4239',
    'PISTA'      => '4',
    'PLAZA'      => '3478',
    'PLAZUELA'   => '16',
    'PUENTE'     => '1 ',
    'RONDA'      => ' ',
    'TRAVESIA'   => '1007',
}

PROPOSALS = %w(subvencionado desistido desestimado excluido)

ENTITY_TYPES = {
  0 => 'Organización',
  1 => 'Empresa',
  2 => 'Asociación',
  3 => 'FUNDACIÓN',
  4 => 'ONG'
}

NOTICE_TYPES = {
  0 => 'email',
  1 => 'sms',
  2 => 'papel'
}

REJECTION_TYPES = {
  1 => 'No procede'

}


# AREAS = {
#   1 => 'EDUCACION',
#   2 => 'CULTURA',
#   3 => 'EMPLEO',
#   4 => 'OTRA'
# }

ALL_RESOURCES = [
  'Rt::SolidarityChain',
  'Volunteer',
  'Volunteer_resource'
]


puts "Creando Medios de comunicación"
NOTICE_TYPES.each do |kind , name|
  NoticeType.create!(kind: kind, description: name)
end

puts "Creando Links"
LINK_TYPES.each do |kind , name|
  LinkType.create!(kind: kind, description: name)
end


# puts "Creando Areas"
# AREAS.each do |kind , name|
#   Area.create!(name: name, description: name, active: true)
# end

puts "Creando Areas Icono"
AREA_NAMES.each do |name , description|
  Area.create!(name: name, description: description, active: true)
end

puts "Creando Tipos de solicitudes"
REQUEST_TYPES.each do |kind , name|
  RequestType.create!(id:kind,kind: kind, description: name)
end

puts "Creando Tipos de entidades"
ENTITY_TYPES.each do |kind , name|
  EntityType.create!(name: name, description: name)
end

puts "Creando Tipos de proyectos"
PROJECT_TYPES.each do |kind_num , kind_name|
  ProjectType.create!(id: kind_num, kind: kind_num, description: kind_name)
end

puts "Creando Tipos de razones"
REQUEST_REASONS.each do |code, name|
  ReqReason.create!(name: name, description: name)
end


puts "Creando Tipos de status"
REQUEST_STATUS.each do |code, name|
  ReqStatus.create!(kind: code, description: name)
end

puts "Creando Tipos de rechazos"
REJECTION_TYPES.each do |code, name|
  ReqRejectionType.create!(description: name, name: name)
end

puts "Creando Distritos"
DISTRICTS.each do |code, name|
  District.create!(code: code, name: name)
end

puts "Creando Provincias"
PROVINCES.each do |code, name|
  Province.create!(code: code, name: name)
end


puts "Creando Tipos de vías"
ROAD_TYPES.each do |name, code|
  RoadType.create!(name: name, code: code)
end

def alter_sequence(sequence_name, sequence_number)
  ActiveRecord::Base.connection.execute("ALTER SEQUENCE #{sequence_name} RESTART WITH #{sequence_number}")
end

puts "Creando Direcciones"
Address.create!(id: 50, road_type: "ACCESO", road_name: "Bajada Guadalupe Lara", road_number_type: "km", road_number: "17", grader: "U", stairs: "83", floor: "2", door: "4", postal_code: "49518", borough: nil, district: nil, town: "Madrid", province: "ALBACETE", country: "España", ndp_code: nil, local_code: nil, province_code: nil, town_code: nil, district_code: nil, class_name: nil, latitude: nil, longitude: nil, normalize: false, created_at: "2018-02-12 15:19:49", updated_at: "2018-02-12 15:19:51", notes: nil, comments: nil, xetrs89: nil, yetrs89: nil)
# Address.create!(id: 2, road_type: "CARRETERA", road_name: "Rambla Rocio Alejandro", road_number_type: "km", road_number: "12", grader: "I", stairs: "80", floor: "7", door: "9", postal_code: "24177", borough: nil, district: "OTROS", town: "Madrid", province: "NAVARRA", country: "España", ndp_code: nil, local_code: nil, province_code: nil, town_code: nil, district_code: "99", class_name: nil, latitude: nil, longitude: nil, normalize: false, created_at: "2018-02-12 15:19:49", updated_at: "2018-04-19 09:49:04", notes: nil, comments: nil, xetrs89: nil, yetrs89: nil)
# Address.create!(id: 3, road_type: "PASADIZO", road_name: "Edificio Ramón", road_number_type: "km", road_number: "80", grader: "D", stairs: "16", floor: "7", door: "1", postal_code: "92546", borough: nil, district: nil, town: "Madrid", province: "BARCELONA", country: "España", ndp_code: nil, local_code: nil, province_code: nil, town_code: nil, district_code: nil, class_name: nil, latitude: nil, longitude: nil, normalize: false, created_at: "2018-02-12 15:19:49", updated_at: "2018-02-12 15:19:51", notes: nil, comments: nil, xetrs89: nil, yetrs89: nil)

puts "Creando Entidades"
ActiveRecord::Base.connection.execute("INSERT INTO entities VALUES (50, 'Entidad 1', null, 'Z8383769K', 'chester@baucheichmann.io', 'Sta. Nicolás Dávila Tovar', 'Márquez', null, 'Sta. Pilar Márquez Terán', 'Corona',null, null, null, true,false, null, 4, null, null, 50,  true, null, null, '2018-02-12 15:19:49', '2018-02-12 15:19:49')")
# Entity.create!(id: 2, name: "Entidad 2", description: nil, vat_number: "38741046F", email: "reynold.stehr@feest.info", representative_name: "Sonia Ocasio Benítez", representative_last_name: "Bernal", representative_last_name_alt: nil, contact_name: "Lorena Cotto Montaño", contact_last_name: "Urrutia", contact_last_name_alt: nil, phone_number: nil, phone_number_alt: nil, publish_pictures: true, annual_survey: false, req_reason_id: nil, entity_type_id: 3, comments: nil, other_subscribe_reason: nil, address_id: 3, active: true, subscribed_at: nil, unsubscribed_at: nil, created_at: "2018-02-12 15:19:49", updated_at: "2018-02-12 15:19:49")
# Entity.create!(id: 3, name: "Example", description: nil, vat_number: "S1819117A", email: "example@example.es", representative_name: "yo", representative_last_name: "yo mismo", representative_last_name_alt: "", contact_name: "yo", contact_last_name: "yo mismo", contact_last_name_alt: "", phone_number: "618844036", phone_number_alt: "", publish_pictures: true, annual_survey: false, req_reason_id: 2, entity_type_id: 4, comments: nil, other_subscribe_reason: "", address_id: 3, active: true, subscribed_at: nil, unsubscribed_at: nil, created_at: "2018-05-15 13:58:18", updated_at: "2018-05-15 13:58:18")


puts "Creando Proyectos"
Project.create!(id: 1, name: "Stronghold Distrito 1", active: true, description: "Vel hic est fuga qui dolorem.", functions: "Modi et nulla et delectus voluptas est praesentium...", execution_start_date: "2018-02-02", execution_end_date: "2018-02-23", contact_name: "Sr. Mario Mota Torres",district: "CENTRO", contact_last_name: "Manzanares", contact_last_name_alt: nil, phone_number: "949-502-090", phone_number_alt: nil, email: "mary@markseffertz.com", participants_num: nil, beneficiaries_num: 10, volunteers_num: 2, volunteers_allowed: true, publish: true, outstanding: false, insurance_date: nil, comments: "Excepturi sunt porro non quo.", insured: false, project_type_id: 1, pt_extendable_id: 1, pt_extendable_type: "Pt::Social", entity_id: 50, created_at: "2018-02-12 15:19:51", updated_at: "2018-02-12 15:19:51", urgent: false, contact_name1: nil, contact_last_name1: nil, contact_last_name_alt1: nil, phone_number1: nil, phone_number_alt1: nil, contact_name2: nil, contact_last_name2: nil, contact_last_name_alt2: nil, phone_number2: nil, phone_number_alt2: nil, email1: nil, email2: nil, area_departament: nil, number_of_activities: nil, number_of_actions: nil, year: nil, review_end_project: false)
# Project.create!(id: 2, name: "Voltsillam Distrito 2", active: true, description: "Deleniti consectetur enim tenetur quis sit.", functions: "Autem in consequuntur consequatur voluptatem liber...", execution_start_date: "2018-02-07", execution_end_date: "2018-02-16", contact_name: "Sr. César Moreno Flores",district: "CENTRO", contact_last_name: "Contreras", contact_last_name_alt: nil, phone_number: "983.262.079", phone_number_alt: nil, email: "zack_zieme@von.org", participants_num: nil, beneficiaries_num: 10, volunteers_num: 33, volunteers_allowed: true, publish: true, outstanding: false, insurance_date: nil, comments: "Accusamus autem et nihil minus nam.", insured: false, project_type_id: 6, pt_extendable_id: 1, pt_extendable_type: "Pt::Subvention", entity_id: 2, created_at: "2018-02-12 15:19:51", updated_at: "2018-02-12 15:19:51", urgent: false, contact_name1: nil, contact_last_name1: nil, contact_last_name_alt1: nil, phone_number1: nil, phone_number_alt1: nil, contact_name2: nil, contact_last_name2: nil, contact_last_name_alt2: nil, phone_number2: nil, phone_number_alt2: nil, email1: nil, email2: nil, area_departament: nil, number_of_activities: nil, number_of_actions: nil, year: nil, review_end_project: false)
# Project.create!(id: 3, name: "Bamity Distrito 3", active: true, description: "Quod adipisci voluptas est ut officia.", functions: "Asperiores impedit quis consequuntur quasi veniam.", execution_start_date: "2018-02-07", execution_end_date: "2018-02-17", contact_name: "Sta. Gregorio Archuleta Marroquín",district: "CENTRO", contact_last_name: "Nieves", contact_last_name_alt: nil, phone_number: "983851940", phone_number_alt: nil, email: "garfield@vonhills.org", participants_num: nil, beneficiaries_num: 10, volunteers_num: 17, volunteers_allowed: true, publish: true, outstanding: false, insurance_date: nil, comments: "Earum delectus voluptatibus totam voluptas aliquam...", insured: false, project_type_id: 4, pt_extendable_id: 1, pt_extendable_type: "Pt::Punctual", entity_id: 1, created_at: "2018-02-12 15:19:51", updated_at: "2018-02-12 15:19:51", urgent: false, contact_name1: nil, contact_last_name1: nil, contact_last_name_alt1: nil, phone_number1: nil, phone_number_alt1: nil, contact_name2: nil, contact_last_name2: nil, contact_last_name_alt2: nil, phone_number2: nil, phone_number_alt2: nil, email1: nil, email2: nil, area_departament: nil, number_of_activities: nil, number_of_actions: nil, year: nil, review_end_project: false)
# Project.create!(id: 4, name: "Rank Distrito 4", active: true, description: "Nemo aut nesciunt veritatis repudiandae beatae rer...", functions: "Voluptatum sed unde deleniti similique voluptas.", execution_start_date: "2018-02-11", execution_end_date: "2018-02-15", contact_name: "Diana Briones Verduzco",district: "CENTRO", contact_last_name: "Rivero", contact_last_name_alt: nil, phone_number: "987.999.117", phone_number_alt: nil, email: "carmen.runte@brekke.io", participants_num: nil, beneficiaries_num: 10, volunteers_num: 94, volunteers_allowed: true, publish: true, outstanding: false, insurance_date: nil, comments: "In aspernatur tenetur earum.", insured: false, project_type_id: 3, pt_extendable_id: 1, pt_extendable_type: "Pt::Permanent", entity_id: 2, created_at: "2018-02-12 15:19:51", updated_at: "2018-02-12 15:19:51", urgent: false, contact_name1: nil, contact_last_name1: nil, contact_last_name_alt1: nil, phone_number1: nil, phone_number_alt1: nil, contact_name2: nil, contact_last_name2: nil, contact_last_name_alt2: nil, phone_number2: nil, phone_number_alt2: nil, email1: nil, email2: nil, area_departament: nil, number_of_activities: nil, number_of_actions: nil, year: nil, review_end_project: false)
# Project.create!(id: 5, name: "Prodder Distrito 5", active: true, description: "Porro perspiciatis reiciendis possimus ducimus sin...", functions: "Excepturi quas quos qui et accusamus dolorem modi.", execution_start_date: "2018-02-03", execution_end_date: "2018-02-19", contact_name: "Rebeca Contreras Solano",district: "CENTRO", contact_last_name: "Chacón", contact_last_name_alt: nil, phone_number: "972 984 369", phone_number_alt: nil, email: "mike.hermann@morar.com", participants_num: nil, beneficiaries_num: 10, volunteers_num: 84, volunteers_allowed: true, publish: true, outstanding: false, insurance_date: nil, comments: "Perspiciatis quae non officiis rerum.", insured: false, project_type_id: 7, pt_extendable_id: 1, pt_extendable_type: "Pt::Other", entity_id: 2, created_at: "2018-02-12 15:19:51", updated_at: "2018-02-12 15:19:51", urgent: false, contact_name1: nil, contact_last_name1: nil, contact_last_name_alt1: nil, phone_number1: nil, phone_number_alt1: nil, contact_name2: nil, contact_last_name2: nil, contact_last_name_alt2: nil, phone_number2: nil, phone_number_alt2: nil, email1: nil, email2: nil, area_departament: nil, number_of_activities: nil, number_of_actions: nil, year: nil, review_end_project: false)
# Project.create!(id: 6, name: "Rank Distrito 4", active: true, description: "Nemo aut nesciunt veritatis repudiandae beatae rer...", functions: "Voluptatum sed unde deleniti similique voluptas.", execution_start_date: "2018-02-11", execution_end_date: "2018-02-15", contact_name: "Diana Briones Verduzco",district: "CENTRO", contact_last_name: "Rivero", contact_last_name_alt: nil, phone_number: "987.999.117", phone_number_alt: nil, email: "carmen.runtcce@brekke.io", participants_num: nil, beneficiaries_num: 10, volunteers_num: 94, volunteers_allowed: true, publish: true, outstanding: false, insurance_date: nil, comments: "In aspernatur tenetur earum.", insured: false, project_type_id: 2, pt_extendable_id: 1, pt_extendable_type: "Pt::Centre", entity_id: 2, created_at: "2018-02-12 15:19:51", updated_at: "2018-02-12 15:19:51", urgent: false, contact_name1: nil, contact_last_name1: nil, contact_last_name_alt1: nil, phone_number1: nil, phone_number_alt1: nil, contact_name2: nil, contact_last_name2: nil, contact_last_name_alt2: nil, phone_number2: nil, phone_number_alt2: nil, email1: nil, email2: nil, area_departament: nil, number_of_activities: nil, number_of_actions: nil, year: nil, review_end_project: false)
# Project.create!(id: 7, name: "Prodder Distrito 5", active: true, description: "Porro perspiciatis reiciendis possimus ducimus sin...", functions: "Excepturi quas quos qui et accusamus dolorem modi.", execution_start_date: "2018-02-03", execution_end_date: "2018-02-19", contact_name: "Rebeca Contreras Solano",district: "CENTRO", contact_last_name: "Chacón", contact_last_name_alt: nil, phone_number: "972 984 369", phone_number_alt: nil, email: "mike.hermanner@morar.com", participants_num: nil, beneficiaries_num: 10, volunteers_num: 84, volunteers_allowed: true, publish: true, outstanding: false, insurance_date: nil, comments: "Perspiciatis quae non officiis rerum.", insured: false, project_type_id: 5, pt_extendable_id: 1, pt_extendable_type: "Pt::Entity", entity_id: 2, created_at: "2018-02-12 15:19:51", updated_at: "2018-02-12 15:19:51", urgent: false, contact_name1: nil, contact_last_name1: nil, contact_last_name_alt1: nil, phone_number1: nil, phone_number_alt1: nil, contact_name2: nil, contact_last_name2: nil, contact_last_name_alt2: nil, phone_number2: nil, phone_number_alt2: nil, email1: nil, email2: nil, area_departament: nil, number_of_activities: nil, number_of_actions: nil, year: nil, review_end_project: false)
