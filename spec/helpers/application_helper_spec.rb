require "rails_helper"

RSpec.describe ApplicationHelper, :type => :helper do
#   describe "#home_page?" do
#     it "returns home" do
#       expect(helper.home_page?).to eq(true)
#     end
#   end

#   describe "#current_path_with_query_params" do
#     it "query params" do
#       expect(helper.current_path_with_query_params({})).to eq({})
#     end
#   end


#   describe "#markdown" do
#     it "returns markdown" do
#       expect(helper.markdown("Text")).to eq("Text")
#     end
#   end

  describe "#districts_select_options" do
    it "returns districts" do
      expect(helper.districts_select_options).not_to eq("")
    end
  end

  describe "#projects_select_options" do
    it "returns projects" do
      expect(helper.projects_select_options).not_to eq("")
    end
  end

  describe "#entity_projects_select_options(id)" do
    it "returns projects of entity" do
        expect(helper.entity_projects_select_options(50)).not_to eq("")
    end
  end
end