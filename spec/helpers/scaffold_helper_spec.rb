require "rails_helper"

RSpec.describe ScaffoldHelper, :type => :helper do
  describe "#model_title" do
    it "returns the model title" do
      expect(helper.model_title(Project)).to eq("<h1 class=\"titlePage\" alt=\"Proyectos\">Proyectos</h1>")
    end
  end

  describe "#main_title" do
    it "returns the model title" do
      expect(helper.main_title("Voluntariado")).to eq("<h1 class=\"titlePage\" alt=\"Voluntariado\">Voluntariado</h1>")
    end
  end


  describe "#get_hidden_fields" do
    it "returns get hidden fields" do
      expect(helper.get_hidden_fields({})).to eq("")
    end
  end

  describe "#show_simple_date" do
    it "returns date" do
      expect(helper.show_simple_date(Date.today)).not_to eq("")
    end
  end

  describe "#show_attr" do
    it "returns date" do
      expect(helper.show_attr(Date.today)).not_to eq("")
    end

    it "returns string" do
        expect(helper.show_attr("Hola")).not_to eq("")
    end

    it "returns project name" do
        expect(helper.show_attr(Project.find(1).name)).not_to eq("")
    end
  end

  describe "#button_to_back" do
    it "returns back" do
        expect(helper.button_to_back).not_to eq("")
    end
  end
end