require "rails_helper"

RSpec.describe ApplicationHelper, :type => :helper do

  describe "events_ajax_previous_link" do
    it "returns link" do
      expect(helper.events_ajax_previous_link).not_to eq(true)
    end
  end

  describe "events_ajax_next_link" do
    it "returns link" do
      expect(helper.events_ajax_next_link).not_to eq(true)
    end
  end

end