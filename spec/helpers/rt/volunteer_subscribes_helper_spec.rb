require "rails_helper"

RSpec.describe Rt::VolunteerSubscribesHelper, :type => :helper do
    describe "create" do
        it "returns projects" do
            attr_p = Project.find(1)
          expect(helper.project_volunteer_create(attr_p)).to eq(true)
        end
    end
end