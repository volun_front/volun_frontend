require 'rails_helper'

RSpec.describe Province, type: :model do
  let(:province) { build(:province) }

  it 'is valid2' do
    expect(province).to be_valid
  end

  it 'method to_s' do
    expect(province.to_s).to eq(province.name)
  end
end
