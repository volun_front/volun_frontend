require 'rails_helper'

RSpec.describe ShiftDefinition, type: :model do
  let(:shift_definition) { create(:shift_definition) }

  it 'is valid' do
    expect(shift_definition).to be_valid
  end

  it 'recordable update' do
    expect(shift_definition.update_history(nil)).to eq(nil)
  end

  it 'to_s' do
    expect(shift_definition.to_s).not_to eq(nil)
  end

  it 'main_columns' do
    expect(ShiftDefinition.main_columns).not_to eq(nil)
  end
end