require 'rails_helper'

RSpec.describe Area, type: :model do
  let(:area) { build(:area) }

  it 'is valid2' do
    expect(area).to be_valid
  end

  it 'method to_s' do
    expect(area.to_s).to eq(area.name)
  end

 
end
