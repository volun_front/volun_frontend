require 'rails_helper'

RSpec.describe Rt::EntitySubscribe, type: :model do
  let(:entity_subscribe) { build(:entity_subscribe) }

  it 'is valid' do
    expect(entity_subscribe).to be_valid
  end

  it 'method to_s' do
    expect(entity_subscribe.to_s).to eq(entity_subscribe.name)
  end

  it 'set data name is blank' do
    entity_subscribe.name = ""
    expect(entity_subscribe.set_data).to eq("<h4 style = 'margin-top: 10px'>Solicitud de alta de entidad</h4>")
  end
end
