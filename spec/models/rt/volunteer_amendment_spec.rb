require 'rails_helper'

RSpec.describe Rt::VolunteerAmendment, type: :model do
  let(:volunteer_amendment) { build(:volunteer_amendment) }

  it 'set_data' do
    expect(volunteer_amendment.set_data).not_to eq(nil)
  end

  it 'email_duplicated' do
    volunteer_amendment.email = "prueba@prueba.es"
    volunteer_amendment.save
    expect(volunteer_amendment.email_duplicated).to eq(nil)
  end

  it 'data_mod' do
    expect(volunteer_amendment.data_mod).not_to eq(nil)
  end
end
