require 'rails_helper'

RSpec.describe Rt::ActivityUnpublishing, type: :model do
  let(:activity_unpublishing) { build(:activity_unpublishing) }

  it 'is valid' do
    expect(activity_unpublishing).to be_valid
  end

  it 'set_data' do
    expect(activity_unpublishing.set_data).to eq("<h4 style = 'margin-top: 10px'><b>Observaciones:</b> #{activity_unpublishing.notes}</h4>")
  end
end
