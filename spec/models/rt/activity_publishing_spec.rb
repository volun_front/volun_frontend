require 'rails_helper'

RSpec.describe Rt::ActivityPublishing, type: :model do
  let(:activity_publishing) { build(:activity_publishing) }

  it 'is valid' do
    expect(activity_publishing).to be_valid
  end

  it 'method to_s' do
    expect(activity_publishing.to_s).to eq(activity_publishing.name)
  end

  it 'set data name is blank' do
    activity_publishing.name = ""
    expect(activity_publishing.set_data).to eq("<h4 style = 'margin-top: 10px'><b>Organizadores:</b> #{activity_publishing.organizer}</h4>")
  end

  it 'set data organizer is blank' do
    activity_publishing.organizer = ""
    expect(activity_publishing.set_data).to eq("<h4 style = 'margin-top: 10px'><b>Nombre:</b> #{activity_publishing.name}</h4>")
  end

  it 'set name and organizer is different to blank ' do
    expect(activity_publishing.set_data).to eq("<h4 style = 'margin-top: 10px'><b>Nombre:</b> #{activity_publishing.name}</h4>\n    \t\t<h4 style = 'margin-top: 10px'><b>Organizadores:</b> #{activity_publishing.organizer}</h4>")
  end
end
