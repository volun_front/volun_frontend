require 'rails_helper'

RSpec.describe Rt::VolunteerUnsubscribe, type: :model do
  let(:volunteer_unsubscribe) { build(:volunteer_unsubscribe) }

  it 'is valid' do
    expect(volunteer_unsubscribe).to be_valid
  end
  it 'set data if project id exist' do
    volunteer_unsubscribe.project_id = ""
    expect(volunteer_unsubscribe.set_data).to eq("<h4 style = 'margin-top: 10px'>Solicitud de baja como voluntario</h4>")
  end
end
