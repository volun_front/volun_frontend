require 'rails_helper'

RSpec.describe Rt::Other, type: :model do
  let(:other) { build(:other) }

  it 'is valid' do
    expect(other).to be_valid
  end

  it 'set data if description exist' do
    other.description = "Hola mundo"
    expect(other.set_data).to eq("<h4 style = 'margin-top: 10px'>#{other.description}</h4>")
    other.description = nil
    expect(other.set_data).not_to eq(nil)
    
  end

  it 'set data if description not exist' do
    other.description = ""
    if other.description.blank?
      expect(other.set_data).to eq("<h4 style = 'margin-top: 10px'>No existe información relativa a esta solicitud</h4>")
    end
  end
end
