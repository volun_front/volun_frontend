require 'rails_helper'

RSpec.describe Rt::VolunteerAppointment, type: :model do
  let(:volunteer_appointment) { build(:volunteer_appointment) }

  it 'is valid' do
    expect(volunteer_appointment).to be_valid
  end

  describe "set data" do
    it "notes" do
      expect(volunteer_appointment.set_data).to eq("<h4 style = 'margin-top: 10px'>#{volunteer_appointment.notes.to_s}</h4>")
    end
    
    it "notes blank" do
      volunteer_appointment.notes = ""
      expect(volunteer_appointment.set_data).to eq("<h4 style = 'margin-top: 10px'>No hay notas referentes a esta solicitud</h4>")
    end
  end
end
