require 'rails_helper'

RSpec.describe Rt::VolunteersDemand, type: :model do
  let(:volunteers_demand) { build(:volunteers_demand) }

  it 'is valid' do
    expect(volunteers_demand).to be_valid
  end

  it 'set data notes is blank' do
    volunteers_demand.notes = ""
    expect(volunteers_demand.set_data).to eq("<h4 style = 'margin-top: 10px'><b>Número de voluntarios solicitado:</b> #{volunteers_demand.requested_volunteers_num}</h4>")
  end

  it 'set data organizer is blank' do
    volunteers_demand.requested_volunteers_num = ""
    expect(volunteers_demand.set_data).to eq("<h4 style = 'margin-top: 10px'><b>Proyecto:</b> #{volunteers_demand.notes}</h4>")
  end

  it 'set notes and requested volunteers num is different to blank ' do
    expect(volunteers_demand.set_data).to eq("<h4 style = 'margin-top: 10px'><b>Proyecto:</b> #{volunteers_demand.notes}</h4>\n    \t\t<h4 style = 'margin-top: 10px'><b>Número de voluntarios solicitado:</b> #{volunteers_demand.requested_volunteers_num}</h4>")
  end
end
