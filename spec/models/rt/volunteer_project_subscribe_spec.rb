require 'rails_helper'

RSpec.describe Rt::VolunteerProjectSubscribe, type: :model do
  let(:volunteer_project_subscribe) { build(:volunteer_project_subscribe) }

  it 'is valid' do
    expect(volunteer_project_subscribe).not_to be_valid
  end

  it "should not be valid without an email" do
    volunteer_project_subscribe.email=nil
    expect(volunteer_project_subscribe).to_not be_valid
  end

  it "set_terms_of_service" do
    expect(volunteer_project_subscribe.set_terms_of_service("Hello world")).to_not eq(nil)
  end

  it "should not be valid without an email valid" do
    volunteer_project_subscribe.email="perezljl"
    expect(volunteer_project_subscribe).to_not be_valid
  end

  it "should not be valid without an last_name" do
    volunteer_project_subscribe.last_name=nil
    expect(volunteer_project_subscribe).to_not be_valid
  end

  it "should not be valid without an name" do
    volunteer_project_subscribe.name=nil
    expect(volunteer_project_subscribe).to_not be_valid
  end

  it 'method to_s' do
    expect(volunteer_project_subscribe.to_s).to eq(volunteer_project_subscribe.name)
  end

  it 'set data if project id exist' do
    if !Project.where(id: volunteer_project_subscribe.project_id)[0].blank?
      expect(volunteer_project_subscribe.set_data).to eq("<h4 style = 'margin-top: 10px'>Solicitud de alta en el proyecto #{Project.where(id: volunteer_project_subscribe.project_id)[0].name.to_s}</h4>")
    end
  end

  it 'set data if project id not exist' do
    volunteer_project_subscribe.project_id = 0
    if Project.where(id: volunteer_project_subscribe.project_id)[0].blank?
      expect(volunteer_project_subscribe.set_data).to eq("<h4 style = 'margin-top: 10px'>Solicitud de alta en proyecto</h4>")
    end
  end
end