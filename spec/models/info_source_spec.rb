require 'rails_helper'

RSpec.describe InfoSource, type: :model do
  let(:info_source) { build(:info_source) }

  it 'is valid' do
    expect(info_source).to be_valid
  end

  it 'to_s' do
    expect(info_source.to_s).not_to eq(nil)
  end


end
