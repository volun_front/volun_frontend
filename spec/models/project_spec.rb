require 'rails_helper'

RSpec.describe Project, type: :model do
  let(:project) { build(:project) }

  it 'is valid2' do
    expect(project).to be_valid
  end

  it 'select_icon_by_project_type' do
    expect(project.select_icon_by_project_type).not_to eq(nil)
    project.project_type = ProjectType.find(3)
    expect(project.select_icon_by_project_type).not_to eq(nil)
    project.project_type = ProjectType.find(4)
    expect(project.select_icon_by_project_type).not_to eq(nil)
  end

  it 'with ransack default' do
    expect(Project.ransack_default).to eq({:s=>"projects.id desc"})
  end
end
