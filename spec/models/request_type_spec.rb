require 'rails_helper'

RSpec.describe RequestType, type: :model do
  let(:request_type) { build(:request_type) }

  it 'is valid2' do
    expect(request_type).to be_valid
  end

  it 'method class active' do
    expect(RequestType.active?("rt_solidarity_chain")).not_to eq(true)
  end

  it 'method class get_request_form_type' do
    expect(RequestType.get_request_form_type("rt_solidarity_chain")).not_to eq(true)
  end
  
  it 'method class extendable' do
    expect(request_type.extendable?).to eq(true)
  end
end
