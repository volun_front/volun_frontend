require 'rails_helper'

RSpec.describe ProjectType, type: :model do
  let(:project_type) { build(:project_type) }

  it 'is valid2' do
    expect(project_type).to be_valid
  end

  it 'method class active' do
    expect(ProjectType.active?("pt_social")).not_to eq(true)
  end

  it 'method class active' do
    expect(project_type.extendable?).not_to eq(true)
  end

  
end
