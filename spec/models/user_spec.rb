require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) { build(:user) }

  it 'method class find_for_database_authentication' do
    expect(User.find_for_database_authentication({username: User.all.first, password: "12345678"})).to eq(nil)
  end

end
