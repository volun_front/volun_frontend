require 'rails_helper'

RSpec.describe Meeting, type: :model do
 

  it 'is valid' do
    expect(Meeting.new(name: "MyString")).to be_valid
  end

  it 'to_s' do
    expect(Meeting.new(name: "MyString").to_s).not_to eq(nil)
  end

  it 'main_columns' do
    expect(Meeting.main_columns).not_to eq(nil)
  end


end
