require 'rails_helper'

RSpec.describe LinkType, type: :model do
  let(:link_type) { build(:link_type) }

  # it 'is valid2' do
  #   expect(link_type).to be_valid
  # end

  # it 'method to_s' do
  #   expect(link_type.to_s).not_to eq(nil)
  # end

  it 'method file_kinds' do
    expect(LinkType.file_kinds).not_to eq(nil)
  end

  # it 'method file_kinds_i18n' do
  #   expect(LinkType.file_kinds_i18n).not_to eq(nil)
  # end
  it 'method visible_kinds' do
    expect(LinkType.visible_kinds).not_to eq(nil)
  end
  # it 'method visible_kinds_i18n' do
  #   expect(LinkType.visible_kinds_i18n).not_to eq(nil)
  # end
  it 'method logo_kind' do
    expect(LinkType.logo_kind).not_to eq(nil)
  end
end
