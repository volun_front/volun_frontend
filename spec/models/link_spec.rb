require 'rails_helper'

RSpec.describe Link, type: :model do
  let(:link) { build(:link) }

  it 'project_images' do
    expect(Link.project_images).not_to eq(nil)
  end

  it 'project_videos' do
    expect(Link.project_videos).not_to eq(nil)
  end

  it 'project_docs' do
    expect(Link.project_docs).not_to eq(nil)
  end

  it 'project_urls' do
    expect(Link.project_urls).not_to eq(nil)
  end

  it 'project_logo' do
    expect(Link.project_logo).not_to eq(nil)
  end

  it 'volunteer_images' do
    expect(Link.volunteer_images).not_to eq(nil)
  end

  it 'volunteer_docs' do
    expect(Link.volunteer_docs).not_to eq(nil)
  end

  it 'volunteer_urls' do
    expect(Link.volunteer_urls).not_to eq(nil)
  end

  it 'volunteer_logo' do
    expect(Link.volunteer_logo).not_to eq(nil)
  end

  it 'entity_images' do
    expect(Link.entity_images).not_to eq(nil)
  end

  it 'entity_docs' do
    expect(Link.entity_docs).not_to eq(nil)
  end

  it 'entity_urls' do
    expect(Link.entity_urls).not_to eq(nil)
  end

  it 'entity_logo' do
    expect(Link.entity_logo).not_to eq(nil)
  end

  it 'activity_images' do
    expect(Link.activity_images).not_to eq(nil)
  end

  it 'activity_videos' do
    expect(Link.activity_videos).not_to eq(nil)
  end

  it 'activity_docs' do
    expect(Link.activity_docs).not_to eq(nil)
  end

  it 'activity_urls' do
    expect(Link.activity_urls).not_to eq(nil)
  end

  it 'activity_logo' do
    expect(Link.activity_logo).not_to eq(nil)
  end

  it 'new_campaing_images' do
    expect(Link.new_campaing_images).not_to eq(nil)
  end

  it 'new_campaing_videos' do
    expect(Link.new_campaing_videos).not_to eq(nil)
  end

  it 'album_images' do
    expect(Link.album_images).not_to eq(nil)
  end

  it 'album_videos' do
    expect(Link.album_videos).not_to eq(nil)
  end

  it 'main_columns' do
    expect(Link.main_columns).not_to eq(nil)
  end

  it 'file_extension' do
    expect(link.file_extension).not_to eq(nil)
  end

  it 'file_base_name' do
    expect(link.file_base_name).not_to eq(nil)
  end

  it 'file_name' do
    expect(link.file_name).not_to eq(nil)
  end
end