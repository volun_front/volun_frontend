require 'rails_helper'

RSpec.describe NewCampaing, type: :model do
  it 'main_column' do
    expect(NewCampaing.main_column).not_to eq(nil)
  end

  it 'campaing_attributes' do
    expect(NewCampaing.new.campaing_attributes).not_to eq(nil)
  end
end
