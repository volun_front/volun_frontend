require 'rails_helper'

RSpec.describe Activity, type: :model do
  let(:activity) { build(:activity) }

  it 'is valid2' do
    expect(activity).to be_valid
  end

  it 'with ransack default' do
    expect(Activity.ransack_default).to eq({:s=>"id desc"})
  end

  it 'method to_s' do
    expect(activity.to_s).to eq(activity.name)
  end
end
