require 'rails_helper'

RSpec.describe Criptografia, type: :service do
    it 'test' do
        cp = Criptografia.new

        encode = cp.encrypt("hello","key_hello")
        expect(encode).not_to eq(nil)

        decode = cp.decrypt("hello",encode)

        expect(decode).not_to eq(nil)
    end
    
end