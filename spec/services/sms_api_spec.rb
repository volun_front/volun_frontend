require 'rails_helper'

RSpec.describe SMSApi, type: :service do
    it 'test' do
        sms = SMSApi.new

        
        expect(sms.url).not_to eq(nil)
        expect(sms.authorization).not_to eq(nil)
        expect(sms.request('618545454','hola mundo')).not_to eq(nil)
        expect(sms.end_point_available?).not_to eq(nil)
        expect(sms.stubbed_response).not_to eq(nil)

    end

    it 'sms_deliver' do
        sms = SMSApi.new
        expect(sms.sms_deliver('618545454','hola mundo')).not_to eq(nil)
    end

    it 'success?(response)' do
        sms = SMSApi.new
        obj = Object.new
        class << obj
          def body
            {enviar_sms_simples_response: {enviar_sms_simples_return: {respuesta_se: {texto_respuesta: 'Sucess'}}}}
          end
        end
        expect(sms.success?(obj)).not_to eq(nil)
    end
    
end