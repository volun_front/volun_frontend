require 'rails_helper'

RSpec.describe VolunteersController, type: :controller do
  let(:user) { create(:user, :user_volunteer) }
  before(:each) do
    sign_in user
  end
  let(:valid_attributes) {
    FactoryGirl.attributes_for(:volunteer).merge(address_attributes: FactoryGirl.attributes_for(:address)).merge( user_attributes: user.to_param)
  }


  describe "GET #index" do
    it 'assigns a volunteer as @volunteers' do
     
      get :index
      expect(assigns(:volunteers).count).not_to eq(0)
    end
  end

 
end
