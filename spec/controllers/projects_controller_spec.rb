require 'rails_helper'

RSpec.describe ProjectsController, type: :controller do
  let(:user) { create(:user, :user_entity) }
  before(:each) do
    sign_in user
  end
  let(:valid_attributes) {
    FactoryGirl.attributes_for(:project).merge(address_attributes: FactoryGirl.attributes_for(:address)).merge( user_attributes: user.to_param)
  }
  

  describe "GET #index" do
    it 'assigns a project as @projects' do
     
      get :index
      expect(assigns(:projects_actives).count).not_to eq(0)
    end
  end

  describe "GET #index" do
    it 'assigns a project as @projects with areas' do
      params = {extend_search: '1',q: {areas_name_in: 'Acción Sanitaria', timetables_execution_date_eq: '2018-12-12'}}
      get :index, params

      expect(assigns(:projects_actives).count).to eq(0)
    end

    it 'assigns a project as @projects without params' do
      params = {extend_search: '1',q: {}}
      get :index, params

      expect(assigns(:projects_actives).count).to eq(0)
    end
  end

  describe "GET #boroughs" do
    it 'assigns a project as @projects' do
     
      get :boroughs, format: :json
      expect(assigns(:boroughs).count).to eq(0)
    end
  end

  describe "GET #show" do
    it 'assign a project as @project' do
      project=Project.find(1)
      get :show,id: project.id
      expect(assigns(:project)).to eq(project)
    end
  end

  describe "GET #my_area" do
    it 'assigns a project as @projects' do
     
      get :my_area, format: :js
      expect(assigns(:projects_actives).count).not_to eq(0)
    end
  end
end
