require 'rails_helper'

RSpec.describe NewCampaingsController, type: :controller do
  before(:each) do
    sign_in create(:user)
  end

  let(:valid_attributes) {
    attributes_for :new_campaing
  }

  let(:invalid_attributes) {
    attributes_for :new_campaing, :invalid
  }

  describe "GET #index" do
    it 'status 200' do
    
      get :index
      expect(response.status).to eq(200)
    end
  end

  describe "GET #show" do
    it 'status 200' do
    
      get :show
      expect(response.status).not_to eq(200)
    end
  end

end
