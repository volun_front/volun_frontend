require 'rails_helper'

RSpec.describe ActivitiesController, type: :controller do
  let(:user) { create(:user, :user_entity) }
  before(:each) do
    sign_in user
  end
  let(:valid_attributes) {
    FactoryGirl.attributes_for(:activity)
  }


  describe "GET #index" do
    it 'assigns a activity as @activities with day' do
      activity=Activity.create! valid_attributes
      get :index, day: 1
      expect(assigns(:timetables).count).not_to eq(0)
    end

    it 'assigns a activity as @activities' do
      activity=Activity.create! valid_attributes
      get :index
      expect(assigns(:timetables).count).not_to eq(0)
    end
  end

  describe "GET #boroughs" do
    it 'assigns a activity as @activities' do
     
      get :boroughs, format: :json
      expect(assigns(:boroughs).count).to eq(0)
    end
  end

  describe "GET #show" do
    it 'assign a activity as @activity' do
      activity=Activity.create! valid_attributes
      get :show,id: activity.id
      expect(assigns(:activity)).to eq(activity)
    end
  end

  describe "GET #search" do
    it 'assigns a activity as @activities with day' do
      activity=Activity.create! valid_attributes
      get :search,day: 1, format: :js
      expect(assigns(:activities).count).not_to eq(0)
    end

    it 'assigns a activity as @activities' do
      activity=Activity.create! valid_attributes
      get :search, format: :js
      expect(assigns(:activities).count).not_to eq(0)
    end
  end

  describe "GET #my_area" do
    it 'assigns a activity as @activities' do
      activity=Activity.create! valid_attributes
      get :my_area, format: :js
      expect(assigns(:activities).count).not_to eq(0)
    end
  end

 
end
