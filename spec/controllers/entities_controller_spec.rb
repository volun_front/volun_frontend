require 'rails_helper'

RSpec.describe EntitiesController, type: :controller do

  let(:valid_attributes) {
    FactoryGirl.attributes_for(:entity).merge(address_attributes: FactoryGirl.attributes_for(:address)).merge( user_attributes: FactoryGirl.attributes_for(:user))
  }

  let(:invalid_attributes) {
    attributes_for(:entity, :invalid).merge(address_attributes: FactoryGirl.attributes_for(:address)).merge( user_attributes: FactoryGirl.attributes_for(:user))
  }

  describe "GET #new" do
    it 'assigns a new entity as @entity' do
     
      get :new
      expect(assigns(:entity)).to be_a_new(Entity)
    end
  end
  
  describe "POST #create" do
    context 'with valid params' do
      it 'creates a new Entity with vat number' do
        expect {
          valid_attributes[:vat_number]="Z8383769K"
          post :create, entity: valid_attributes
        }.to change(Entity, :count).by(0)
      end

      it 'creates a new Entity' do
        expect {
          post :create, entity: valid_attributes
        }.to change(Entity, :count).by(1)
      end

      it 'assigns a newly created entity as @entity' do
        valid_attributes[:vat_number]="Z8383769K"
        post :create, entity: valid_attributes
        expect(assigns(:entity)).to be_a(Entity)
        expect(assigns(:entity)).to be_persisted
      end

      it 'redirects to the entities_path' do
        valid_attributes[:vat_number]="Z8383769K"
        post :create, entity: valid_attributes
        expect(response).to redirect_to entities_path
      end
    end
    context 'with invalid params' do
      it 'falla assigns a newly created but unsaved entity as @entity' do
        post :create, entity: invalid_attributes
        expect(assigns(:entity)).to be_a_new(Entity)
      end

      it 're-renders the "new" template' do
        post :create, entity: invalid_attributes
        expect(response).to render_template('new')
      end
    end
  end

end
