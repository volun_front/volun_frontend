require 'rails_helper'

RSpec.describe Rt::ProjectPublishingsController, type: :controller do
  let(:user) { create(:user, :user_entity) }
  before(:each) do
    sign_in user
  end

  let(:valid_attributes) {
    attributes_for :project_publishing
  }

  let(:invalid_attributes) {
    attributes_for :project_publishing, :invalid
  }

  describe "GET #new" do
    it 'assigns a new rt_project_publishing as @rt_project_publishing' do
      get :new
      expect(assigns(:rt_project_publishing)).to be_a_new(Rt::ProjectPublishing)
    end
  end

  # describe "GET #load_rt_project_publishing" do
  #   it 'assigns a new rt_project_publishing as @rt_project_publishing' do
  #     get :load_rt_project_publishing
  #     expect(assigns(:rt_project_publishing)).to be_a(Rt::ProjectPublishing)
  #   end
  # end

  # describe "GET #set_project" do
  #   it 'assigns a new rt_project_publishing as @rt_project_publishing' do
  #     get :set_project,project_id: 50
  #     expect(assigns(:project)).to be_valid
  #   end
  # end

  describe "POST #create" do
    context 'with valid params' do
      # it 'falla creates a new Rt::ActivityPublishing' do
      #   expect {
      #     post :create, rt_project_publishing: valid_attributes
      #   }.to change(Rt::ProjectPublishing, :count).by(1)
      # end

      it 'assigns a newly created rt_project_publishing as @rt_project_publishing' do
        post :create, rt_project_publishing: valid_attributes
        expect(assigns(:rt_project_publishing)).to be_a(Rt::ProjectPublishing)
        expect(assigns(:rt_project_publishing)).to be_persisted
      end

      it 'falla redirects to the created rt_activity_publishing' do
        post :create, rt_project_publishing: valid_attributes
        expect(response).to redirect_to user_path(user)
      end
    end

    context 'with invalid params' do
      it 'falla assigns a newly created but unsaved rt_project_publishing as @rt_project_publishing' do
        post :create, rt_project_publishing: invalid_attributes
        expect(assigns(:rt_project_publishing)).to be_a_new(Rt::ProjectPublishing)
      end

      it 're-renders the "new" template' do
        post :create, rt_project_publishing: invalid_attributes
        expect(response).to render_template('new')
      end
    end
  end
end
