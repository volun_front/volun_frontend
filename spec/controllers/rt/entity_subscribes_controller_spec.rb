require 'rails_helper'

RSpec.describe Rt::EntitySubscribesController, type: :controller do
  let(:user) { create(:user, :user_entity) }
  before(:each) do
    sign_in user
  end

  let(:valid_attributes) {
    attributes_for :entity_subscribe
  }

  let(:invalid_attributes) {
    attributes_for :entity_subscribe, :invalid
  }



  # describe "GET #new" do
  #   it 'assigns a new rt_entity_subscribe as @rt_entity_subscribe' do
  #     get :new
  #     expect(assigns(:rt_entity_subscribe)).to be_a_new(Rt::EntitySubscribe)
  #   end
  # end

  # describe "POST #create" do
  #   context 'with valid params' do
  #     it 'creates a new Rt::EntitySubscribe' do
  #       expect {
  #         post :create, rt_entity_subscribe: valid_attributes
  #       }.to change(Rt::EntitySubscribe, :count).by(1)
  #     end

  #     it 'assigns a newly created rt_entity_subscribe as @rt_entity_subscribe' do
  #       post :create, rt_entity_subscribe: valid_attributes
  #       expect(assigns(:rt_entity_subscribe)).to be_a(Rt::EntitySubscribe)
  #       expect(assigns(:rt_entity_subscribe)).to be_persisted
  #     end

  #     it 'redirects to the created rt_entity_subscribe' do
  #       post :create, rt_entity_subscribe: valid_attributes
  #       expect(response).to redirect_to user_path(user)
  #     end
  #   end

  #   context 'with invalid params' do
  #     it 'assigns a newly created but unsaved rt_entity_subscribe as @rt_entity_subscribe' do
  #       post :create, rt_entity_subscribe: invalid_attributes
  #       expect(assigns(:rt_entity_subscribe)).to be_a_new(Rt::EntitySubscribe)
  #     end

  #     it 're-renders the "new" template' do
  #       post :create, rt_entity_subscribe: invalid_attributes
  #       expect(response).to render_template('new')
  #     end
  #   end
  # end
end
