require 'rails_helper'

RSpec.describe Rt::SolidarityChainsController, type: :controller do
  
  let(:valid_attributes) {
    attributes_for :rt_solidarity_chain
  }

  let(:invalid_attributes) {
    attributes_for :rt_solidarity_chain, :invalid
  }

  let(:invalid_attributes) {
    attributes_for :rt_solidarity_chain, :no_phone
  }

  let(:invalid_attributes) {
    attributes_for :rt_solidarity_chain, :no_mail
  }

  let(:invalid_attributes) {
    attributes_for :rt_solidarity_chain, :no_data
  }

  
  describe "GET #new" do
    it 'assigns a new rt_solidarity_chain as @rt_solidarity_chain' do
      get :new
      expect(assigns(:rt_solidarity_chain)).to be_a_new(Rt::SolidarityChain)
    end
  end

  describe "GET #show_image" do
    it 'show a image asocciation' do
      solidaryChain=Rt::SolidarityChain.create! valid_attributes
      get :show_image,id: solidaryChain.id
      expect(response.status).to eq(200)
    end
  end

  describe "GET #tarjeta" do
    it 'download solidary chain s target' do
      get :tarjeta
      expect(response).to have_http_status(200)
    end
  end

  describe "POST #create" do
    context 'with valid params' do
      it 'creates a new Rt::SolidarityChain' do
        expect {
          post :create, rt_solidarity_chain: valid_attributes
        }.to change(Rt::SolidarityChain, :count).by(1)
      end

      it 'assigns a newly created rt_solidarity_chain as @rt_solidarity_chain' do
        post :create, rt_solidarity_chain: valid_attributes
        expect(assigns(:rt_solidarity_chain)).to be_a(Rt::SolidarityChain)
        expect(assigns(:rt_solidarity_chain)).to be_persisted
      end

      it 'redirects to the new rt_solidarity_chain' do
        post :create, rt_solidarity_chain: valid_attributes
        expect(response).to redirect_to new_rt_solidarity_chain_path
      end
    end

    context 'with invalid params' do
      it 'assigns a newly created but unsaved rt_solidarity_chain as @rt_solidarity_chain' do
        post :create, rt_solidarity_chain: invalid_attributes
        expect(assigns(:rt_solidarity_chain)).to be_a_new(Rt::SolidarityChain)
      end


      it 're-renders the "new" template' do
        post :create, rt_solidarity_chain: invalid_attributes
        expect(response).to render_template('new')
      end
    end
  end
end
