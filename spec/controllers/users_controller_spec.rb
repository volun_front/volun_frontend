require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  let(:user) { create(:user, :user_entity) }
  before(:each) do
    sign_in user
  end
  let(:valid_attributes) {
    FactoryGirl.attributes_for(:user)
  }

  let(:volunteer_attributes){
    FactoryGirl.attributes_for(:user, :user_volunteer)
  }

  describe "GET #show" do
    it 'status 200' do
     
      get :show,id: user.id
      expect(response.status).to eq(200)
    end
  end

  describe "GET #search_activities" do
   it 'assigns a user as @users' do
     
      get :search_activities, format: :js
      expect(assigns(:activities).count).not_to eq(0)
    end
  end

  describe "GET #search_projects" do
    it 'assigns a user as @users' do
      
       get :search_projects, format: :js
       expect(assigns(:projects).count).not_to eq(0)
     end
    
     it 'assigns a user as @users' do
      volunteer_attributes[:email]="examplexx@example.es"
      user_volun=User.create!(volunteer_attributes)
      sign_in user_volun
      get :search_projects, format: :js
      expect(assigns(:projects).count).not_to eq(0)
    end
   end

end
