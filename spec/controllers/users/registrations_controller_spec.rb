require 'rails_helper'

RSpec.describe Users::RegistrationsController, type: :controller do
   

    describe "PUT #update" do
        context 'with valid params' do
            it 'assigns the requested entity as @entity' do
                user=create(:user, :user_entity)
                put :update, id: user.to_param, user:  user.to_param
                expect(assigns(:user)).to eq(user)
            end

            it 'redirects to entitys' do
                user=create(:user, :user_entity)
                put :update, id: user.to_param, user:  user.to_param
                expect(response).to redirect_to(users_url)
            end
        end
    end
end
