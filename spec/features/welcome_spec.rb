  require 'rails_helper'

feature 'Welcome' do


  scenario 'Index with featured' do
    projects = Project.all#[create(:project, :featured),create(:project, :featured),create(:project, :featured)]
    projects[0].outstanding=true
    projects[0].save
    visit 'welcome/index'

    expect(page).to have_selector('#projects', count: 1)
    projects.each do |project|
      within('#projects') do
        expect(page).to have_content project.name
        expect(page).to have_content project.description
        #expect(page).to have_css(".row destacadosWrapperHome", text: project.description)
      end
    end

  end

  scenario 'Index without featured' do
    projects = Project.all#[create(:project),create(:project),create(:project)]

    visit 'welcome/index'
    expect(page).to have_selector('#projects', count: 1)
    projects.each do |project|
      within('#projects') do
        expect(page).not_to have_content project.name
        expect(page).not_to have_content project.description
        #expect(page).to have_css(".row destacadosWrapperHome", text: project.description)
      end
    end

  end

  scenario 'user no validado' do

    visit 'welcome/index'

    click_link I18n.t('action.in_session')
    fill_in 'user_username', with: 'pereekdkdk@madrid.es'
    fill_in 'user_password', with: 'Testing'
    click_button I18n.t('action.in_session')
    expect(page).not_to have_content I18n.t('devise.failure.invalid')

  end

  scenario 'user' do
    login_as(create(:user, :user_entity))

    visit 'welcome/index'

    expect(page).to have_content I18n.t('shared.personal_area')

  end


end